import React, {useState} from 'react';
import {mount} from 'enzyme/build';
import useOutsideClick from './OutsideClick';

const map = {};
document.addEventListener = jest.fn((event, cb) => {
  map[event] = cb;
});

const TestComponent = () => {
  const ref = {
    current: {
      contains: target => target === 'div',
    },
  };
  const [state, setState] = useState(0);
  useOutsideClick(ref, () => {
    setState(1);
  });
  return <div>{state}</div>;
};

describe('OutsideClick', () => {
  it('should render initial state', () => {
    const component = mount(<TestComponent />);
    expect(component.find('div').text()).toBe('0');
  });

  it('should call callback and render 1 when click outside', () => {
    const component = mount(<TestComponent />);
    map.mousedown({
      target: null,
    });
    expect(component.find('div').text()).toBe('1');
  });

  it('should not call callback and not render 1 when click inside component', () => {
    const component = mount(<TestComponent />);
    map.mousedown({
      target: 'div',
    });
    expect(component.find('div').text()).toBe('0');
  });
});
