import React, {useCallback, useEffect} from "react";

const useOutsideClick = (ref: React.Ref | [React.Ref], callback) => {
  const handleClick = useCallback(
    (e) => {
      if (!ref) return;

      const isRefContainsTarget = (ref) => {
        return ref.current && ref.current.contains(e.target);
      };

      if (Array.isArray(ref)) {
        const isAnyRefContainsTarget = ref.some((refItem) => {
          return isRefContainsTarget(refItem);
        });

        !isAnyRefContainsTarget && callback();
      } else {
        !isRefContainsTarget(ref) && callback();
      }
    },
    [callback, ref]
  );

  useEffect(() => {
    document.addEventListener("mousedown", handleClick);
    document.addEventListener("touchstart", handleClick);

    return () => {
      document.removeEventListener("mousedown", handleClick);
      document.removeEventListener("touchstart", handleClick);
    };
  }, [handleClick]);
};

export default useOutsideClick;
