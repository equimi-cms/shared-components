import { useEffect } from "react";
import { WindowResponse } from "../../services/WindowResponse";

export const useWindowResize = (onResize: Function = () => null) => {
  useEffect(() => {
    const wr = new WindowResponse(onResize);
    return () => wr.destroy();
  }, [onResize]);
};
