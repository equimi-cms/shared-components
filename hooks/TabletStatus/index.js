import {useState, useEffect} from 'react';
import {WindowResponse} from '../../services/WindowResponse';

export const useTabletStatus = () => {
  const [isTablet, setIsTablet] = useState(false);
  useEffect(() => {
    const wr = new WindowResponse((e, isMob, isTablet) => {
      setIsTablet(isTablet);
    });
    return () => wr.destroy();
  });
  return isTablet;
};
