import {useEffect, useState} from "react";
import {WindowResponse} from "../../services/WindowResponse";
import {isClient} from "../../utils/layout";

export const getMobileInitialState = () => (isClient() ? WindowResponse.isMobile() : false);

export const useMobileStatus = (
  onResize: Function = () => null,
  initialState = false
) => {
  const [isMobile, setIsMobile] = useState(initialState);

  useEffect(() => {
    const wr = new WindowResponse((e, isMob, isTablet) => {
      setIsMobile(isMob);
      onResize(e, isMob, isTablet);
    });
    return () => wr.destroy();
  }, [onResize]);

  return isMobile;
};
