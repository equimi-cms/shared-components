import {useEffect} from "react";

export const useWindowScroll = (onScroll: Function = () => null, el = window) => {
  useEffect(() => {
    if (!el) return;

    el.addEventListener("scroll", onScroll);
    return () => {
      el.removeEventListener("scroll", onScroll);
    };
  }, [onScroll, el]);
};
