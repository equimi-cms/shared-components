import {useCallback, useEffect, useRef} from "react";

export const useScrollEvents = (scrollElRef, options = {}) => {
  const ref = useRef({
    timer: setTimeout(() => null, 0),
  });

  const onScroll = useCallback(
    (e) => {
      const scrollEl = e.target;

      const isHScroll = scrollEl.state.scrollLeft !== scrollEl.scrollLeft;
      const isVScroll = scrollEl.state.scrollTop !== scrollEl.scrollTop;

      scrollEl.state = {
        scrollLeft: scrollEl.scrollLeft,
        scrollTop: scrollEl.scrollTop,
      };

      // eslint-disable-next-line no-unused-expressions
      options.onScroll && options.onScroll?.(e, {isHScroll, isVScroll});

      clearTimeout(ref.current.timer);

      ref.current.timer = setTimeout(() => {
        options.onScrollStop && options.onScrollStop(scrollEl);
      }, 100);
    },
    [options]
  );

  useEffect(() => {
    const scrollEl = scrollElRef.current;

    scrollEl.state = {
      scrollLeft: scrollEl.scrollLeft,
      scrollTop: scrollEl.scrollTop,
    };

    scrollEl.addEventListener("scroll", onScroll, {passive: true});
    return () => {
      scrollEl.removeEventListener("scroll", onScroll, {passive: true});
    };
  }, [scrollElRef, onScroll]);
};
