/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {Image} from './Image';
import {Common} from '../../../../.storybook/config';

const imageSrc =
  'https://images.pexels.com/photos/459225/pexels-photo-459225.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500';
const styles = {width: '500px'};

storiesOf(Common + '/Image', module)
  .add('default', () => <Image src={imageSrc} />)
  .add('className image', () => <Image className="image" style={styles} src={imageSrc} />)
  .add('className image--grayscale', () => (
    <Image className="image image--grayscale" style={styles} src={imageSrc} />
  ))
  .add('className image--cursor', () => (
    <Image className="image image--cursor" style={styles} src={imageSrc} />
  ));
