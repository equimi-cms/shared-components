import React from 'react';
import {shallow} from 'enzyme/build';
import {Image} from './Image';

describe('Image', () => {
  it('should renders', () => {
    const component = shallow(<Image src={'testImg'} />);
    expect(component.props().src).toBe('testImg');
  });

  it('should prevent drag event', () => {
    const mockFn = jest.fn();
    const component = shallow(<Image src={'testImg'} />);
    component.simulate('dragStart', {
      preventDefault:mockFn
    });
    expect(mockFn).toBeCalled();
  });
});
