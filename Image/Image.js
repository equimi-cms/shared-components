import React from 'react';
import PropTypes from 'prop-types';
import './Image.scss';

export const Image = ({src, ...props}) => {
  const preventDragHandler = e => {
    e.preventDefault();
  };
  return (
    <img {...props} src={src} alt={props.alt || 'equimi image'} onDragStart={preventDragHandler} />
  );
};

Image.propTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
};

export default React.memo(Image);
