/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import SvgInlineImage from './SvgInlineImage';
import {Common} from '../../../../../.storybook/config';
import icon from '../../../../scenes/Auth/imgs/login-phone.svg';

storiesOf(Common + '/SvgInlineImage', module).add('default', () => (
  <div style={{display: 'inline-block'}}>
    <SvgInlineImage src={icon} />
  </div>
));
