import React from 'react';
import SvgInlineImage from './SvgInlineImage';
import {mount} from 'enzyme/build';
const icon = ``;

describe('SvgInlineImage', () => {
  it('renders', () => {
    const component = mount(<SvgInlineImage src={icon} />);
    expect(component.html().includes('svg')).toBeTruthy();
  });
});
