import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './SvgInlineImage.scss';
import SVG from 'react-inlinesvg';

const SvgInlineImage = ({
  src,
  type,
  classNameRoot,
  className,
  ...props
}: SvgInlineImage.propsType) => {
  return (
    <div {...props} className={classNames('svg-image-cnt', classNameRoot)}>
      <SVG
        src={src}
        className={classNames('svg-image', type && 'svg-image--svg-full', className)}
      />
    </div>
  );
};

export default React.memo(SvgInlineImage);

SvgInlineImage.propsType = {
  src: PropTypes.string.required,
  type: PropTypes.oneOf(['', 'svg-full']),
  className: PropTypes.string,
  classNameRoot: PropTypes.string,
};

SvgInlineImage.defaultType = {
  type: '',
};
