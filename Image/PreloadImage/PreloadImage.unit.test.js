import React from 'react';
import {shallow} from 'enzyme/build';
import {PreloadImage} from './PreloadImage';

const img = '../../../../../scenes/Auth/imgs/email.svg';
const img1 = '../../../../../scenes/Auth/imgs/login.svg';

describe('PreloadImage', () => {
  it('renders', () => {
    const component = shallow(<PreloadImage />);
    expect(component.hasClass('away')).toBeTruthy();
    expect(component.text()).toBe('');
  });

  it('render image', () => {
    const component = shallow(<PreloadImage imgs={[img]} />);
    expect(component.find('Image').length).toBe(1);
  });

  it('render multiple images', () => {
    const component = shallow(<PreloadImage imgs={[img, img1]} />);
    expect(component.find('Image').length).toBe(2);
  });
});
