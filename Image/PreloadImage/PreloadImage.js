import React from 'react';
import PropTypes from 'prop-types';
import {Image} from '../Image';

export const PreloadImage = props => {
  return (
    <div className="preload-image away">
      {props.imgs.map((src, index) => (
        <Image key={index} src={src} />
      ))}
    </div>
  );
};
PreloadImage.propTypes = {
  imgs: PropTypes.array,
};
PreloadImage.defaultProps = {
  imgs: [],
};
