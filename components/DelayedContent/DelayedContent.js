//@flow
import React, {useState, useEffect} from 'react';
import Loader from '../../Loader/Loader';

interface IDelayedContentProps {
  children: React.Elements;
  timeBeforeShow: Number;
}

const DelayedContent = ({children, timeBeforeShow}: IDelayedContentProps) => {
  const [content, setContent] = useState(null);

  useEffect(() => {
    const time = setTimeout(() => setContent(children), timeBeforeShow);
    return () => clearTimeout(time);
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    const time = setTimeout(() => setContent(children), timeBeforeShow);
    return () => clearTimeout(time);
    // eslint-disable-next-line
  }, [children]);

  return content ? content : <Loader type={'full'} size={30} />;
};

DelayedContent.defaultProps = {
  children: null,
  timeBeforeShow: 0,
};

export default React.memo(DelayedContent);
