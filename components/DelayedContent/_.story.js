/* eslint-disable import/first */
import React from 'react';
import {storiesOf} from '@storybook/react';
import DelayedContent from './DelayedContent';

storiesOf('Shared/DelayedContent', module)
  .add('default', () => (
    <div style={{display: 'inline-block'}}>
      <DelayedContent>
        Delayed Content <br /> rendered after 0ms
      </DelayedContent>
    </div>
  ))
  .add('with timeBeforeShow 500ms', () => (
    <div style={{display: 'inline-block'}}>
      <DelayedContent timeBeforeShow={500}>
        Delayed Content <br /> rendered after 500ms
      </DelayedContent>
    </div>
  ));
