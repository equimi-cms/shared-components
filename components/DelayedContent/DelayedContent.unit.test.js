import React from 'react';
import {shallow, mount} from 'enzyme/build';
import DelayedContent from './DelayedContent';

describe('DelayedContent', () => {
  it('should render', () => {
    const component = shallow(
      <div>
        <DelayedContent>
          <div className="test">test</div>
        </DelayedContent>
      </div>
    );
    expect(component.find('Memo(DelayedContent)').length).toBe(1);
  });

  it('should render loader before the content rendered', () => {
    const component = mount(
      <div>
        <DelayedContent timeBeforeShow={100}>
          <div className="test">test</div>
        </DelayedContent>
      </div>
    );
    expect(component.find('.loader').length).toBe(1);
  });

  it('should render content after 100ms', done => {
    const component = mount(
      <div>
        <DelayedContent timeBeforeShow={100}>
          <div className="test">testContent</div>
        </DelayedContent>
      </div>
    );
    setTimeout(() => {
      expect(component.html()).toContain('testContent');
      done();
    }, 100);
  });
});
