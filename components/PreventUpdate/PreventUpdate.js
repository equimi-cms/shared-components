import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';

export class PreventUpdate extends Component {
  shouldComponentUpdate(nProps, nS, nC) {
    return nProps.allowUpdate;
  }

  render() {
    return <Fragment>{this.props.children}</Fragment>;
  }
}

PreventUpdate.propTypes = {
  allowUpdate: PropTypes.bool,
  children: PropTypes.any,
};
PreventUpdate.defaultProps = {
  allowUpdate: false,
};
