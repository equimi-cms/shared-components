import React from 'react';
import {ButtonMod1} from './ButtonMod1';
import {shallow} from 'enzyme';

describe('ButtonMod1', () => {
  it('should renders', () => {
    const Main = shallow(
      <ButtonMod1>
        <p>test</p>
      </ButtonMod1>
    );
    expect(Main.find('p').length).toBe(1);
  });

  it('should render class name when pass className prop`', () => {
    const Main = shallow(
      <ButtonMod1 className="test">
        <p>test</p>
      </ButtonMod1>
    );
    expect(Main.props().className).toContain('test');
  });
  it('should renders with props', () => {
    const Main = shallow(
      <ButtonMod1 onClick={() => null}>
        <p>test</p>
      </ButtonMod1>
    );
    expect(Main.props().onClick).toBeDefined();
  });
});
