//@flow
import React from 'react';
import './ButtonMod1.scss';
import classNames from 'classnames';
import {renderTypesAsClassNames} from '../../utils/utils';

interface IDefaultProps {
  className?: String;
  type?: '' | 'primary' | 'inline';
  children: React.ReactNode;
}

export const ButtonMod1 = ({className, type, children, ...props}: IDefaultProps) => {
  return (
    <div
      {...props}
      className={classNames('m-btn-m1', renderTypesAsClassNames(type, 'm-btn-m1--'), className)}
    >
      {children}
    </div>
  );
};

export default React.memo(ButtonMod1);

ButtonMod1.defaultProps = {
  type: 'primary',
};
