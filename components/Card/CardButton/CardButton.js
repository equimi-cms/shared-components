import React from 'react';
import classNames from 'classnames';
import './CardButton.scss';
import {renderTypesAsClassNames} from '../../../utils/utils';
import SvgInlineImage from '../../../Image/SvgInlineImage/SvgInlineImage';
import plus from './imgs/icon-plus.svg';
import dots from './imgs/dots.svg';
import close from './imgs/close.svg';
import link from './imgs/icon-link.svg'

const type = '' | 'line' | 'plus' | 'dots' | 'danger' | 'link' | 'full-h';

interface ICardButtonProps {
  type?: type | Array<type>;
  className?: String;
  children?: React.ReactElement;
}

export const CardButton = ({children, className, type, ...props}: ICardButtonProps) => {
  let image = null;

  if (type.length) {
    if (type.includes('plus')) {
      image = <SvgInlineImage className="svg-path-stroke js-image" type="svg-full" src={plus} />;
    } else if (type.includes('dots')) {
      image = <SvgInlineImage className="svg-path-fill js-image" type="svg-full" src={dots} />;
    } else if (type.includes('danger')) {
      image = <SvgInlineImage className="svg-path-fill js-image" type="svg-full" src={close} />;
    }
    else if(type.includes('link')) {
      image = <SvgInlineImage className="svg-path-fill js-image" type="svg-full" src={link} />;
    }
  }

  return (
    <div
      className={classNames(
        'm-card-btn',
        'js-m-card-btn',
        renderTypesAsClassNames(type, 'm-card-btn--type-'),
        className
      )}
      {...props}
    >
      {children ? children : image}
    </div>
  );
};

export default React.memo(CardButton);

CardButton.defaultProps = {
  type: [],
};
