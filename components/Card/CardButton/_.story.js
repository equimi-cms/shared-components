/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {CardButton} from './CardButton';

const styles = {backgroundColor: 'lightgray', width: 400, height: '60px'};
storiesOf('Shared/CardButton', module)
  .add('default', () => (
    <div style={styles}>
      <CardButton>Content</CardButton>
    </div>
  ))
  .add('with type plus', () => (
    <div style={styles}>
      <CardButton type={'plus'} />
    </div>
  ))
  .add('with type dots', () => (
    <div style={styles}>
      <CardButton type={'dots'} />
    </div>
  ));
