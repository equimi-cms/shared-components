import React from 'react';
import {CardButton} from './CardButton';
import {shallow} from 'enzyme';

describe('CardButton', () => {
  it('should renders', () => {
    const Main = shallow(
      <CardButton>
        <div className="content">content</div>
      </CardButton>
    );
    expect(
      Main.find('.content')
        .text()
        .trim()
    ).toBe('content');
  });

  it('should render with className', () => {
    const Main = shallow(<CardButton className={'test'} />);
    expect(Main.find('.js-m-card-btn').hasClass('test')).toBeTruthy();
  });

  it('should render type `line`', () => {
    const Main = shallow(<CardButton type={'line'} />);
    expect(Main.find('.js-m-card-btn').hasClass('m-card-btn--type-line')).toBeTruthy();
  });

  it('should render type `plus`', () => {
    const Main = shallow(<CardButton type={'plus'} />);
    expect(Main.find('.js-m-card-btn').hasClass('m-card-btn--type-plus')).toBeTruthy();
    expect(Main.find('.js-image').prop('src')).toBe('icon-plus.svg');
  });

  it('should render type `dots`', () => {
    const Main = shallow(<CardButton type={'dots'} />);
    expect(Main.find('.js-m-card-btn').hasClass('m-card-btn--type-dots')).toBeTruthy();
    expect(Main.find('.js-image').prop('src')).toBe('dots.svg');
  });

  it('should render type `danger`', () => {
    const Main = shallow(<CardButton type={'danger'} />);
    expect(Main.find('.js-m-card-btn').hasClass('m-card-btn--type-danger')).toBeTruthy();
    expect(Main.find('.js-image').prop('src')).toBe('close.svg');
  });

  it('should render with props when props entered', () => {
    const Main = shallow(<CardButton test={1} />);
    expect(Main.props().hasOwnProperty('test')).toBeTruthy();
  });
});
