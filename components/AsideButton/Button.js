//@flow
import React from "react";
import "./Button.scss";
import classNames from "classnames";
import arrowLeft from "./imgs/arrow-left.svg";
import close from "./imgs/close.svg";
import plus from "./imgs/plus.svg";
import {renderTypesAsClassNames} from "../../utils/utils";
import SvgInlineImage from "../../Image/SvgInlineImage/SvgInlineImage";
import {closeButtonBase64} from "./helpers";

interface IButtonProps {
  type?: ["" | "arrowLeft" | "close" | "text" | "mod-1" | "mod-2" | "plus"];
  allowTypeAsClassName?: boolean;
  title: any;
  disabled?: Boolean;
  /** set styles as disabled with all events*/
  isActive?: Boolean;
  className?: String;
  translate?: (v: string) => v;
}

export const Button = ({
  type,
  allowTypeAsClassName,
  title,
  className,
  isActive,
  disabled,
  translate: Translate,
  ...props
}: IButtonProps) => {
  let cnt = "",
    isText = false;
  const _title = Translate ? <Translate value={title} dangerousHTML /> : <span>{title}</span>;
  const _type = Array.isArray(type) ? type[0] : type;
  switch (_type) {
    case "plus":
      cnt = <SvgInlineImage src={plus} />;
      break;
    case "arrowLeft":
      cnt = <SvgInlineImage width="22" src={arrowLeft} />;
      break;
    case "close":
      cnt = <img width={18} height={18} alt={"close-icon"} src={closeButtonBase64} />;
      break;
    case "text":
      isText = true;
      cnt = _title;
      break;
    case "mod-2":
      cnt = _title;
      break;
    default:
      break;
  }

  return (
    <div
      {...props}
      className={classNames(
        "m-button",
        "js-m-button",
        !isActive && "m-button--not-active",
        disabled && "m-button--disabled",
        allowTypeAsClassName && renderTypesAsClassNames(type, "m-button--"),
        isText && "m-button--type-text",
        className
      )}
    >
      {cnt}
    </div>
  );
};

Button.defaultProps = {
  type: "",
  title: "",
  isActive: true,
  allowTypeAsClassName: true,
  disabled: false,
  className: "",
};

export default React.memo(Button);
