import React from "react";
import {Button} from "./Button";
import {shallow} from "enzyme";
import SvgInlineImage from "../../Image/SvgInlineImage/SvgInlineImage";

describe('Button', () => {
  it('should render button with type `text`', () => {
    const Main = shallow(<Button type="text" title="Done" />);
    expect(Main.text()).toBe('Done');
    expect(Main.find('.js-m-button').hasClass('m-button--type-text')).toBeTruthy();
  });

  it('should render with className', () => {
    const Main = shallow(<Button className="test" />);
    expect(Main.find('.js-m-button').hasClass('test')).toBeTruthy();
  });

  it('should render with props when props entered', () => {
    const fn = jest.fn();
    const Main = shallow(<Button onClick={fn} />);
    Main.find('.js-m-button').simulate('click');
    expect(fn).toHaveBeenCalled();
  });

  it('should render plus icon when type is `plus`', () => {
    const Main = shallow(<Button type={'plus'} />);
    expect(Main.find(SvgInlineImage).prop('src')).toBe('plus.svg');
  });

  it('should render close icon when type is `close`', () => {
    const Main = shallow(<Button type={'close'} />);
    expect(Main.find(SvgInlineImage).prop('src')).toBe('close.svg');
  });

  it('should render arrowLeft icon when type is `arrowLeft`', () => {
    const Main = shallow(<Button type={'arrowLeft'} />);
    expect(Main.find(SvgInlineImage).prop('src')).toBe('arrow-left.svg');
  });

  it('should render type `mod-2`', () => {
    const Main = shallow(<Button type="mod-2" title="Done" />);
    expect(Main.text()).toBe('Done');
    expect(Main.find('.js-m-button').hasClass('m-button--type-text')).toBeFalsy();
  });

  it('should render button disabled', () => {
    const Main = shallow(<Button disabled />);
    expect(Main.find('.js-m-button').hasClass('m-button--disabled')).toBeTruthy();
  });

  it('should render button inactive', () => {
    const Main = shallow(<Button isActive={false} />);
    expect(Main.find('.js-m-button').hasClass('m-button--not-active')).toBeTruthy();
  });
});
