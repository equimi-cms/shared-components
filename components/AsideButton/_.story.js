/* eslint-disable import/first */
import React from 'react';
import {storiesOf} from '@storybook/react';
import {Button} from './Button.js';
// import {ButtonAction} from './ButtonAction/ButtonAction.js';

storiesOf('Shared/Button', module)
  .add('default container', () => <Button />)
  .add('with text', () => <Button type={'text'} title={'Btn'} />)
  .add('arrowLeft', () => <Button type={'arrowLeft'} />)
  .add('plus', () => <Button type={'plus'} />)
  .add('skip button mod-2', () => <Button type={['mod-2']} title="Skip" />)
  .add('close', () => <Button type={'close'} />);

/*storiesOf(Dashboard + '/ButtonAction', module)
  .add('default', () => <ButtonAction>Btn</ButtonAction>)
  .add('remove', () => <ButtonAction type={['remove', '']}>Delete</ButtonAction>)
  .add('close', () => <ButtonAction type={['close', '']}>Cancel</ButtonAction>)
  .add('borderTop', () => <ButtonAction type={['borderTop']}>Btn</ButtonAction>);*/
