import {getPhoneWithPlusSign} from './utils';

describe('getPhoneWithPlusSign', () => {
  it('should return phone value with +', () => {
    expect(getPhoneWithPlusSign('+123')).toBe('+123');
  });

  it('should return updated phone value with +', () => {
    expect(getPhoneWithPlusSign('123')).toBe('+123');
  });

  it('should return phone value with + when passed empty string', () => {
    expect(getPhoneWithPlusSign('')).toBe('+');
  });
});
