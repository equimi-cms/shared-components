export const KEYS = {
  del: 46,
  enter: 13,
  back: 8,
  tab: 9,
  space: 32,
  androidBack: 229,
  dot: 190,
  comma: 188,
  minus: 189,
  ctrl: 17,
  cmd: 91,
  v: 86,
};


/**
 *
 * @param value
 * @returns {*}
 */
export const getPhoneWithPlusSign = (value: string | number = "") => {
  const isStartsWithPlusSign = /^\+/.test(value);
  return isStartsWithPlusSign ? value : `+${value}`;
};
export const cutZerosInPhoneStart = (value: string | number = "") => {
  return value.replace(/^00/, "");
};

export const isEnter = (val: number): boolean => val === KEYS.enter;