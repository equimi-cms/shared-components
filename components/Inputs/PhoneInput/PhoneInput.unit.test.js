import React from "react";
import {shallow} from "enzyme";
import {PhoneInput} from "./PhoneInput";
import ReactPhoneInput from "react-phone-input-2";

const mockPhone = "+380545456979";
const mockCountry = "ua";
const reactPhoneInputProps = component => component.find(ReactPhoneInput).props();
const testService = {isTestEnv: () => true};
describe("PhoneInput", () => {
  it("should renders", () => {
    const component = shallow(<PhoneInput testService={testService}/>);
    expect(component.hasClass("test")).toBeTruthy();
  });
  it("should render `ReactPhoneInput` and give him props `value` and `defaultCountry` and each other props", () => {
    const component = shallow(
      <PhoneInput testService={testService} value={mockPhone} defaultCountry={mockCountry} testProp/>,
    );

    expect(reactPhoneInputProps(component).value).toBe(mockPhone);
    expect(reactPhoneInputProps(component).defaultCountry).toBe(mockCountry);
    expect(reactPhoneInputProps(component).testProp).toBeDefined();
  });
});
