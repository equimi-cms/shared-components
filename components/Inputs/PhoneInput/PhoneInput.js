import React, {Component, ReactChildren} from "react";
import ReactPhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import classNames from "classnames";
import "./PhoneInput.scss";
import {cutZerosInPhoneStart, getPhoneWithPlusSign, isEnter as checkIsEnter} from "./utils/utils";

export interface IPhoneInputProps {
  countryCode?: string;
  placeholder?: string;
  inputPlaceholder?: string;
  onCountryCode?: () => void;
  onKeyDown?: () => void;
  onChange?: () => void;
  onEnter?: () => void;
  value?: string;
  children?: ReactChildren;
  className?: string;
  inputExtraProps?: any;
  getUserCountryCodeMemoized: () => string;
  testService: {isTestEnv: () => boolean};
}

export class PhoneInput extends Component<IPhoneInputProps> {
  constructor(props) {
    super(props);
    this.state = {
      countryCode: props.countryCode,
    };

    /**
     * for testing set country directly
     */
    if (props.testService.isTestEnv()) {
      this.state.countryCode = "ua";
    }
  }

  componentDidMount() {
    if (this.state.countryCode || this.props.value) return;

    this.props.getUserCountryCodeMemoized().then((countryCode) => {
      const _countryCode = countryCode && countryCode.toLowerCase();
      this.setState(
        {
          countryCode: _countryCode,
        },
        () => this.props.onCountryCode(_countryCode),
      );
    });
  }

  handleKeyDown = (e) => {
    const isEnter = checkIsEnter(e.keyCode);
    this.props.onKeyDown(e);
    isEnter && this.props.onEnter(e);
  };

  handleChange = (value: string | number) => {
    this.props.onChange(getPhoneWithPlusSign(cutZerosInPhoneStart(value)));
  };

  render() {
    const {countryCode: country} = this.state;
    const {
      className,
      value,
      inputExtraProps,
      onKeyDown,
      onEnter,
      onChange,
      children,
      inputPlaceholder,
      ...props
    } = this.props;

    const _inputExtraProps = {
      name: "phone",
      placeholder: inputPlaceholder,
      required: true,
      autoFocus: true,
      ...inputExtraProps,
    };

    return (
      <div className={classNames("input-phone", className, "test")}>
        <ReactPhoneInput
          value={value}
          country={country}
          inputExtraProps={_inputExtraProps}
          onKeyDown={this.handleKeyDown}
          onChange={this.handleChange}
          {...props}
        />
        {children}
      </div>
    );
  }
}

PhoneInput.defaultProps = {
  countryCode: null,
  onCountryCode: () => null,
  onChange: () => null,
  onKeyDown: () => null,
  onEnter: () => null,
  inputExtraProps: {},
};
