/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {PhoneInput} from './PhoneInput';
import {Common} from '../../../../.storybook/config';

storiesOf(Common + '/PhoneInput', module).add('default', () => <PhoneInput placeholder="+380" />);
