import React from 'react';
import {shallow} from 'enzyme';
import {ClearButton} from './ClearnButton';
describe('UndoButton', () => {
  it('should renders', () => {
    const component = shallow(<ClearButton />);
    expect(component.hasClass('test')).toBeTruthy();
  });
  it('should call `onClick` function while clicked', () => {
    const mockOnClick = jest.fn();
    const component = shallow(<ClearButton onClick={mockOnClick} />);
    component.simulate('click');
    expect(mockOnClick).toHaveBeenCalled();
  });
});
