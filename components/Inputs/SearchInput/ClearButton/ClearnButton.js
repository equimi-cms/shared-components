import React from 'react';
import PropTypes from 'prop-types';
import SVG from 'react-inlinesvg';
import clearIcon from '../imgs/ClearIcon.svg';
import './ClearButton.scss';

interface IClearButtonProps {
  onClick: () => void;
}
export const ClearButton = ({onClick}: IClearButtonProps): JSX.Element => {
  return (
    <div className="clear-button test" onClick={onClick}>
      <SVG src={clearIcon} />
    </div>
  );
};

ClearButton.propTypes = {
  onClick: PropTypes.func,
};
