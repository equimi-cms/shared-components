/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {SearchInput} from './SearchInput';

storiesOf('Shared/SearchInput', module)
  .add('default', () => <SearchInput />)
  .add('with value', () => <SearchInput value={'test'} />)
  .add(
    'on mobile devices default',
    () => {
      return <SearchInput isMobile={true} />;
    },
    {viewport: {defaultViewport: 'iphonex'}}
  )
  .add(
    'on mobile devices with value',
    () => {
      return <MobileWithValue />;
    },
    {viewport: {defaultViewport: 'iphonex'}}
  );
const MobileWithValue = () => {
  return <SearchInput isMobile={true} value="Lauren Hough" />;
};
