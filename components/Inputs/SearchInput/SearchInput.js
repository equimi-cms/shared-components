//@flow
import React, {useEffect, useRef, useState} from "react";
import "./SearchInput.scss";
import classNames from "classnames";
import searchIcon from "./imgs/search.svg";
import Image from "../../../Image/Image";
import {ClearButton} from "./ClearButton/ClearnButton";
import {DebounceInput} from "react-debounce-input";

export interface ISearchInputProps {
  className: String;
  debounceTime: Number;
  onChange?: (val: string) => void;
  onSearch: (val: string) => void;
  value: any;
  searchBtnText?: any;
  isMobile: Boolean;
  isSearchBtn: Boolean;
  disableAutoFocus: Boolean;
  hideClearButton: Boolean;
}

/**
 * Show clear button `close` only on mobile
 */
export const SearchInput = ({
  isMobile,
  debounceTime,
  isSearchBtn,
  className,
  onChange,
  value,
  onSearch,
  inputClassName,
  disableAutoFocus,
  hideClearButton,
  searchBtnText,
  ...props
}: ISearchInputProps): React.ReactElement => {
  const [activeSearchBtn, setActiveSearchBtn] = useState(!!value);
  const [inputValue, setInputValue] = useState(value);
  const ref = useRef(null);

  useEffect(() => {
    !disableAutoFocus && ref.current.focus();
  });

  const _onChange = e => {
    const value = e.target.value;
    setActiveSearchBtn(!!value);
    setInputValue(value);
    onChange(value);
  };

  const _onKeyUp = e => {
    const enter = 13;
    if (e.keyCode === enter) onSearch(e.target.value);
  };

  const _onClick = () => {
    onSearch(ref.current.value);
  };
  const _onClear = () => {
    _onChange({target: {value: ''}});
  };

  const clearButton = hideClearButton
    ? null
    : activeSearchBtn && <ClearButton onClick={_onClear} />;

  const searchBtn = isSearchBtn && (
    <span
      onClick={_onClick}
      className={classNames(
        's-input-cnt__btn test-s-input-cnt__btn',
        activeSearchBtn ? 's-input-cnt__btn--active' : 's-input-cnt__btn--disabled'
      )}
    >
      {searchBtnText}
    </span>
  );

  return (
    <form action="#" method="post" onSubmit={e=>e.preventDefault()} className={classNames('s-input-cnt test-s-input', className)}>
      <label>
        <Image
          className={classNames('s-input-cnt__icon', isMobile && 's-input-cnt__icon--mobile')}
          src={searchIcon}
        />
        <DebounceInput
          {...props}
          inputRef={ref}
          value={inputValue}
          debounceTimeout={debounceTime}
          className={classNames(
            's-input',
            isMobile && 's-input--mobile',
            isSearchBtn && 's-input--search-btn',
            !!inputClassName && inputClassName
          )}
          onChange={_onChange}
          onKeyUp={_onKeyUp}
        />
      </label>
      {isMobile ? clearButton : searchBtn}
    </form>
  );
};

export default React.memo(SearchInput);

SearchInput.defaultProps = {
  isMobile: false,
  debounceTime: 300,
  disableAutoFocus: false,
  isSearchBtn: true,
  className: '',
  onChange:/* istanbul ignore next */ () => null,
  onSearch: /* istanbul ignore next */() => null,
};
