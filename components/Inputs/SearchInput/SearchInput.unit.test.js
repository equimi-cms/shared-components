import React from 'react';
import {SearchInput} from './SearchInput';
import {shallow, mount} from 'enzyme';
import {ClearButton} from './ClearButton/ClearnButton';

describe('SearchInput', () => {
  it('should renders', () => {
    const Main = shallow(<SearchInput />);
    expect(Main.find('.s-input').length).toBe(1);
  });

  it('should enable search btn when input value not empty', () => {
    const Main = mount(<SearchInput debounceTime={0} />);
    Main.find('DebounceInput').simulate('change', {
      target: {
        value: '1',
      },
    });
    expect(Main.find('.s-input-cnt__btn').hasClass('s-input-cnt__btn--active')).toBeTruthy();
  });
  it('should render `CleatButton` while `isMobile` props is true', () => {
    const component = shallow(<SearchInput isMobile={true} />);
    expect(component.find(ClearButton)).toBeTruthy();
  });
  it('should render `test-s-input-cnt__btn` while `isMobile` props is  false', () => {
    const component = shallow(<SearchInput isMobile={true} />);
    expect(component.find('test-s-input-cnt__btn')).toBeTruthy();
  });
});
