//@flow
import React, {Component} from "react";
import {type IContacts} from "../utils/types";
import "./ContactsItem.scss";
import {LinkItem} from "./components/LinkItem/LinkItem";
import {presets} from "../utils/constants";
import {Column} from "../../Column/Column";
import {ColumnTitle} from "../../Column/ColumnTitle/ColumnTitle";
import ActionItem from "./components/ActionItem/ActionItem";

interface IContactsItemProps {
  contacts: IContacts;
  onClick?: () => void;
  isMobile?: boolean;
  isTablet?: boolean;
  translate?: (key: string) => void;
  useMobileBasicMode?: boolean;
}

export class ContactsItem extends Component<IContactsItemProps> {
  render() {
    const {
      contacts,
      onClick = () => null,
      isMobile,
      isTablet,
      translate = () => null,
      useMobileBasicMode,
    }: IContactsItemProps = this.props;

    const notDesktop = isMobile || isTablet;
    const showFullItem = !notDesktop || useMobileBasicMode;
    const showCountInTitle = !useMobileBasicMode && notDesktop;

    return (
      <Column
        type={["action", "overflow"]}
        className="contacts-item test-contacts-item"
        onClick={onClick}
      >
        {!!contacts.general.length && (
          <div className="contacts-item__section">
            <ColumnTitle
              type={["mod-1", "mod-3", "mod-5"]}
              count={showCountInTitle && contacts.general.length}
            >
              {translate("CONTACTS.General_Contacts")}
            </ColumnTitle>
            {showFullItem && (
              <div className="contacts-item__group contacts-item__general">
                {contacts.general.map(({key, value}, index) => {
                  return (
                    (key || value) && (
                      <div key={index} className="contacts-item__content">
                        <div className="contacts-item__key test-contacts-general-key">
                          {" "}
                          {key
                            ? key
                            : !!value &&
                              translate(presets.general[index < 3 ? index : 2].keyPlaceholder)}
                        </div>
                        <ActionItem
                          value={value}
                          className="contacts-item__value test-contacts-general-value"
                        />
                      </div>
                    )
                  );
                })}
              </div>
            )}
          </div>
        )}

        {!!contacts.details.length && (
          <div className="contacts-item__section">
            <ColumnTitle
              type={["mod-1", "mod-3", "mod-5"]}
              count={showCountInTitle && contacts.details.length}
            >
              {translate("CONTACTS.Contact_Details")}
            </ColumnTitle>
            {showFullItem && (
              <div className="contacts-item__group contacts-item__details">
                {contacts.details.map((subDetails, index) =>
                  subDetails.map(({key, value}, indexOfDetail) => {
                    return (
                      (key || value) && (
                        <div
                          key={indexOfDetail}
                          className="contacts-item__content contacts-item__details--content"
                        >
                          <div className="contacts-item__key test-contacts-details-key">
                            {key
                              ? key
                              : value &&
                                translate(
                                  presets.contactDetails[index % 2 > 0 ? 1 : 0][indexOfDetail]
                                    .keyPlaceholder
                                )}
                          </div>
                          <ActionItem
                            value={value}
                            className="contacts-item__value test-contacts-details-value"
                          />
                        </div>
                      )
                    );
                  })
                )}
              </div>
            )}
          </div>
        )}

        {!!contacts.links.length && (
          <div className="contacts-item__section">
            <ColumnTitle
              type={["mod-1", "mod-3", "mod-5"]}
              count={showCountInTitle && contacts.links.length}
            >
              {translate("CONTACTS.External_Links")}
            </ColumnTitle>
            {showFullItem && (
              <div className="contacts-item__group contacts-item__links">
                {contacts.links.map(({key, value}, index) => {
                  return (
                    (key || value) && (
                      <div key={index} className="contacts-item__content">
                        <LinkItem type={key} value={value} />
                      </div>
                    )
                  );
                })}
              </div>
            )}
          </div>
        )}
      </Column>
    );
  }
}
