import React, { Component } from "react";
import { Image } from "../../../../../Image/Image";
import "./LinkItem.scss";
import facebookImage from "./imgs/facebook-image.svg";
import twitterImage from "./imgs/twitter-image.svg";
import instagramImage from "./imgs/instagram-image.svg";
import websiteImage from "./imgs/website-image.svg";
import {cutProtocolFromLink} from "../../../utils/CutProtocolFromLink/CutProtocolFromLink";
interface ILinkItemProps {
  type: String;
  value?: String;
}
export class LinkItem extends Component<ILinkItemProps> {
  getImageByKey = type => {
    let image;
    switch (type) {
      case "Facebook":
        image = facebookImage;
        break;
      case "Twitter":
        image = twitterImage;
        break;
      case "Instagram":
        image = instagramImage;
        break;
      default:
        image = websiteImage;
        break;
    }
    return image;
  };
  render() {
    const { value, type } = this.props;
    const image = this.getImageByKey(type);
    const url = value.match("//") ? value : `//${value}`;
    const formattedValue = cutProtocolFromLink(value);
    return (
      value && (
        <a className="link-item" href={url}>
          <Image className="link-item__icon" src={image} />
          <span className="link-item__value">{formattedValue}</span>
        </a>
      )
    );
  }
}
LinkItem.defaultProps = {
  value: ""
};
