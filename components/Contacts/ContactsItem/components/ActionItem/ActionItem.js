import React, { memo } from "react";
import isEmail from "validator/lib/isEmail";
import { parsePhoneNumberFromString } from "libphonenumber-js";
interface IActionItemProps {
  className: string;
  value: string;
}

function ActionItem({ className, value }: IActionItemProps) {
  if (!value) {
    return null;
  }
  if (isEmail(value)) {
    return (
      <a href={`mailto:${value}`} className={className}>
        {value}
      </a>
    );
  }
  if (parsePhoneNumberFromString(value)) {
    return (
      <a href={`tel:${value}`} className={className}>
        {value}
      </a>
    );
  } else {
    return <div className={className}>{value}</div>;
  }
}

ActionItem.defaultProps = {
    className: "",
    value: "",
  };

export default memo(ActionItem);

