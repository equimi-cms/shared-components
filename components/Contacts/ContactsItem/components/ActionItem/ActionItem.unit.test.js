import React from 'react';
import ActionItem from './ActionItem';
import {shallow} from 'enzyme';

describe('ActionItem', () => {
  const testClass = "test";
  it('should render nothing if value is empty', () => {
    const Main = shallow(<ActionItem className={testClass}/>);
    expect(Main.find(`.${testClass}`).length).toBeFalsy();
  });

  it('should render value if value is not empty', () => {
    const Main = shallow(<ActionItem className={testClass} value="test"/>);
    expect(Main.find(`.${testClass}`).length).toBeTruthy();
  });

  it('should render mailto link if value is valid email', () => {
    const mockEmail = "test@equimi.com";
    const Main = shallow(<ActionItem className={testClass} value={mockEmail}/>);
    expect(Main.find(`.${testClass}`).props().href).toBe(`mailto:${mockEmail}`);
  });

  it('should render tel link if value is valid phone', () => {
    const mockPhone = "+380660000000";
    const Main = shallow(<ActionItem className={testClass} value={mockPhone}/>);
    expect(Main.find(`.${testClass}`).props().href).toBe(`tel:${mockPhone}`);
  });


});
