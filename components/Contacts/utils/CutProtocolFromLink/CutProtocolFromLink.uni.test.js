import {cutProtocolFromLink} from "./CutProtocolFromLink";

describe('CutProtocolFromLink', function () {
   it("should cut `https , http, ftp` from given link or return unchanged link if it without protocol", () => {
       const mockHTTPSLink = "https://test.com";
       const mockHTTPLink = "https://test.com";
       const mockFTPLink = "https://test.com";
       const expectedResult = "test.com";
       expect(cutProtocolFromLink(mockHTTPSLink)).toBe(expectedResult);
       expect(cutProtocolFromLink(mockHTTPLink)).toBe(expectedResult);
       expect(cutProtocolFromLink(mockFTPLink)).toBe(expectedResult);
       expect(cutProtocolFromLink(expectedResult)).toBe(expectedResult);
   })
});