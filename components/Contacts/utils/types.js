//@flow
export interface IContact {
  key: String;
  value: String;
}
export interface IContacts {
  general: Array<IContact>;
  details: Array<Array<IContact>>;
  links: Array<IContact>;
}
export interface IContactsPresets {
  general: Array<IContact>;
  generalLimit: Number;
  contactDetails: Array<Array<IContact>>;
  contactDetailsLimit: Number;
  externalLinks: Array<IContact>;
  externalLinksLimit: Number;
}
