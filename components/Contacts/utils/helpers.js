import flattenDepth from "lodash/flattenDepth";
import type {IContact} from "./types";

/**
 *
 * @param string
 * @returns {*|boolean}
 */
const notEmpty: () => boolean = string => string && string.trim() !== '';

/**
 *
 * @param contacts
 * @returns {*}
 */
export const getNotEmptyContacts = contacts => {
  return contacts.filter(contact => contact && (notEmpty(contact.key) || notEmpty(contact.value)));
};

/**
 *
 * @param contacts
 * @returns {{isGeneralExist: *, isContactsExist: (*|boolean), isDetailsExist: (boolean|*), isLinksExist: boolean}}
 */
export const handleContactsAvailabillity = (contacts: IContact) => {
  const {general, details, links} = contacts;

  const isContactsNotEmpty = contacts => getNotEmptyContacts(contacts).length > 0;
  const isGeneralExist = isContactsNotEmpty(general);
  const isDetailsExist =
    details && details.length > 0 && isContactsNotEmpty(flattenDepth(details, 2));
  const isLinksExist =
    links && links.length > 0 && links.filter(link => link && notEmpty(link.value)).length > 0;
  const isContactsExist = isGeneralExist || isDetailsExist || isLinksExist;
  return {
    isContactsExist,
    isGeneralExist,
    isDetailsExist,
    isLinksExist,
  };
};
