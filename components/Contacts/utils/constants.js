import type {IContactsPresets} from './types';

export const presets: IContactsPresets = {
  general: [
    {
      keyPlaceholder: 'CONTACTS.Stable_Address',
      valuePlaceholder: 'CONTACTS.Ex_Address',
    },
    {keyPlaceholder: 'CONTACTS.Main_Email', valuePlaceholder: 'CONTACTS.Ex_Email'},
    {keyPlaceholder: 'CONTACTS.Phone', valuePlaceholder: 'CONTACTS.Ex_Phone'},
  ],
  generalLimit: 10,
  contactDetails: [
    [
      {
        keyPlaceholder: 'CONTACTS.DETAILS.Office',
        valuePlaceholder: 'CONTACTS.DETAILS.Ex_Office',
      },
      {
        keyPlaceholder: 'CONTACTS.DETAILS.Phone',
        valuePlaceholder: 'CONTACTS.DETAILS.Ex_Phone',
      },
      {
        keyPlaceholder: 'CONTACTS.DETAILS.Email',
        valuePlaceholder: 'CONTACTS.DETAILS.Ex_Email',
      },
    ],
    [
      {
        keyPlaceholder: 'CONTACTS.DETAILS.Role',
        valuePlaceholder: 'CONTACTS.DETAILS.Ex_Role',
      },
      {
        keyPlaceholder: 'CONTACTS.DETAILS.Phone',
        valuePlaceholder: 'CONTACTS.DETAILS.Ex_Phone',
      },
      {
        keyPlaceholder: 'CONTACTS.DETAILS.Email',
        valuePlaceholder: 'CONTACTS.DETAILS.Ex_2_Email',
      },
    ],
  ],
  contactDetailsLimit: 4,
  externalLinks: [
    {keyPlaceholder: 'Facebook', valuePlaceholder: 'CONTACTS.Facebook_Ex'},
    {keyPlaceholder: 'Twitter', valuePlaceholder: 'CONTACTS.Twitter_Ex'},
    {keyPlaceholder: 'Instagram', valuePlaceholder: 'CONTACTS.Instagram_Ex'},
    {keyPlaceholder: 'Website', valuePlaceholder: 'CONTACTS.paste_url_here'},
  ],
  externalLinksLimit: 8,
};
export const linksExamples = {
  facebook: {ex: 'facebook.com', name: 'Facebook'},
  twitter: {ex: 'twitter.com', name: 'Twitter'},
  instagram: {ex: 'instagram.com', name: 'Instagram'},
  website: {
    name: 'Website',
  },
};
