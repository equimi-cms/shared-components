// @flow
import React, {PureComponent} from "react";
import Button from "../../AsideButton/Button";
import {Aside} from "../../Aside/Aside";
import {ContactsItem} from "../ContactsItem/ContactsItem";
import {Scroll} from "../../Scroll/Scroll";

interface IContactsAsideProps {
  className?: String;
  show?: Boolean;
  onClose?: () => void;
  isMobile?: Boolean;
  asideTitle: String;
  translate: (v: string) => void;
}

interface IContactsAsideState {
  showLoader: Boolean;
}

export class ContactsAside extends PureComponent<
  IContactsAsideProps,
  IContactsAsideState
> {
  constructor(props) {
    super(props);
    this.state = {
      showLoader: false
    };
  }

  render() {
    const {
      translate,
      asideTitle,
      contacts,
      show,
      isMobile,
      onClose,
      className,
      ...rest
    } = this.props;

    return (
      <>
        <Aside
          open={show}
          className={`test-aside-contacts ${className}`}
          title={asideTitle}
          onOverlay={onClose}
          headerLeft={
            <Button type={isMobile ? "arrowLeft" : "close"} onClick={onClose} />
          }
          contentType="no-padding"
          withCustomScroll={false}
          {...rest}
        >
          <Scroll>
            <ContactsItem
              useMobileBasicMode
              isMobile={isMobile}
              contacts={contacts}
              translate={translate}
            />
          </Scroll>
        </Aside>
      </>
    );
  }
}

ContactsAside.defaultProps = {
  asideTitle: "",
  show: false,
  isMobile: false,
  onClose: () => null,
  translate: () => null
};
