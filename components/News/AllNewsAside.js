//@flow
import React, {useEffect, useState} from "react";
import {Aside} from "../Aside/Aside";
import "./AllNewsAside.scss";
import classNames from "classnames";
import Button from "../AsideButton/Button";
import {NewsAsideList} from "./NewsAsideList/NewsAsideList";
import {SearchBar} from "../../components/SearchBar/SearchBar";
import {useMobileStatus} from "../../hooks/MobileStatus";
import {useTabletStatus} from "../../hooks/TabletStatus";
import type {INews} from "../../utils/type";

interface IAllNewsAsideProps {
  className?: String;
  showAside: Boolean;
  entity?: String;
  onCloseAside: () => void;

  showPlusButton: Boolean;
  onPlusButtonClick: () => void;

  onGetNextNews: () => void;
  onSearchMoreNews: () => void;
  timeLine: [];
  onSearchingStart: () => void;
  onSearchLastNews: () => void;
  onSearchingEnd: () => void;
  searching: Boolean;

  inputPlaceholder: String;
  emptySearchDataTitle: String;
  searchDataTitle: String;
  countOfEntitiesTitle: String;
  searchBtnText: String;
  asideTitle: String;
  noNewsTitle: String;
  clearBtnText: String;
  applyBtnText: String;
  cancelBtnText: String;

  newsData: [];
  newsCount: Number;
  isLoading: Boolean;
  searchedNews: [];
  searchedNewsCount: Number;
  onNewsItemClick: (news: INews) => void;
}

export const AllNewsAside = ({
  className,
  timeLine,
  showAside,
  onCloseAside,
  onPlusButtonClick,
  onGetLastNews,
  onGetNextNews,
  onSearchLastNews,
  onSearchMoreNews,
  onSearchingStart,
  onSearchingEnd,
  searching,
  inputPlaceholder,
  emptySearchDataTitle,
  searchDataTitle,
  countOfEntitiesTitle,
  searchBtnText,
  noNewsTitle,
  clearBtnText,
  applyBtnText,
  cancelBtnText,
  asideTitle,
  newsData,
  newsCount,
  isLoading,
  searchedNews,
  searchedNewsCount,
  showPlusButton,
  onNewsItemClick,
  entity,
}: IAllNewsAsideProps): React.ElementType => {
  const isMobile = useMobileStatus();
  const isTablet = useTabletStatus();
  const [isCalendarOpen, setIsCalendarOpen] = useState(false);
  const [isCalendarDataVisible, setIsCalendarDataVisible] = useState(true);
  const initialDateState = {
    dateFrom: undefined,
    dateTo: undefined,
  };
  const [filteringData, setFilteringData] = useState({
    query: "",
    ...initialDateState,
    isTouched: false,
  });
  const handleClear = () =>
    filteringData.isTouched && setFilteringData({...filteringData, ...initialDateState});
  const handleCloseAside = () => {
    filteringData.isTouched && setFilteringData({query: "", ...initialDateState, isTouched: false});
    onCloseAside();
  };
  const handleSearch = (query: String) =>
    setFilteringData({...filteringData, query: query, isTouched: true});
  const handlePickYear = (year: Number) =>
    setFilteringData({
      ...filteringData,
      dateFrom: new Date(year, 0),
      dateTo: new Date(year + 1, 0),
      isTouched: true,
    });
  const handlePickYearAndMonth = ({year, month}: Object<Number>) =>
    setFilteringData({
      ...filteringData,
      dateFrom: new Date(year, month),
      dateTo: new Date(year, month + 1),
      isTouched: true,
    });
  const handleToggleCalendar = (isOpen: Boolean) => setIsCalendarOpen(isOpen);
  const getFilteringValues = () => {
    let query, dateFrom, dateTo;
    if (filteringData.query) {
      query = filteringData.query;
    }
    if (filteringData.dateFrom) {
      dateFrom = filteringData.dateFrom;
    }
    if (filteringData.dateTo) {
      dateTo = filteringData.dateTo;
    }
    return {query, dateFrom, dateTo};
  };
  const loadNews = () => {
    let {query, dateFrom, dateTo} = getFilteringValues();

    filteringData.isTouched ? onSearchLastNews(query, dateFrom, dateTo) : onGetLastNews();
  };
  const loadMoreNews = (offset) => {
    let {query, dateFrom, dateTo} = getFilteringValues();

    filteringData.isTouched ? onSearchMoreNews(offset, query, dateFrom, dateTo) : onGetNextNews();
  };
  useEffect(() => {
    filteringData.isTouched ? !searching && onSearchingStart() : searching && onSearchingEnd();
    filteringData.isTouched &&
      !filteringData.query &&
      !filteringData.dateFrom &&
      !filteringData.dateTo &&
      setFilteringData({isTouched: false});
    loadNews();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filteringData]);
  return (
    <Aside
      open={showAside}
      withCustomScroll={false}
      className={classNames([
        `test-aside-all-${entity} aside-all-news `,
        showAside && "freezing-of-main-scroll",
        (isMobile || isTablet) && isCalendarOpen && "aside-all-news__overlay",
        className,
      ])}
      title={asideTitle}
      isBackBtn={true}
      onBack={handleCloseAside}
      onOverlay={!isCalendarOpen ? handleCloseAside : null}
      backBtnType={isMobile ? "arrowLeft" : "close"}
      contentType="no-padding"
      headerRight={
        showPlusButton && (
          <Button
            className={`news-button__add test-${entity}-btn-add`}
            onClick={onPlusButtonClick}
            type="plus"
          />
        )
      }
    >
      <SearchBar
        isMobile={isMobile}
        isTablet={isTablet}
        onSearch={handleSearch}
        onPickMonthAndYear={handlePickYearAndMonth}
        onPickYear={handlePickYear}
        timeLine={timeLine}
        onToggleCalendar={handleToggleCalendar}
        setIsCalendarDataVisible={setIsCalendarDataVisible}
        onClear={handleClear}
        inputPlaceholder={inputPlaceholder}
        emptySearchDataTitle={emptySearchDataTitle}
        searchDataTitle={searchDataTitle}
        countOfEntitiesTitle={countOfEntitiesTitle}
        searchBtnText={searchBtnText}
        clearBtnText={clearBtnText}
        applyBtnText={applyBtnText}
        cancelBtnText={cancelBtnText}
      />
      <NewsAsideList
        isCalendarDataVisible={isCalendarDataVisible}
        isMobile={isMobile}
        onGetNextNews={loadMoreNews}
        isSearching={filteringData.isTouched}
        noNewsTitle={noNewsTitle}
        newsData={newsData}
        newsCount={newsCount}
        isLoading={isLoading}
        searchedNews={searchedNews}
        searchedNewsCount={searchedNewsCount}
        onNewsItemClick={onNewsItemClick}
      />
    </Aside>
  );
};

AllNewsAside.defaultProps = {
  timeLine: [],
  entity: "news",
  showAside: false,
  showPlusButton: true,
  onCloseAside: () => null,
  isMobile: false,
  onPlusButtonClick: () => null,
  onSearchLastNews: () => null,
  onGetNextNews: () => null,
  onGetLastNews: () => null,
  onSearchingStart: () => null,
  onSearchingEnd: () => null,
  searching: false,
  onNewsItemClick: () => null,
};

export default AllNewsAside;
