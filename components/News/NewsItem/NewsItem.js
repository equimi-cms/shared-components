import React from 'react';
import moment from 'moment';
import Image from '../../../Image/Image';
import './NewsItem.scss';
import classNames from 'classnames';
import {getImageSrcWithSize} from '../../../utils/utils';
import type {INews} from '../../../utils/type';

interface INewsItemProps {
  createdAt: string;
  coverImage: string;
  title: string;
  body: string;
  countOfViewing?: number;
  className: String;
  onClick: (news: INews) => void;
  allNewsItemData: INews;
}

export const NewsItem = ({
  createdAt,
  coverImage,
  title,
  body,
  countOfViewing,
  className,
  allNewsItemData,
  onClick,
}: INewsItemProps) => {
  const handleClick = () => {
    onClick(allNewsItemData);
  };
  const date = moment(createdAt).format('LL');
  return (
    <div onClick={handleClick} className={classNames('news-item', 'test-news-item', className)}>
      {coverImage && coverImage.image && (
        <div className="news-item-img">
          <Image className="image" src={getImageSrcWithSize(coverImage, '350x184')} />
        </div>
      )}
      <div
        className={classNames([
          (!coverImage || (coverImage && !coverImage.image))
            ? 'news-item-content__without-image'
            : 'news-item-content',
        ])}
      >
        <div className="news-item-date-of-creating test-item-date">{date}</div>
        <h3 className="news-item-title test-item-title">{title}</h3>
        <p className="news-item-description test-item-body">{body}</p>

        {/*  <div className="news__item-count-of-viewing" /> */}
      </div>
    </div>
  );
};

/* istanbul ignore next */
NewsItem.defaultProps = {
  onClick: () => null,
};
