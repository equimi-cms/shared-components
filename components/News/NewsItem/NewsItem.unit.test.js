import React from 'react';
import {shallow} from 'enzyme';
import {NewsItem} from './NewsItem';
import Image from '../../../Image/Image';

describe('NewsItem', () => {
  const mockBody = 'Super body text';
  const mockTitle = 'Just title';
  const mockCoverImage = {
    image: 'nci-98-936.png',
    options: {},
    canvas: {},
    height: 76,
    rotate: 0,
    scaleX: 1,
    scaleY: 1,
    width: 135,
    x: 0,
    y: 2,
    zoom: 0,
    original: 'nci-98-936--src.png',
  };
  const mockDate = new Date('2019 - 04 - 10T12: 06: 38.625Z');
  it('should renders', () => {
    const component = shallow(<NewsItem />);
    expect(component.find('.test-news-item')).toHaveLength(1);
  });
  it('should render `Image` with provided `src` props if `coverImage` is provided', () => {
    const component = shallow(
      <NewsItem
        coverImage={mockCoverImage}
        createdAt={mockDate}
        title="Just image"
        body="Super body text"
      />
    );
    expect(
      component
        .find(Image)
        .props()
        .src.split('/')
        .slice(-1)
        .toString()
    ).toBe(mockCoverImage.image);
  });

  it('should render all provided data', () => {
    const component = shallow(<NewsItem createdAt={mockDate} title={mockTitle} body={mockBody} />);
    expect(component.find('.test-item-title').text()).toBe(mockTitle);
    expect(component.find('.test-item-body').text()).toBe(mockBody);
    expect(component.find('.test-item-date').text()).not.toBe('');
  });

  it('should call onClick', () => {
    const onClick = jest.fn();
    const component = shallow(
      <NewsItem onClick={onClick} createdAt={mockDate} title={mockTitle} body={mockBody} />
    );
    component.simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
