import React from 'react';
import {shallow} from 'enzyme';
import AllNewsAside from './AllNewsAside';

describe('AllNewsAside', () => {
  it('should renders', () => {
    const component = shallow(<AllNewsAside showAside />);
    expect(component.find('.test-aside-news'));
  });
});
