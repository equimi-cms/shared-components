import React from 'react';
import {shallow} from 'enzyme';
import {NewsAsideList} from './NewsAsideList';
import {EntitiesVirtualList} from '../../EntitiesVirtualList/EntititesVirtualList';

describe('NewsAsideList', () => {
  it('should renders', () => {
    const mockNewsData = [{title: 'test', description: 'test'}];
    const component = shallow(<NewsAsideList newsData={mockNewsData} />);
    expect(component.find('.test-aside-list')).toHaveLength(1);
  });
  it('should render no news title while no data', () => {
    const component = shallow(<NewsAsideList />);
    expect(component.find('.test-news-aside-header__nonews')).toHaveLength(1);
  });
  it('should render loader data while loading', () => {
    const component = shallow(<NewsAsideList isLoading={true} />);
    expect(component.find('.test-loader')).toHaveLength(1);
  });
  it('should render EntitiesVirtualList and pass there all props', () => {
    const mockNewsData = [{title: 'test', description: 'test'}];
    const mockOnGetNextNews = jest.fn();
    const component = shallow(
      <NewsAsideList newsData={mockNewsData} onGetNextNews={mockOnGetNextNews} />
    );
    expect(component.find(EntitiesVirtualList).props().entities).toEqual(mockNewsData);
    expect(component.find(EntitiesVirtualList).props().onLoadItemsScrollBoundary).toEqual(
      mockOnGetNextNews
    );
  });
});
