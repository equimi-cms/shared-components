//@flow
import React from 'react';
import './NewsAsideList.scss';
import {EntitiesVirtualList} from '../../EntitiesVirtualList/EntititesVirtualList';
import {useState} from 'react';
import {WindowResponse} from '../../../services/WindowResponse';
import {useEffect} from 'react';
import Loader from '../../../Loader/Loader';
import {ListItem} from './components/ListItem';
import type {INews} from "../../../utils/type";
import NoDataTitle from '../../NoDataTitle/NoDataTitle';
interface INewsAsideListProps {
  newsData: [];
  onGetNextNews: () => void;
  isMobile?: Boolean;
  isCalendarDataVisible?: Boolean;
  isLoading?: Boolean;
  isSearching?: Boolean;
  searchedNews: [];
  searchedNewsCount: Number;
  noNewsTitle: String;
  onNewsItemClick : (news: INews) => void;
}

export const NewsAsideList = ({
  newsData,
  onGetNextNews,
  isMobile,
  isCalendarDataVisible,
  isLoading,
  isSearching,
  searchedNews,
  noNewsTitle,
  searchedNewsCount,
    onNewsItemClick,
}: INewsAsideListProps) => {
  const [height, setHeight] = useState(
    isMobile ? window.innerHeight - 88 : window.innerHeight - 166
  );
  const [data, setData] = useState(newsData);
  useEffect(() => {
    setData(isSearching ? searchedNews : newsData);
  }, [isSearching, searchedNews, newsData]);
  const calculateHeight = height => {
    let mobileOtherHeight = 88;
    let desktopOtherHeight = 128;
    if (!isCalendarDataVisible) mobileOtherHeight += 38;
    if (!isCalendarDataVisible) desktopOtherHeight += 38;
    height && setHeight(isMobile ? height - mobileOtherHeight : height - desktopOtherHeight);
  };
  const handleHeightChange = e =>
    e && e.currentTarget && calculateHeight(e.currentTarget.innerHeight);
  const handleSearchLastNews = () =>
    !isLoading && searchedNewsCount > searchedNews.length && onGetNextNews(searchedNews.length);

  useEffect(() => {
    const winResp = new WindowResponse(handleHeightChange);
    calculateHeight(window.innerHeight);
    return () => winResp.destroy();
  });
  const loader = (
    <div className="indent align-center test-loader">
      <Loader size={24} />
    </div>
  );
  const loadItemsBoundary = '80%';
  const content =
    data && data.length > 0 ? (
      <EntitiesVirtualList
        className="news-aside-list test-aside-list"
        scrollClassName="news-aside__scroll"
        itemSize={isMobile ? 100 : 135}
        height={height}
        renderItem={ListItem}
        entities={data}
        loadItemsWhileScrollBoundaryIs={loadItemsBoundary}
        onLoadItemsScrollBoundary={isSearching ? handleSearchLastNews : onGetNextNews}
        onItemClick={onNewsItemClick}
      />
    ) : (
      <NoDataTitle className='test-news-aside-header__nonews' children={noNewsTitle}/>
    );

  return isLoading ? loader : content;
};
