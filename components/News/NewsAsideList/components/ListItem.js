//@flow
import React from 'react';
import {NewsItem} from '../../NewsItem/NewsItem';
import type {INews} from "../../../../utils/type";

interface ListItemProps {
  data: [];
  index: Number;
  style: any;
    onClick : (news: INews) => void;
}
export const ListItem = ({data : {entities , onClick}, index, style}: ListItemProps) =>
  entities[index] ? (
    <div style={style}>
      <NewsItem
        className="news-item--sidebar news-aside-list-item"
        createdAt={entities[index].date}
        coverImage={entities[index].coverImage}
        title={entities[index].title}
        body={entities[index].body}
        onClick={onClick}
        allNewsItemData={entities[index]}
      />
    </div>
  ) : null;
