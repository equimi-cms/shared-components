/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {ResultItem} from './ResultItem';

const MockItem = (props)=>(
  <ResultItem
    member={'Hickstead CSI2/5*'}
    horse={'placeholder-icon.svg'}
    show={'show'}
    level={'level'}
    countryName={'Ukraine'}
    place={'1st'}
    date={'2019/01/01'}
    classOfResult={'Million dollar class 170'}
    {...props}
  />
);

storiesOf('Shared/Results/ResultItem', module)
  .add('default without data', () => (
      <ResultItem/>
  ))
  .add('with data', () => (
    <MockItem/>
  ))
  .add('with type dashboard', () => (
    <MockItem type={'dashboard'}/>
  ))
  .add('with type sidebar', () => (
    <MockItem type={'sidebar'}/>
  ));

