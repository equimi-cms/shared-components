import React from 'react';
import {shallow} from 'enzyme';
import {ResultItem} from './ResultItem';

describe('ResultItem', () => {
  it('should renders with default type `dashboard`', () => {
    const component = shallow(
      <ResultItem/>
    );
    expect(component.find('.test-result-item').length).toBe(1);
    expect(component.find('.test-result-item').hasClass('m-result-item--dashboard')).toBeTruthy();

  });

  it('should render className when pass prop className `test`', () => {
    const component = shallow(
      <ResultItem className={'test'}/>
    );
    expect(component.find('.test-result-item').hasClass('test')).toBeTruthy();
  });

  it('should renders with type', () => {
    const component = shallow(
      <ResultItem type={'sidebar'}/>
    );
    expect(component.find('.test-result-item').hasClass('m-result-item--sidebar')).toBeTruthy();
  });

  it('should render flag when pass countryName `Ukraine`', () => {
    const component = shallow(
      <ResultItem countryName={'Ukraine'} flagPath={'/test/'}/>
    );
    expect(component.find('.test-flag').length).toBe(1);
    expect(component.find('.test-flag').props().src).toContain('/test/');
  });

  it('should render show and level fields', () => {
    const component = shallow(
      <ResultItem show={'show'} level={'level'}/>
    );
    expect(component.find('.test-result-item__show').props().children).toBe('show');
    expect(component.find('.test-result-item__level').props().children).toBe('level');
  });

  it('should render classOfResult and member with horse', () => {
    const component = shallow(
      <ResultItem classOfResult={'classOfResult'} member={'member'} horse={'placeholder-icon.svg'}/>
    );
    expect(component.find('.test-result-item__class').props().children).toBe('classOfResult');
    expect(component.find('.test-result-item__member').props().children).toBe('member');
    expect(component.find('.test-result-item__horse').props().children).toBe('placeholder-icon.svg');
  });

  it('should render place and date', () => {
    const component = shallow(
      <ResultItem place={'1'} date={'2019/01/01'}/>
    );
    expect(component.find('.test-result-item__place').props().children).toBe('1');
    expect(component.find('.test-result-item__date').props().children).toBe('01 Jan. 2019');
  });
});
