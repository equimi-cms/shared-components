//@flow
import React from 'react';
import './ResultItem.scss';
import classNames from 'classnames';
import Image from '../../../Image/Image';
import countriesData from '../../../data/countries';
import {getResultDate} from '../utils/utils';
import type {IResult} from '../../../../../scenes/Dashboard/scenes/Home/components/Results/utils/types';

export const ResultType = {
  dashboard: 'dashboard',
  sidebar: 'sidebar',
};

interface IResultItemProps {
  className?: String;
  type?: 'dashboard' | 'sidebar';
  member?: String;
  horse?: String;
  classOfResult?: String;
  level?: String;
  place?: String;
  date?: String;
  show?: String;
  countryName?: String;
  children?: React.Element;
  flagPath?: String;
  memberHorseSeparatorTitle?: String;
  onClick: (result: IResult) => void;
  allResultData: IResult;
}

export const ResultItem = ({
  className,
  type,
  member,
  horse,
  classOfResult,
  level,
  place,
  date,
  show,
  countryName,
  flagPath,
  memberHorseSeparatorTitle,
  onClick,
  allResultData,
  ...props
}: IResultItemProps) => {
  const preparedDate = getResultDate(date);
  const country = countriesData.find(country => country.name === countryName);
  const handleClick = () => {
    onClick(allResultData);
  };
  let flag, alpha3Code;
  if (country) {
    flag = country.flag;
    alpha3Code = country.alpha3Code;
  }
  return (
    <div
      {...props}
      className={classNames(
        'm-result-item',
        className,
        'test-result-item',
        type && 'test-result-item--' + type,
        type && 'm-result-item--' + type
      )}
      onClick={handleClick}
    >
      <div className="m-result-item__flag">
        {flag && alpha3Code && (
          <Image className="test-flag" alt={alpha3Code} src={flagPath + flag} />
        )}
      </div>
      <div className="m-result-item__left-cnt">
        <div className="m-result-item__show-and-level">
          <span className="m-result-item__show test-result-item__show">{show}</span>

          <span className="m-result-item__level test-result-item__level">{level}</span>
        </div>
        <div className="m-result-item__class test-result-item__class">{classOfResult}</div>
        <div className="m-result-item__member-and-horse">
          {member && <span className="member-and-horse__text test-result-item__member">{member}</span>}
          &nbsp;
          {member && horse && <span>{memberHorseSeparatorTitle}</span>}
          &nbsp;
          {horse && <span className="member-and-horse__text test-result-item__horse">{horse}</span>}
        </div>
      </div>
      <div className="m-result-item__right-cnt">
        <div className="m-result-item__place test-result-item__place">{place}</div>
        <div className="m-result-item__date test-result-item__date">{preparedDate}</div>
      </div>
    </div>
  );
};

export default React.memo(ResultItem);

ResultItem.defaultProps = {
  className: '',
  flagPath: '',
  type: ResultType.dashboard,
  member: '',
  horse: '',
  memberHorseSeparatorTitle: 'on',
  onClick: /* istanbul ignore next */ () => null,
  allResultData: {},
};
