import moment from 'moment';

export const getResultDate = date => {
  return date ? moment(date, 'YYYY/MM/DD').format('DD MMM. YYYY') : '';
};
