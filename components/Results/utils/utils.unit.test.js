import {getResultDate} from './utils';

describe('getResultDate', () => {
  it('should return `` when no date', () => {
    expect(getResultDate()).toBe('');
  });

  it('should return formatted string `01 Jan. 2019` when input date 2019/01/01', () => {
    expect(getResultDate('2019/01/01')).toBe('01 Jan. 2019');
  });
});
