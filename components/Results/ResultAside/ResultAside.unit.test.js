import React from 'react';
import {shallow} from 'enzyme';
import {ResultAside} from './ResultAside';

describe('ResultAside', () => {
  it('should renders', () => {
    const component = shallow(
      <ResultAside showAside/>
    );
    expect(component.find('Aside').length).toBe(1);
  });

  it('should render SearchBar and ResultsAsideList', () => {
    const component = shallow(
      <ResultAside showAside/>
    );
    expect(component.find('SearchBar').length).toBe(1);
    expect(component.find('ResultsAsideList').length).toBe(1);
  });

  it('should render children', () => {
    const component = shallow(
      <ResultAside showAside>
        <p className='test-p'>test</p>
      </ResultAside>
    );
    expect(component.find('.test-p').length).toBe(1);
  });
});
