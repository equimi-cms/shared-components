//@flow
import React, {useState, useEffect} from 'react';
import {Aside} from '../../Aside/Aside';
import {ResultsAsideList} from './ResultAsideList/ResultsAsideList';
import './ResultAside.scss';
import classNames from 'classnames';
import {SearchBar} from '../../../components/SearchBar/SearchBar';
import {useMobileStatus} from '../../../hooks/MobileStatus';
import {useTabletStatus} from '../../../hooks/TabletStatus';
import type {IResult} from '../../../../../scenes/Dashboard/scenes/Home/components/Results/utils/types';

export interface IAllResultsAsideProps {
  showAside: Boolean;
  className: String;
  resultsCount: Number;
  onClose?: () => void;
  onPlusButtonClick?: () => void;
  onGetNextResults?: () => void;
  onGetLastResults?: () => void;
  onSearchLastResults?: () => void;
  onSearchMoreResults?: () => void;
  timeLine: [];
  onSearchingStart?: () => void;
  onSearchingEnd?: () => void;
  searching?: Boolean;
  isLoading?: Boolean;
  results: {rows: Array, count: Number};
  searchedResults: [];
  searchedResultsCount: Number;
  contentWrapper?: React.Element;
  contentItemWrapper?: React.Element;
  contentItemWrapperProps?: any;
  inputPlaceholderTitle?: String;
  emptySearchDataTitle?: String;
  countOfEntitiesTitle?: String;
  title?: String;
  searchDataTitle?: String;
  noResultsTitle?: String;
  clearBtnText?: any;
  applyBtnText?: any;
  cancelBtnText?: any;
  resultOpt?: any;
  children?: React.Element;
  headerRight?: React.Element;
  onResultClick : (result : IResult) => void;
}

export const ResultAside = ({
  timeLine,
  results,
  className,
  showAside,
  onClose,
  onGetLastResults,
  onGetNextResults,
  onSearchLastResults,
  onSearchMoreResults,
  onSearchingStart,
  onSearchingEnd,
  searching,
  isLoading,
  contentWrapper,
  contentItemWrapper,
  contentItemWrapperProps,
  searchedResults,
  searchedResultsCount,
  inputPlaceholderTitle,
  emptySearchDataTitle,
  countOfEntitiesTitle,
  title,
  searchDataTitle,
  noResultsTitle,
  clearBtnText,
  applyBtnText,
  cancelBtnText,
  children,
  headerRight,
  resultOpt,
    onResultClick,
  ...rest
}: IAllResultsAsideProps): React.ElementType => {
  const isMobile = useMobileStatus();
  const isTablet = useTabletStatus();
  const [isCalendarOpen, setIsCalendarOpen] = useState(false);
  const [isCalendarDataVisible, setIsCalendarDataVisible] = useState(true);
  const initialDateState = {
    dateFrom: undefined,
    dateTo: undefined,
  };
  const [filteringData, setFilteringData] = useState({
    query: '',
    ...initialDateState,
    isTouched: false,
  });

  const handleClear = () =>
    filteringData.isTouched && setFilteringData({...filteringData, ...initialDateState});
  const handleCloseAside = () => {
    filteringData.isTouched && setFilteringData({query: '', ...initialDateState, isTouched: false});
    onClose();
  };
  const handleSearch = (query: String) =>
    setFilteringData({...filteringData, query: query, isTouched: true});
  const handlePickYear = (year: Number) =>
    setFilteringData({
      ...filteringData,
      dateFrom: new Date(year, 0),
      dateTo: new Date(year + 1, 0),
      isTouched: true,
    });
  const handlePickYearAndMonth = ({year, month}: Object<Number>) =>
    setFilteringData({
      ...filteringData,
      dateFrom: new Date(year, month),
      dateTo: new Date(year, month + 1),
      isTouched: true,
    });
  const handleToggleCalendar = (isOpen: Boolean) => setIsCalendarOpen(isOpen);
  const getFilteringValues = () => {
    let query, dateFrom, dateTo;
    if (filteringData.query) query = filteringData.query;
    if (filteringData.dateFrom) dateFrom = filteringData.dateFrom;
    if (filteringData.dateTo) dateTo = filteringData.dateTo;
    return {query, dateFrom, dateTo};
  };
  const loadResults = () => {
    let {query, dateFrom, dateTo} = getFilteringValues();

    filteringData.isTouched ? onSearchLastResults(query, dateFrom, dateTo) : onGetLastResults();
  };
  const loadMoreResults = offset => {
    let {query, dateFrom, dateTo} = getFilteringValues();

    filteringData.isTouched
      ? onSearchMoreResults(offset, query, dateFrom, dateTo)
      : onGetNextResults();
  };
  useEffect(() => {
    filteringData.isTouched ? !searching && onSearchingStart() : searching && onSearchingEnd();
    filteringData.isTouched &&
      !filteringData.query &&
      !filteringData.dateFrom &&
      !filteringData.dateTo &&
      setFilteringData({isTouched: false});
    loadResults();
    // eslint-disable-next-line
  }, [filteringData]);

  return (
    <>
      <Aside
        open={showAside}
        withCustomScroll={false}
        className={classNames([
          'test-aside-all-results aside-all-results ',
          showAside && 'freezing-of-main-scroll',
          (isMobile || isTablet) && isCalendarOpen && 'aside-all-results__overlay',
          className,
        ])}
        title={title}
        isBackBtn={true}
        onBack={handleCloseAside}
        onOverlay={!isCalendarOpen ? handleCloseAside : null}
        backBtnType={isMobile ? 'arrowLeft' : 'close'}
        contentType="no-padding"
        headerRight={headerRight}
        {...rest}
      >
        <SearchBar
          isMobile={isMobile}
          isTablet={isTablet}
          onSearch={handleSearch}
          onPickMonthAndYear={handlePickYearAndMonth}
          onPickYear={handlePickYear}
          timeLine={timeLine}
          onToggleCalendar={handleToggleCalendar}
          setIsCalendarDataVisible={setIsCalendarDataVisible}
          onClear={handleClear}
          inputPlaceholder={inputPlaceholderTitle}
          emptySearchDataTitle={emptySearchDataTitle}
          searchDataTitle={searchDataTitle}
          countOfEntitiesTitle={countOfEntitiesTitle}
          clearBtnText={clearBtnText}
          applyBtnText={applyBtnText}
          cancelBtnText={cancelBtnText}
        />
        <ResultsAsideList
          isCalendarDataVisible={isCalendarDataVisible}
          isMobile={isMobile}
          noResultsTitle={noResultsTitle}
          onGetNextResults={loadMoreResults}
          isSearching={filteringData.isTouched}
          itemClass="m-result-aside-list__item"
          listItemClass="m-result-aside-list"
          contentWrapper={contentWrapper}
          contentItemWrapper={contentItemWrapper}
          contentItemWrapperProps={contentItemWrapperProps}
          resultsData={results.rows}
          resultsCount={results.count}
          isLoading={isLoading}
          searchedResults={searchedResults}
          searchedResultsCount={searchedResultsCount}
          resultOpt={resultOpt}
          onResultClick={onResultClick}
        />
      </Aside>
      {children}
    </>
  );
};
/* istanbul ignore next */
ResultAside.defaultProps = {
  timeLine: [],
  results: {rows: [], count: 0},
  searching: false,
  showAside: false,
  contentWrapper: props => props.children,
  contentItemWrapper: props => props.children,
  onClose: () => null,
  onPlusButtonClick: () => null,
  onGetNextResults: () => null,
  onGetLastResults: () => null,
  onSearchMoreResults: () => null,
  onSearchLastResults: () => null,
  onSearchingStart: () => null,
  onSearchingEnd: () => null,
  onResultClick : () => null,
};
