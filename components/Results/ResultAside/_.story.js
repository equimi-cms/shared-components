/* eslint-disable import/first */
import React from 'react';
import {storiesOf} from '@storybook/react';
import {ResultAside} from './ResultAside';

const mockResults = {
  'rows': [{
    'id': '53c80bde-0344-4f8d-8d39-8b8b2807b37a',
    'memberName': '4',
    'horseName': '4',
    'className': '4',
    'level': '4',
    'place': '4',
    'date': '2019-11-11',
    'showName': '4',
    'country': 'Afghanistan',
    'description': '',
    'isPublished': true,
    'coverImage': {},
    'videos': null,
    'isDraftViewed': false,
    'created': '2019-09-03T10:06:15.832Z',
    'horses': [],
    'members': [],
  }, {
    'id': 'e72a23fe-f24a-4f48-a36d-a1a4a3583122',
    'memberName': '5',
    'horseName': '5',
    'className': '5',
    'level': '5',
    'place': '5',
    'date': '2019-11-11',
    'showName': '5',
    'country': 'Albania',
    'description': '',
    'isPublished': true,
    'coverImage': {},
    'videos': null,
    'isDraftViewed': false,
    'created': '2019-09-03T10:21:52.537Z',
    'horses': [],
    'members': [],
  }, {
    'id': '34c9be60-e52a-479a-af66-2171277f06a5',
    'memberName': '2',
    'horseName': '2',
    'className': '2',
    'level': '2',
    'place': '2',
    'date': '1111-11-11',
    'showName': '2',
    'country': 'Afghanistan',
    'description': '',
    'isPublished': true,
    'coverImage': {},
    'videos': null,
    'isDraftViewed': false,
    'created': '2019-09-03T08:16:59.174Z',
    'horses': [],
    'members': [],
  }, {
    'id': 'a0a5302c-9b61-4d01-b343-7f5f83d0c68f',
    'memberName': '1',
    'horseName': '1',
    'className': '12',
    'level': '21',
    'place': '23',
    'date': '1111-11-11',
    'showName': '1',
    'country': 'Afghanistan',
    'description': '',
    'isPublished': true,
    'coverImage': {},
    'videos': null,
    'isDraftViewed': false,
    'created': '2019-09-03T12:30:59.374Z',
    'horses': [],
    'members': [],
  }, {
    'id': '81b6af9e-49ac-4305-98b7-7fdca721106b',
    'memberName': 'asd',
    'horseName': 'asd',
    'className': 'asd',
    'level': 'asd',
    'place': 'asd',
    'date': '1111-11-10',
    'showName': '1',
    'country': 'Algeria',
    'description': '',
    'isPublished': true,
    'coverImage': {},
    'videos': null,
    'isDraftViewed': false,
    'created': '2019-06-21T10:55:15.570Z',
    'horses': [],
    'members': [],
  }, {
    'id': '4d91bfb0-e77c-4662-adbb-ba992fad029f',
    'memberName': 'Hickstead CSI2/5*',
    'horseName': '1',
    'className': 'Million dollar class 170',
    'level': '1',
    'place': '1st',
    'date': '1111-11-10',
    'showName': '1',
    'country': 'Afghanistan',
    'description': 'asd',
    'isPublished': true,
    'coverImage': {},
    'videos': null,
    'isDraftViewed': false,
    'created': '2019-07-05T11:48:45.457Z',
    'horses': [],
    'members': [],
  }], 'count': 6, 'countByDate': [{'1111-11': 4}, {'2019-11': 2}],
};

storiesOf('Shared/Results/ResultAside', module)
  .add('default hidden', () => (
    <ResultAside/>
  ))
  .add('visible', () => (
    <ResultAside showAside/>
  ))
  .add('with title', () => (
    <ResultAside
      showAside
      title={'Title'}
    />
  ))
  .add('isLoading', () => (
    <ResultAside
      showAside
      title={'Title'}
      isLoading
    />
  ))
  .add('with data', () => (
    <div>
      <style dangerouslySetInnerHTML={{
        __html:
          `
          .m-search-aside-list {
            height: calc(100% - 62px);
            width: 100%;
          }
        `,
      }}
      />
      <ResultAside
        showAside
        title={'Title'}
        results={mockResults}
        inputPlaceholderTitle={'placeholder'}
        noResultsTitle={'no results'}
        clearBtnText={'clear'}
        applyBtnText={'apply'}
        cancelBtnText={'cancel'}
        headerRight={<p>Right content</p>}
      />
    </div>

  ));


