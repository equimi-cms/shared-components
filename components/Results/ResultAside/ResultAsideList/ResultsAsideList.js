//@flow
import React, {useState, useEffect} from 'react';
import Loader from '../../../../Loader/Loader';
import classNames from 'classnames';
import Scroll from '../../../../components/Scroll/Scroll';
import ResultItem from '../../ResultItem/ResultItem';
import type {IResult} from "../../../../../../components/sections/SectionResults/utils/types";

interface IResultsAsideListProps {
  resultsData?: [];
  resultsCount?: Number;
  isLoading?: Boolean;
  isSearching?: Boolean;
  searchedResults?: [];
  searchedResultsCount?: Number;
  itemClass?: String;
  listClass?: String;
  listItemClass?: String;
  contentWrapper?: React.Element;
  contentItemWrapper?: React.Element;
  contentItemWrapperProps?: any;
  onLoadMore?: () => void;
  onGetNextResults?: () => void;
  noResultsTitle?: String;
  resultOpt?:any;
  onResultClick : (result : IResult) => void;
}

export const ResultsAsideList = ({
  resultsData,
  resultsCount,
  onGetNextResults,
  contentWrapper,
  contentItemWrapper,
  contentItemWrapperProps,
  isLoading,
  isSearching,
  searchedResults,
  searchedResultsCount,
  itemClass,
  listClass,
  listItemClass,
  noResultsTitle,
  resultOpt,
    onResultClick,
}: IResultsAsideListProps) => {
  const [data, setData] = useState(resultsData);
  useEffect(() => {
    setData(isSearching ? searchedResults : resultsData);
  }, [isSearching, searchedResults, resultsData]);

  const ContentItemWrapper = contentItemWrapper;
  const handleSearchLastResults = () =>
    !isLoading &&
    searchedResultsCount > searchedResults.length &&
    onGetNextResults(searchedResults.length);

  const renderData = data.map(itemData => {
    const {memberName, horseName, id, className, level, place, date, showName, country} = itemData;
    return (
      <ContentItemWrapper {...contentItemWrapperProps} itemData={itemData} key={id}>
        <ResultItem
          className={itemClass}
          member={memberName}
          horse={horseName}
          classOfResult={className}
          level={level}
          place={place}
          date={date}
          show={showName}
          countryName={country}
          type="sidebar"
          allResultData={itemData}
          onClick={onResultClick}
          {...resultOpt}
        />
      </ContentItemWrapper>
    );
  });
  const loader = (
    <div className="indent align-center test-loader">
      <Loader size={24} />
    </div>
  );
  const onScrollBoundary = ([isReached]) =>
    isReached && isSearching
      ? searchedResults.length < searchedResultsCount && handleSearchLastResults()
      : resultsData && resultsData.length < resultsCount && onGetNextResults(resultsData.length);
  const loadItemsBoundary = '80%';
  const ContentWrapper = contentWrapper;
  const content = (
    <div className={classNames(['m-search-aside-list test-aside-list', listClass && listClass])}>
      <Scroll boundary={loadItemsBoundary} onScrollBoundary={onScrollBoundary}>
        <ContentWrapper>
          {data && data.length > 0 ? (
            <div className={listItemClass}>{renderData}</div>
          ) : (
            <div className="aside-all-results__header_noresults test-results-aside-header__noresults">
              {noResultsTitle}
            </div>
          )}
        </ContentWrapper>
      </Scroll>
    </div>
  );

  return isLoading ? loader : content;
};

ResultsAsideList.defaultProps = {
  resultsData: [],
  searchedResults: [],
  contentWrapper:()=>null,
  onGetNextResults:()=>null,
  onLoadMore:()=>null,
  onResultClick : () => null,
};
