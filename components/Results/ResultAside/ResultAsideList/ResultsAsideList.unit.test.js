import React from 'react';
import {shallow} from 'enzyme';
import {ResultsAsideList} from './ResultsAsideList';

describe('ResultsAsideList', () => {
  it('should renders', () => {
    const component = shallow(
      <ResultsAsideList/>
    );
    expect(component.hasClass('test-aside-list')).toBeTruthy();
  });

  it('should render no results title', () => {
    const mockNoResultsTitle = 'No results';
    const component = shallow(
      <ResultsAsideList resultsData={[]} noResultsTitle={mockNoResultsTitle}/>
    );
    expect(component.find('.test-results-aside-header__noresults').props().children).toBe(mockNoResultsTitle);
  });
});
