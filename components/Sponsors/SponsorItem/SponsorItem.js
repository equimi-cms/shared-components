//@flow
import React from "react";
import "./SponsorItem.scss";
import classNames from "classnames";
import imagePlaceholder from "./imgs/placeholder.svg";
import {Image} from "../../../Image/Image";
import type {ISponsor} from "../../../../../components/sections/SectionSponsors/utils/types";

interface ISponsorItemProps {
  className?: String;
  type?: "dashboard" | "sidebar";
  name?: String;
  tagline?: String;
  imgSrc?: String;
  children?: React.Element;
  onClick? : (sponsor: ISponsor) => void;
  allSponsorData : ISponsor;
}

export const SponsorItem = ({
  className,
  type,
  name,
  imgSrc,
  tagline,
    onClick,
    allSponsorData,
  ...props
}: ISponsorItemProps) => {
  const handleClick = () => {
    onClick(allSponsorData)
  };
  return (
    <div
      {...props}
      className={classNames(
        "m-sponsor-item test-sponsor-item",
        className,
        type && "m-sponsor-item--" + type
      )}
      onClick={handleClick}
    >
      <div className="m-sponsor-item__img-cnt">
        {imgSrc ? (
          <Image className="m-sponsor-item__img" alt={name} src={imgSrc} />
        ) : (
          <div className="m-sponsor-item__img m-sponsor-item__placeholder">
            <Image src={imagePlaceholder} alt={name} />
          </div>
        )}
      </div>
      <div className={classNames(["m-sponsor-item__text-cnt"])}>
        <h6 className="m-sponsor-item__name">{name}</h6>
        {tagline && <div className="m-sponsor-item__tagline">{tagline}</div>}
      </div>
    </div>
  );
};

export default React.memo(SponsorItem);

SponsorItem.defaultProps = {
  className: "",
  type: "dashboard",
  title: "",
  imgSrc: "",
  children: "",
  onClick : () => null,
  allSponsorData : {}
};
