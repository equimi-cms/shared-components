// @flow
import React, {Fragment, PureComponent} from "react";
import SponsorItem from "../SponsorItem/SponsorItem";
import "./SponsorAside.scss";
import {AsideSearchCnt} from "../../Aside/components/AsideSearchCnt/AsideSearchCnt";
import Button from "../../AsideButton/Button";
import {Aside} from "../../Aside/Aside";
import {getImageSrcWithSize} from "../../../utils/utils";
import {LoadMore} from "../../../utils/LoadMore/LoadMore";

interface ISponsorAsideProps {
  className?: String;
  show?: Boolean;
  onClose?: () => void;
  onSelectItem?: () => void;
  searchPlaceholder: String;
  asideTitle: String;
  noEntitiesTitle: String;
  isMobile?: Boolean;
  searchSponsors: () => void;
  loadMoreSearchSponsors: () => void;
  onSponsorClick: (sponsor: any) => void;
}

interface ISponsorAsideState {
  showLoader: Boolean;
  searchData: {rows: Array, count: Number};
}

export class SponsorAside extends PureComponent<ISponsorAsideProps, ISponsorAsideState> {
  constructor() {
    super();
    this.state = {
      showLoader: false,
      searchData: {rows: [], count: 0},
    };
    this.searchQuery = "";
    this.lm = new LoadMore();
  }

  componentDidMount() {
    this.search(this.searchQuery);
  }

  componentDidUpdate(prevProps: ISponsorAsideProps, _prevState: ISponsorAsideState, _sn: any) {
    if (this.props.show && prevProps.show !== this.props.show) {
      this.search("");
    }
  }

  search = (query: string) => {
    this.searchQuery = query;
    this.lm.reset();

    this.setState({showLoader: true}, () =>
      this.props
        .searchSponsors(query)
        .then(({rows = [], count}) => {
          this.lm.setAllItemsCount(count).setLoadedItemsCount(rows.length);
          return this.setState({searchData: {rows, count}});
        })
        .then(() => this.setState({showLoader: false}))
    );
  };

  onLoadMore = () => {
    if (this.state.showLoader || !this.lm.canLoadMore()) return;
    this.setState({showLoader: true}, () => {
      this.props
        .loadMoreSearchSponsors(this.searchQuery, this.lm.getNextOffset())
        .then(({rows}) => {
          this.lm.setLoadedItemsCount(rows.length);
          return this.setState((prevState) => ({
            searchData: {rows: [...prevState.searchData.rows, ...rows]},
          }));
        })
        .then(() => this.setState({showLoader: false}));
    });
  };

  render() {
    const {showLoader, searchData} = this.state;
    const {
      show,
      searchPlaceholder,
      noEntitiesTitle,
      onPlusButtonClick,
      usePlusButton,
      isMobile,
      asideTitle,
      onClose,
      className,
      onSponsorClick,
    } = this.props;
    const searchList = searchData.rows
      ? searchData.rows.map((data) => {
          const {id, name, tagline, logoImage} = data;
          return (
            <SponsorItem
              key={id}
              imgSrc={getImageSrcWithSize(logoImage, "160x160")}
              name={name}
              tagline={tagline}
              type="aside"
              onClick={onSponsorClick}
              allSponsorData={data}
            />
          );
        })
      : [];
    return (
      <Fragment>
        <Aside
          open={show}
          className={`test-aside-sponsors ${className}`}
          title={asideTitle}
          onOverlay={onClose}
          headerLeft={<Button type={isMobile ? "arrowLeft" : "close"} onClick={onClose} />}
          headerRight={
            usePlusButton && (
              <Button
                className="news-button__add test-sponsor-aside-btn-add"
                onClick={onPlusButtonClick}
                type="plus"
              />
            )
          }
          contentType="no-padding"
          withCustomScroll={false}
        >
          <AsideSearchCnt
            isMobile={isMobile}
            isLoading={showLoader}
            placeholder={searchPlaceholder}
            data={searchList}
            noEntitiesTitle={noEntitiesTitle}
            itemClass="m-sponsor-aside-list__item"
            listItemClass="m-sponsor-aside__list"
            onSearch={this.search}
            onLoadMore={this.onLoadMore}
          />
        </Aside>
      </Fragment>
    );
  }
}

SponsorAside.defaultProps = {
  show: false,
  searchPlaceholder: "",
  noEntitiesTitle: "",
  asideTitle: "",
  usePlusButton: false,
  isMobile: false,
  onPlusButtonClick: () => null,
  onClose: () => null,
  onSelectItem: () => null,
  onSponsorClick: () => null,
};
