//@flow
import React, { memo, Fragment } from "react";

interface Props {
  text?: String;
  contentWrapper?: React.Element;
}

/**
 * Replace \n to <br/> in text string
 *
 * @param children
 * @returns {*}
 */
const NlToBr = ({ text = "", contentWrapper: ContentWrapper }: Props) => {
  const arr = text ? text.split("\n") : [];
  return arr.map((item, index) => (
    <Fragment key={index}>
      <ContentWrapper children={item} />
      {arr.length - 1 !== index ? <br /> : null}
    </Fragment>
  ));
};
NlToBr.defaultProps = {
  text: "",
  contentWrapper: ({ children }) => children
};
export default memo(NlToBr);
