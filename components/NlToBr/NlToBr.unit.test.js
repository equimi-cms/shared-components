import React from "react";
import NlToBr from "./NlToBr";
import { shallow,mount } from "enzyme";

describe("NlToBr", () => {
  it("should not render when no text", () => {
    const component = shallow(<NlToBr text={null} />);
    expect(component.find("Fragment").length).toBe(0);
  });

  it("should render pure text", () => {
    const component = shallow(<NlToBr text={"text1"} />);
    expect(component.find("Fragment").length).toBe(1);
    expect(component.props().children.includes("<br/>")).toBeFalsy();
  });

  it("should render with ContentWrapper", () => {
    const component = mount(<NlToBr contentWrapper={({children})=>(<p className='js-wrapper'>{children}</p>)} text={"text1"} />);
    expect(component.find(".js-wrapper").length).toBe(1);
    expect(component.find(".js-wrapper").text()).toBe('text1');
  });

  it("should render 2 `Fragment` and first `Fragment` with br tag", () => {
    const component = shallow(<NlToBr text={"text1 \n text2"} />);
    expect(component.find("Fragment").length).toBe(2);
    expect(
      component
        .find("Fragment")
        .at(1)
        .props()
        .children.includes("<br/>")
    ).toBeFalsy();
  });
});
