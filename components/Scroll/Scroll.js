import React, {useMemo, useCallback} from "react";
import "./Scroll.scss";
import PropTypes from "prop-types";
import classNames from "classnames";
import {Scrollbars} from "react-custom-scrollbars";

const horizontalTrack = (props) => <div {...props} className="m-scroll__track-h" />;

const verticalTrack = (props) => <div {...props} className="m-scroll__track-v" />;

export const Scroll = ({
  children,
  boundary,
  scrollRef,
  onScrollBoundary,
  onScrollGone,
  className,
  onScrollBoundaryWithKeys,
  allowHorizontalTrack,
  ...props
}) => {
  const memoClass = useMemo(() => classNames(["m-scroll test", className]), [className]);

  const _onScrollFrame = useCallback(
    ({top, scrollTop}) => {
      const _boundary: Array<string> = Array.isArray(boundary) ? boundary : [boundary];
      const res = [];
      const resWithKeys = [];
      const isPx = (val) => val.includes("px");
      _boundary.forEach((el) => {
        const val = parseFloat(el);
        isPx(el) ? res.push(scrollTop >= val) : res.push(top >= val / 100);
      });
      _boundary.forEach((el) => {
        resWithKeys.push({[el]: res[_boundary.indexOf(el)]});
      });
      onScrollBoundaryWithKeys.call(null, resWithKeys);
      onScrollBoundary.call(null, res);
    },
    [boundary, onScrollBoundary, onScrollBoundaryWithKeys]
  );

  const _onUpdate = useCallback(
    ({clientHeight, scrollHeight}) => {
      if (clientHeight === scrollHeight) onScrollGone();
    },
    [onScrollGone]
  );

  return (
    <Scrollbars
      {...props}
      className={memoClass}
      ref={scrollRef}
      renderTrackVertical={verticalTrack}
      renderTrackHorizontal={allowHorizontalTrack ? horizontalTrack : () => <div />}
      onScrollFrame={_onScrollFrame}
      onUpdate={_onUpdate}
    >
      {children}
    </Scrollbars>
  );
};

export default React.memo(Scroll);

Scroll.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  boundary: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.string), PropTypes.string]),
  onScrollBoundary: PropTypes.func,
  onScrollBoundaryWithKeys: PropTypes.func,
  onScrollGone: PropTypes.func,
  allowHorizontalTrack: PropTypes.bool,
  scrollRef: PropTypes.object,
};
Scroll.defaultProps = {
  className: "",
  /**
   * can be string or array of string
   * 100% or [100%,80%]
   */
  boundary: "100%", //%,px
  /**
   * when boundary is array of [80,50]
   * the onScrollBoundary will be called with array of [false,true] when reach the boundary 50
   * the onScrollBoundary will be called with array of [true,true] when reach the boundary 80
   */
  onScrollBoundary: () => null,
  /**
   * called when scroll is hidden
   */
  /**
   * new version of deprecated onScrollBoundary()
   * @return array of key: values where key is boundary and value is taken or not
   */
  onScrollBoundaryWithKeys: () => null,
  onScrollGone: () => null,
};
