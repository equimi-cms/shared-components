import React from 'react';
import {shallow} from 'enzyme';
import {Scroll} from './Scroll';

describe('Scroll', () => {
  it('should renders', () => {
    const component = shallow(
      <Scroll>Adipisicing elit tempor veniam fugiat ut dolor velit cupidatat</Scroll>
    );
    expect(component.hasClass('test')).toBeTruthy();
  });
  it('should render children while `children` props has been provided', () => {
    const component = shallow(
      <Scroll>
        <div className="test-children">
          Adipisicing elit tempor veniam fugiat ut dolor velit cupidatat culpa commodo qui tempor
          pariatur. Occaecat cupidatat sit sint laborum incididunt veniam consectetur irure. Eu ad
          voluptate aliquip eiusmod exercitation in adipisicing proident in deserunt sint. Amet
          dolor elit consequat anim non nostrud laboris adipisicing ullamco ullamco. Exercitation
          sunt aliquip voluptate qui in pariatur dolore sunt sunt. Lorem ullamco voluptate deserunt
          culpa Lorem ut aute quis et Lorem culpa irure consequat. Cillum amet cillum proident
          nostrud irure velit adipisicing irure. Ipsum anim fugiat consequat ea est in in. Sint est
          do dolor aliqua non. Deserunt reprehenderit aliqua tempor nulla laboris minim incididunt
          occaecat ipsum non qui excepteur consequat. Voluptate ad et mollit dolore adipisicing duis
          mollit nisi ex ad. Aliqua adipisicing sunt eiusmod nulla fugiat anim. Sit exercitation sit
          dolore excepteur ut incididunt commodo consequat enim. Laboris est culpa elit adipisicing
          fugiat quis mollit ex voluptate amet consectetur consectetur. Ad nisi culpa mollit ea
          adipisicing cupidatat et eu et do amet exercitation cillum esse. Exercitation sint do
          consequat cillum minim enim. Et magna sit ex minim velit ut irure eiusmod labore irure. Et
          voluptate ad veniam ipsum ipsum fugiat. Anim in amet esse ea dolore ipsum dolor irure.
          Exercitation minim nulla duis ipsum cupidatat mollit labore pariatur amet qui nisi magna
          ea laboris. Tempor voluptate sint nisi deserunt esse pariatur. Minim voluptate
          reprehenderit fugiat nulla aute proident anim laborum.
        </div>
      </Scroll>
    );
    expect(component.find('.test-children')).toHaveLength(1);
  });
});
