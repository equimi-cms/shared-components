import React from 'react';
import {shallow} from 'enzyme/build';
import BackButton from './BackButton.js';

describe('Menu', () => {
  it('should render', () => {
    const component = shallow(<BackButton />);
    expect(component.find('.js-d-back').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<BackButton className={'test'} />);
    expect(
      component
        .find('.js-d-back')
        .props()
        .className.includes('test')
    ).toBeTruthy();
  });
});
