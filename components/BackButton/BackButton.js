import React from 'react';
import './BackButton.scss';
import classNames from 'classnames';
import backIcon from './imgs/arrow.svg';
import {renderTypesAsClassNames} from '../../utils/utils';
import SvgInlineImage from '../../Image/SvgInlineImage/SvgInlineImage';

export const BackButtonType = {
  inHeader: "in-header",
  black: "black",
  white: "white",
};

interface IBackButtonProps {
  className?: String;
  type?: String;
  as?: String;
}

const BackButton = ({
  className,
  type,
  as: RenderComponent,
  ...props
}: IBackButtonProps) => {
  return (
    <RenderComponent
      {...props}
      className={classNames(
        "d-back js-d-back",
        className,
        renderTypesAsClassNames(type, "d-back--")
      )}
    >
      <SvgInlineImage src={backIcon} />
    </RenderComponent>
  );
};

BackButton.defaultProps = {
  as: "div",
};

export default React.memo(BackButton);
