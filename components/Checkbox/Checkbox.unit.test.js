import React from "react";
import {Checkbox} from "./Checkbox";
import {shallow} from "enzyme";

describe('Checkbox', () => {
  it('should renders', () => {
    const component = shallow(<Checkbox />);
    expect(component.hasClass('common-checkbox')).toBeTruthy();
  });
  it('should renders with label', () => {
    const component = shallow(<Checkbox>Label</Checkbox>);
    expect(component.find('.common-checkbox__text').text().trim()).toBe('Label');
  });

  it('should renders with type block', () => {
    const component = shallow(<Checkbox type="block">Label</Checkbox>);
    expect(component.find('label').hasClass('common-checkbox--block')).toBeTruthy();
  });

  it('should renders with class `--disabled` when set prop `disabled` ', () => {
    const component = shallow(<Checkbox disabled>Label</Checkbox>);
    expect(component.find('.common-checkbox').hasClass('common-checkbox--disabled')).toBeTruthy();
  });

  it('should renders with class `--not-active` when set prop `isActive` false ', () => {
    const component = shallow(<Checkbox isActive={false}>Label</Checkbox>);
    expect(component.find('.common-checkbox').hasClass('common-checkbox--not-active')).toBeTruthy();
  });

  it('should renders with class "is-active" when set checked true', () => {
    const component = shallow(<Checkbox checked={true}>Label</Checkbox>);
    expect(component.find('input').props().checked).toBeTruthy();
    expect(component.find('label').hasClass('is-active')).toBeTruthy();
  });

  it('should call onChange fn when checked', () => {
    const fn = jest.fn();
    const component = shallow(<Checkbox onChange={fn}>Label</Checkbox>);
    component.find('label input').simulate('change', {
      target: {
        checked: true,
      },
    });
    expect(fn).toBeCalled();
  });

  it('should not call onChange fn when isActive false', () => {
    const fn = jest.fn();
    const component = shallow(
      <Checkbox isActive={false} onChange={fn}>
        Label
      </Checkbox>
    );
    component.find('label input').simulate('change', {
      target: {
        checked: true,
      },
    });
    expect(fn).not.toBeCalled();
  });
});
