import React from "react";
import { shallow } from "enzyme/build";
import { CheckboxWithCounter } from "./CheckboxWithCounter";

describe("CheckboxWithCounter", () => {
  it("should render", () => {
    const component = shallow(<CheckboxWithCounter />);
    expect(component.find(".test-content").length).toBe(1);
  });

  it("should render with counter", () => {
    const component = shallow(<CheckboxWithCounter count="2" />);
    expect(component.find(".test-counter").text()).toBe("2");
  });
});
