//@flow
import React from "react";
import "./CheckboxWithCounter.scss";
import Checkbox from "../Checkbox";
import type { ICheckboxProps } from "../Checkbox";

interface ICheckboxWithCounterProps extends ICheckboxProps {
  count: number;
}

export const CheckboxWithCounter = ({
  children,
  count,
  ...otherProps
}: ICheckboxWithCounterProps) => {
  return (
    <Checkbox {...otherProps}>
      <div className="cwc-content test-content">
        <div className="cwc-content__label">{children}</div>
        <div className="cwc-content__counter test-counter">{count}</div>
      </div>
    </Checkbox>
  );
};

export default React.memo(CheckboxWithCounter);

CheckboxWithCounter.defaultProps = {};
