//@flow
import React, {useState, useEffect, useCallback} from "react";
import "./Checkbox.scss";
import classNames from "classnames";
import {renderTypesAsClassNames} from "../../utils/utils";
import checkIcon from "./imgs/checkmark.svg";
import InlineSVG from "react-inlinesvg";

export interface ICheckboxProps {
  type?: ["" | "block" | "setting" | "mod-1" | "mod-2"];
  children?: any;
  value?: any;
  inputType?: "radio" | "checkbox";
  className?: String;
  onChange?: (e: any, value: any) => void;
  onClick?: (value: any) => void;
  checked?: Boolean;
  disabled?: Boolean;
  isActive?: Boolean;
  labelProps?: {};
}

export const Checkbox = ({
  type,
  children,
  value,
  inputType,
  className,
  onChange,
  onClick,
  checked,
  disabled,
  isActive,
  labelProps,
  ...props
}: ICheckboxProps) => {
  const [_checked, _setChecked] = useState(checked);

  const _onChange = useCallback(
    (e) => {
      if (!isActive) return;

      _setChecked(e.target.checked);
      onChange(e, value);
    },
    [_setChecked, isActive, onChange, value]
  );
  const _onClick = useCallback(() => onClick(value), [onClick, value]);

  useEffect(() => _setChecked(checked), [checked]);

  return (
    <label
      {...labelProps}
      className={classNames(
        "common-checkbox",
        className,
        renderTypesAsClassNames(type, "common-checkbox--"),
        _checked && "is-active",
        disabled && "common-checkbox--disabled",
        !isActive && "common-checkbox--not-active"
      )}
    >
      <input
        {...props}
        checked={_checked}
        onChange={_onChange}
        onClick={_onClick}
        type={inputType}
      />
      <span className="common-checkbox__img">
        <InlineSVG className="common-checkbox__img-icon" src={checkIcon} />
      </span>
      {children && <div className="common-checkbox__text">{children}</div>}
    </label>
  );
};

Checkbox.defaultProps = {
  type: null,
  disabled: false,
  isActive: true,
  labelProps: {},
  value: null,
  checked: false,
  inputType: "checkbox",
  onChange: () => null,
  onClick: () => null,
};

export default React.memo(Checkbox);
