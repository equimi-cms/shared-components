import React from 'react';
import {shallow} from 'enzyme/build';
import {NotifyBannerButton} from './NotifyBannerButton';

describe('NotifyBannerButton', () => {
  it('should render', () => {
    const component = shallow(<NotifyBannerButton/>);
    expect(component.find('.js-nb-btn').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<NotifyBannerButton className='test'/>);
    expect(component.find('.js-nb-btn').hasClass('test')).toBeTruthy();
  });

  it('should render with children', () => {
    const component = shallow(<NotifyBannerButton>test</NotifyBannerButton>);
    expect(component.find('.js-nb-btn').props().children).toBe('test');
  });
});
