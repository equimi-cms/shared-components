//@flow
import React from "react";
import "./NotifyBannerButton.scss";
import classNames from "classnames";

interface INotifyBannerButtonProps {
  className: String;
  children: React.Children;
}

export const NotifyBannerButton = ({
  className,
  children,
  ...props
}: INotifyBannerButtonProps) => {
  return (
    <button {...props} className={classNames("g-nb-btn js-nb-btn", className)}>
      {children}
    </button>
  );
};

export default React.memo(NotifyBannerButton);

NotifyBannerButton.defaultProps = {};
