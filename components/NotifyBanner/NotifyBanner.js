//@flow
import React from "react";
import "./NotifyBanner.scss";
import classNames from "classnames";

interface INotifyBannerProps {
  className?: String;
  title: String;
  description: String;
  rightContent?: React.Children;
}

export const NotifyBanner = ({
  className,
  title,
  description,
  rightContent
}: INotifyBannerProps) => {
  return (
    <section className={classNames("g-nb js-g-nb", className)}>
      <div className="g-nb__cnt">
        <div className="g-nb__content">
          <h1 className="g-nb__title js-g-nb-title">{title}</h1>
          <p className="g-nb__desc js-g-nb-desc">{description}</p>
        </div>
        <div className="g-nb__content-right">{rightContent}</div>
      </div>
    </section>
  );
};

export default React.memo(NotifyBanner);

NotifyBanner.defaultProps = {};
