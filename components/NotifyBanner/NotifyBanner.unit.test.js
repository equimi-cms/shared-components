import React from 'react';
import {shallow} from 'enzyme/build';
import {NotifyBanner} from './NotifyBanner';

describe('NotifyBanner', () => {
  it('should render', () => {
    const component = shallow(<NotifyBanner/>);
    expect(component.find('.js-g-nb').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<NotifyBanner className='test'/>);
    expect(component.find('.js-g-nb').hasClass('test')).toBeTruthy();
  });

  it('should render with title', () => {
    const component = shallow(<NotifyBanner title={<p className='test'>test'</p>}/>);
    expect(component.find('.test').length).toBe(1);
  });

  it('should render with description', () => {
    const component = shallow(<NotifyBanner description={<p className='test'>test'</p>}/>);
    expect(component.find('.test').length).toBe(1);
  });

  it('should render with rightContent', () => {
    const component = shallow(<NotifyBanner rightContent={<p className='test'>test'</p>}/>);
    expect(component.find('.test').length).toBe(1);
  });
});
