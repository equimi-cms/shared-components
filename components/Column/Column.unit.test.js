import React from 'react';
import Column from './Column';
import {shallow} from 'enzyme';

describe('Column', () => {
  it('should renders', () => {
    const Main = shallow(<Column>test</Column>);
    expect(Main.find('.m-column').text()).toBe('test');
  });

  it('should renders with type when set prop type', () => {
    const Main = shallow(<Column type={'content'}>test</Column>);
    expect(Main.find('.m-column').hasClass('m-column--type-content')).toBeTruthy();
  });
});
