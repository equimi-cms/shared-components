import React from 'react';
import './Column.scss';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {renderTypesAsClassNames} from '../../utils/utils';

export const Column = ({type, children, className, ...props}) => {
  return (
    <div
      {...props}
      className={classNames(
        'm-column',
        renderTypesAsClassNames(type, 'm-column--type-'),
        className
      )}
    >
      {children}
    </div>
  );
};

Column.propTypes = {
  type: PropTypes.oneOfType([
    PropTypes.oneOf([
      '',
      'main-photo',
      'main-photo-mod-1',
      'sidebar',
      'column-indent',
      'content',
      'overflow',
      'action',
    ]),
    PropTypes.arrayOf(PropTypes.string),
  ]),
  className: PropTypes.string,
  children: PropTypes.node,
};

Column.defaultProps = {
  type: '',
  className: '',
};

export default React.memo(Column);
