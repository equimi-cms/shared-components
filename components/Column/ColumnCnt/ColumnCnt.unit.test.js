import React from 'react';
import {ColumnCnt} from './ColumnCnt';
import {shallow} from 'enzyme';

describe('ColumnCnt', () => {
  it('should renders', () => {
    const Main = shallow(<ColumnCnt>test</ColumnCnt>);
    expect(Main.text()).toBe('test');
  });

  it('should renders with className', () => {
    const Main = shallow(<ColumnCnt className={'test'}>test</ColumnCnt>);
    expect(Main.hasClass('test')).toBeTruthy();
  });

  it('should renders with type mod-height-1', () => {
    const Main = shallow(<ColumnCnt type={'mod-height-1'}>test</ColumnCnt>);
    expect(Main.hasClass('m-column-cnt--mod-height-1')).toBeTruthy();
  });
});
