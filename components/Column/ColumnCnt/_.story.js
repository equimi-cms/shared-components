/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {ColumnCnt} from './ColumnCnt';
import {Dashboard} from '../../../../../../.storybook/config';
import {Column} from '../Column';

storiesOf(Dashboard + '/Column/ColumnCnt', module).add('default', () => (
  <ColumnCnt>
    <Column type={['half', 'content']}>Content</Column>
    <Column type={['half', 'content']}>Content</Column>
  </ColumnCnt>
));
