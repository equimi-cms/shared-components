import React from 'react';
import './ColumnCnt.scss';
import classNames from 'classnames';
import {renderTypesAsClassNames} from '../../../utils/utils';

interface IColumnCnt {
  className?: String;
  type?:
    | ''
    | 'mod-height-auto'
    | 'mod-indent'
    | 'mod-height-1'
    | 'mod-height-2'
    | 'mod-height-3'
    | 'mod-height-4'
    | 'mod-height-5';
  children?: React.ReactNode;
}

export const ColumnCnt = ({children, type, className, ...props}: IColumnCnt) => {
  return (
    <div
      {...props}
      className={classNames(
        'm-column-cnt',
        className,
        renderTypesAsClassNames(type, 'm-column-cnt--')
      )}
    >
      {children}
    </div>
  );
};

export default React.memo(ColumnCnt);

ColumnCnt.defaultProps = {
  type: '',
  className: '',
  children: null,
};
