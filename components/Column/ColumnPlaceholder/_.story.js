/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {ColumnPlaceholder} from './ColumnPlaceholder';
import {Dashboard} from '../../../../../../.storybook/config';

storiesOf(Dashboard + '/Column/ColumnPlaceholder', module).add('default', () => (
  <ColumnPlaceholder title="Title" description="Description" type="news" />
));
