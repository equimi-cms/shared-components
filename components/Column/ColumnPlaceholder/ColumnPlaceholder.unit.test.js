import React from 'react';
import {ColumnPlaceholder} from './ColumnPlaceholder';
import {render, shallow} from 'enzyme';

describe('ColumnPlaceholder', () => {
  it('should renders', () => {
    const Main = shallow(<ColumnPlaceholder />);
    expect(Main.find('.column-placeholder').length).toBe(1);
  });
  it('should renders with title when pass title', () => {
    const Main = render(<ColumnPlaceholder title={'test'} />);
    expect(Main.find('.m-column-title').html()).toContain('test');
  });
  it('should call onClick when click on card btn', () => {
    const onClick = jest.fn();
    const Main = shallow(<ColumnPlaceholder onClick={onClick} />);
    Main.find('.column-placeholder-button').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
