import React from "react";
import "./ColumnPlaceholder.scss";
import classNames from "classnames";
import Column from "../../../components/Column/Column";
import ColumnTitle from "../../../components/Column/ColumnTitle/ColumnTitle";
import ContentPlaceholder from "../../../components/ContentPlaceholder/ContentPlaceholder";
import ColumnDescription from "../../../components/Column/ColumnDescription/ColumnDescription";
import CardButton from "../../../components/Card/CardButton/CardButton";
import type {Entity} from "shared/types";
import {renderTypesAsClassNames} from "utils/utils";

interface IColumnPlaceholderProps {
  className?: String;
  btnClass?: String;
  title?: React.ReactElement | "";
  description?: React.ReactElement | "";
  columnType?: ["half" | "content"];
  type?: Entity;
  onClick?: () => void;
  cardButton?: React.ReactNode;
}

export const ColumnPlaceholder = ({
  className,
  title,
  description,
  type,
  columnType,
  onClick,
  btnClass,
  cardButton,
  ...props
}: IColumnPlaceholderProps) => {
  return (
    <div
      {...props}
      className={classNames(
        "column-placeholder",
        className,
        renderTypesAsClassNames(columnType, "column-placeholder--type-")
      )}
    >
      <Column type={["overflow"]}>
        <ColumnTitle>{title}</ColumnTitle>
        <ColumnDescription>{description}</ColumnDescription>
        <ContentPlaceholder type={type} />
        {cardButton || (
          <CardButton
            onClick={onClick}
            type={["line", "plus"]}
            className={classNames("column-placeholder-button", btnClass)}
          />
        )}
      </Column>
    </div>
  );
};

export default React.memo(ColumnPlaceholder);

ColumnPlaceholder.defaultProps = {
  className: "",
  btnClass: "",
  title: "",
  description: "",
  columnType: [""],
  type: [""],
  onClick: /* istanbul ignore next */ () => null,
};
