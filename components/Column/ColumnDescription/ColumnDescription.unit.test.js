import React from 'react';
import {ColumnDescription} from './ColumnDescription';
import {shallow} from 'enzyme';

describe('ColumnDescription', () => {
  it('should renders', () => {
    const Main = shallow(<ColumnDescription>test</ColumnDescription>);
    expect(Main.text()).toBe('test');
  });
});
