import React from 'react';
import './ColumnDescription.scss';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export const ColumnDescription = ({className, children, ...props}) => {
  return (
    <div {...props} className={classNames('m-column-description', className)}>
      {children}
    </div>
  );
};

export default React.memo(ColumnDescription);

ColumnDescription.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};
ColumnDescription.defaultProps = {
  className: '',
};
