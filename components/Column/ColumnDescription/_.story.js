/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {ColumnDescription} from './ColumnDescription';
import {Dashboard} from '../../../../../../.storybook/config';

storiesOf(Dashboard + '/Column/ColumnDescription', module).add('default', () => (
  <ColumnDescription>Content</ColumnDescription>
));
