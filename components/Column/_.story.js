/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import Column from './Column';
import {Dashboard} from '../../../../../.storybook/config';

storiesOf(Dashboard + '/Column', module)
  .add('default', () => <Column>Content</Column>)
  .add('with type sidebar', () => <Column type="sidebar">Content</Column>);
