/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {ColumnTitle} from './ColumnTitle';
import {Dashboard} from '../../../../../../.storybook/config';

storiesOf(Dashboard + '/Column/ColumnTitle', module)
  .add('default', () => <ColumnTitle>Content</ColumnTitle>)
  .add('mod-1', () => <ColumnTitle type="mod-1">Content</ColumnTitle>)
  .add('mod-2', () => <ColumnTitle type="mod-2">Content</ColumnTitle>)
  .add('mod-hover', () => <ColumnTitle type="mod-hover">Content</ColumnTitle>);
