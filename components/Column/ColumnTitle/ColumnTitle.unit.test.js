import React from 'react';
import {ColumnTitle} from './ColumnTitle';
import {shallow} from 'enzyme';

describe('ColumnTitle', () => {
  it('should renders', () => {
    const Main = shallow(<ColumnTitle>test</ColumnTitle>);
    expect(Main.text()).toBe('test');
  });

  it('should render with className', () => {
    const component = shallow(<ColumnTitle className='test'/>);
    expect(component.find('.js-m-column-title').hasClass('test')).toBeTruthy();
  });

  it('should render with type', () => {
    const component = shallow(<ColumnTitle type='test'/>);
    expect(component.find('.js-m-column-title').hasClass('m-column-title--test')).toBeTruthy();
  });

  it('should not render count title', () => {
    const Main = shallow(<ColumnTitle>test</ColumnTitle>);
    expect(Main.find('.test-num').length).toBe(0);
  });

  it('should not render arrow right', () => {
    const Main = shallow(<ColumnTitle>test</ColumnTitle>);
    expect(Main.find('.test-arrow-right').length).toBe(0);
  });

  it('should render arrow right', () => {
    const Main = shallow(<ColumnTitle arrowRight>test</ColumnTitle>);
    expect(Main.find('.test-arrow-right').length).toBe(1);
  });

  it('should render count title 0', () => {
    const Main = shallow(<ColumnTitle count={0}/>);
    expect(Main.find('.test-num').text()).toBe('0');
  });

  it('should render count title 1', () => {
    const Main = shallow(<ColumnTitle count={1}/>);
    expect(Main.find('.test-num').text()).toBe('1');
  });
});
