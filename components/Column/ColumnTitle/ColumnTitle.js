import React from 'react';
import './ColumnTitle.scss';
import classNames from 'classnames';
import Image from '../../../Image/Image';
import arrowRightIcon from './imgs/arrow-right.svg';
import {renderTypesAsClassNames} from '../../../utils/utils';

interface IColumnTitleProps {
  className?: String;
  type?: ['' | 'mod-1' | 'mod-2' | 'mod-hover'] | '';
  count?: Number;
  arrowRight?: Boolean;
  children: React.Element;
}

export const ColumnTitle = ({
  className,
  children,
  type,
  count,
  arrowRight,
  ...props
}: IColumnTitleProps) => {
  const isCount = count !== null;
  return (
    <div
      {...props}
      className={classNames(
        'm-column-title js-m-column-title',
        className,
        renderTypesAsClassNames(type, 'm-column-title--')
      )}
    >
      <div className="m-column-title__text">{children}</div>
      {isCount && <span className="m-column-title__num test-num">{count}</span>}
      {arrowRight && (
        <Image className="m-column-title__arrow test-arrow-right" src={arrowRightIcon} />
      )}
    </div>
  );
};

export default React.memo(ColumnTitle);

ColumnTitle.defaultProps = {
  className: '',
  type: [''],
  count: null,
  arrowRight: false,
};
