import {IMedia} from '../../../utils/type';
import {getImageSrcWithSize} from '../../../utils/utils';

/**
 *
 * @param data
 * @returns {boolean}
 */
export const isImageType = (data: IMedia) => {
  return data && data.content && !!data.content.image;
};

/**
 *
 * @param data
 * @returns {boolean}
 */
export const isVideoType = (data: IMedia) => {
  return data && data.content && !!data.content.videoId;
};

/**
 *
 * @param videoId
 * @returns {string}
 */
export const getYoutubeVideoPreview = (videoId: String = '') =>
  `https://img.youtube.com/vi/${videoId}/hqdefault.jpg`;

/**
 *
 * @param videoId
 * @param params
 * @returns {string}
 */
export const getYoutubeVideoEmbedUrl = (videoId: String = '', params: String = '') =>
  `https://www.youtube.com/embed/${videoId}${params}`;

/**
 *
 * @param data
 * @param size
 * @returns {*}
 */
export const getMediaPreview = (data: IMedia, size = '406x406') => {
  if (!data) return;
  const {content = {videoId: ''}} = data;
  if (isImageType(data)) {
    return getImageSrcWithSize(content, size);
  } else if (isVideoType(data)) {
    return getYoutubeVideoPreview(content.videoId);
  } else {
    return void 0;
  }
};
