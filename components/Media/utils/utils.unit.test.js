import {getYoutubeVideoEmbedUrl, getYoutubeVideoPreview, isImageType, isVideoType} from './utils';

describe('isImageType', () => {
  it('should return true when content has image field', () => {
    expect(isImageType({content: {image: 1}})).toBeTruthy();
  });

  it('should return false when content hasn`t image field', () => {
    expect(isImageType({content: {test: 1}})).toBeFalsy();
  });

  it('should return false when no data passed', () => {
    expect(isImageType()).toBeFalsy();
  });
});

describe('isVideoType', () => {
  it('should return true when content has id field', () => {
    expect(isVideoType({content: {videoId: 1}})).toBeTruthy();
  });

  it('should return false when content hasn`t id field', () => {
    expect(isVideoType({content: {test: 1}})).toBeFalsy();
  });

  it('should return false when no data passed', () => {
    expect(isVideoType()).toBeFalsy();
  });
});

describe('getYoutubeVideoPreview', () => {
  it('should return url', () => {
    expect(getYoutubeVideoPreview('test')).toBe('https://img.youtube.com/vi/test/hqdefault.jpg');
  });
});

describe('getYoutubeVideoEmbedUrl', () => {
  it('should return url', () => {
    expect(getYoutubeVideoEmbedUrl('test')).toBe('https://www.youtube.com/embed/test');
  });

  it('should return url with params', () => {
    expect(getYoutubeVideoEmbedUrl('test', '?test=1')).toBe(
      'https://www.youtube.com/embed/test?test=1'
    );
  });
});
