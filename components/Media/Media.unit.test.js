import React from 'react';
import {mount} from 'enzyme/build';
import {Media, MOBILE_VIEW_BELOW} from './Media';
import {mockMediaData} from './utils/mockData';

window.unitTestMode = true;
const rows = mockMediaData();
const MockComponent = props => <Media data={{rows,count:rows.length}} open={true} {...props} />;
const DESKTOP_VIEW = MOBILE_VIEW_BELOW + 100;

describe('Media', () => {
  it('should render', () => {
    const component = mount(<MockComponent />);
    expect(component.find('.js-media-aside').length).toBeTruthy();
  });

  it('should render MediaSection on desktop', () => {
    const component = mount(<MockComponent />);
    expect(component.find('.js-media-section').length).toBeTruthy();
  });

  it('should not render MediaSection on mobile', () => {
    window.innerWidth = MOBILE_VIEW_BELOW;
    const component = mount(<MockComponent />);
    expect(component.find('.js-media-section').length).toBeFalsy();
  });

  it('should render Timeline on mobile when open Media', () => {
    window.innerWidth = MOBILE_VIEW_BELOW;
    const component = mount(<MockComponent />);
    expect(component.find('.js-aside-timeline').length).toBeTruthy();
  });

  it('should not render Timeline on desktop', () => {
    window.innerWidth = DESKTOP_VIEW;
    const component = mount(<MockComponent />);
    expect(component.find('.js-aside-timeline').length).toBeFalsy();
  });

  it('should call onClose', () => {
    const onClose = jest.fn();
    const component = mount(<MockComponent onClose={onClose} open={true} />);
    component
      .find('Aside')
      .find('.test-back-btn')
      .at(0)
      .simulate('click');
    expect(onClose).toHaveBeenCalled();
  });

  it('should open Timeline under mobile when click GridItem', () => {
    window.innerWidth = MOBILE_VIEW_BELOW;
    const component = mount(<MockComponent open={true} />);
    component
      .find('.js-grid-item')
      .at(0)
      .simulate('click');
    expect(component.find('.js-aside-timeline').at(0).props().open).toBeTruthy();
  });
});
