//@flow
import React, {useCallback, useEffect, useMemo, useRef, useState} from "react";
import "./Media.scss";
import {Aside} from "../Aside/Aside";
import MediaSection from "./components/MediaSection/MediaSection";
import {WindowResponse} from "../../services/WindowResponse";
import Grid from "./components/Grid/Grid";
import MediaCalendarButton from "./components/MediaCalendarButton/MediaCalendarButton";
import Timeline from "./components/Timeline/Timeline";
import {MediaContext} from "./mediaContext/MediaContext";
import MediaCalendar from "./components/MediaCalendar/MediaCalendar";
import {IMedia} from "../../utils/type";
import type {IPlatformContext} from "./mediaContext/PlatformContext";
import {PlatformContextConsumer} from "./mediaContext/PlatformContext";
import {LoadMore} from "../../utils/LoadMore/LoadMore";
import isEqual from "lodash/isEqual";
import produce from "immer";

/**
 * Render mobile layout for media section when below 960px
 * @type {number}
 */
export const MOBILE_VIEW_BELOW = 960;

interface IMediaData {
  rows: Array<IMedia>;
  count: Number;
  countByDate: Array<{[Number]: Number}>;
}

interface IMediaProps extends IPlatformContext {
  open: Boolean;
  data: IMediaData;
  selected: Number;
  onClose: () => void;
  requestMedia: (requestOpt: {}) => Promise<IMediaData>;
  //entity id
  relatedTo: Number;
  onSelected: (selectedItem: IMedia, index: Number) => void;
  onLoadMore: (data: IMediaData) => void;
  renderMobileViewBelow: Number;
  sectionHeader: React.ReactNode;
  timelineItemOptions: React.ReactNode;
  className?: String;
}

/**
 * Media component for dashboard and public pages
 *
 * MediaSection and Timeline use for viewing the individual media
 *
 * MediaSection shows only for desktop
 * Timeline shows only for mobile
 *
 * Grid is common for desktop and mobile (only styles change on mobile)
 */
const initialFilterTime = {
  dateFrom: null,
  dateTo: null,
};

export const Media = ({
  open,
  data: initialData,
  selected,
  onClose,
  renderMobileViewBelow,
  sectionHeader,
  timelineItemOptions,
  requestMedia,
  relatedTo,
  onSelected,
  onLoadMore,
  className,
  localization: {t},
}: IMediaProps) => {
  const [state, setState] = useState({
    selectedItem: {},
    selectedIndex: selected,
    showCalendar: false,
    isLoading: false,
    showTimeline: false,
    showMobileView: false,
    filterTime: initialFilterTime,
    data: initialData,
  });
  const {
    selectedItem,
    selectedIndex,
    showCalendar,
    isLoading,
    showTimeline,
    showMobileView,
    filterTime,
    data,
  } = state;
  const {rows = [], count = 0, countByDate = []} = data;

  const showDesktopView = !showMobileView;
  const isSelectedLast = rows.length - 1 === selectedIndex;

  const mediaRef = useRef({lm: null, isLoadingData: false});

  const emitToggleCalendar = useCallback(() => {
    setState(state => ({
      ...state,
      showCalendar: !state.showCalendar,
    }));
  }, []);

  const _selectSlide = useCallback(
    (index, data = rows, options = {}) => {
      const _options = {...options};
      setState(state => {
        return {
          ...state,
          selectedIndex: index,
          selectedItem: data[index],
          ..._options,
        };
      });
      onSelected(data[index], index);
    },
    [rows, onSelected]
  );
  const handleNextSlide = useCallback(() => {
    const possibleNext = selectedIndex + 1;
    const lastItem = rows.length - 1;
    const next = possibleNext <= lastItem ? possibleNext : lastItem;
    _selectSlide(next);
  }, [_selectSlide, selectedIndex, rows]);

  const handlePrevSlide = useCallback(() => {
    const possiblePrevIndex = selectedIndex - 1;
    const prev = possiblePrevIndex > 0 ? possiblePrevIndex : 0;
    _selectSlide(prev);
  }, [_selectSlide, selectedIndex]);

  const handleGridItemClick = useCallback(
    (index, data) => {
      _selectSlide(index);
      if (showMobileView) {
        setState(state => ({
          ...state,
          showTimeline: true,
        }));
      }
    },
    [showMobileView, _selectSlide]
  );

  const handleTimelineClose = useCallback(() => {
    setState(state => ({
      ...state,
      showTimeline: false,
    }));
  }, []);

  const handleLoadMore = useCallback(() => {
    const {lm} = mediaRef.current;
    if (mediaRef.current.isLoadingData || !lm.canLoadMore()) return;
    mediaRef.current.isLoadingData = true;
    requestMedia({
      limit: 40,
      offset: lm.getNextOffset(),
      dateFrom: filterTime.dateFrom,
      dateTo: filterTime.dateTo,
      id: relatedTo,
    })
      .then(({data: newData}) => {
        const {rows: newRows = []} = newData;
        lm.setLoadedItemsCount(rows.length + newRows.length);
        const updatedData = produce(data, nData => {
          nData.rows = [...nData.rows, ...newData.rows];
        });

        /**
         * If data handled in wrapper component (onLoadMore is set)
         * then we don't need to update data in this component
         * to prevent double render
         */
        onLoadMore
          ? onLoadMore(updatedData)
          : setState(state => ({
              ...state,
              data: updatedData,
            }));
      })
      .finally(() => {
        mediaRef.current.isLoadingData = false;
      });
  }, [mediaRef, filterTime, requestMedia, onLoadMore, data, relatedTo, rows.length]);

  const handlePickTime = useCallback(
    time => {
      if (isEqual(filterTime, time)) return;
      setState(state => ({
        ...state,
        filterTime: time,
        isLoading: true,
      }));
      requestMedia({limit: 40, dateFrom: time.dateFrom, dateTo: time.dateTo, id: relatedTo})
        .then(({data: newData}) => {
          const {rows, count} = newData;
          mediaRef.current.lm = new LoadMore(40);
          mediaRef.current.lm.setAllItemsCount(count);
          _selectSlide(0, rows, {isLoading: false, data: newData});
        })
        .catch(() => {
          setState(state => ({
            ...state,
            isLoading: false,
          }));
        });
    },
    [filterTime, requestMedia, relatedTo, _selectSlide]
  );

  const mediaContextData = useMemo(
    () => ({
      selectedItem,
      selectedIndex,
      isSelectedLast,
      onNext: handleNextSlide,
      onPrev: handlePrevSlide,
    }),
    [handleNextSlide, handlePrevSlide, isSelectedLast, selectedIndex, selectedItem]
  );

  useEffect(() => {
    const wr = new WindowResponse(() => {
      setState(state => ({
        ...state,
        showMobileView: window.innerWidth <= renderMobileViewBelow,
      }));
    });
    const isInitialData = data && !!data.rows.length;
    mediaRef.current.lm = new LoadMore(40);

    if (!isInitialData) {
      setState(state => ({
        ...state,
        isLoading: true,
      }));
      requestMedia({limit: 40, id: relatedTo})
        .then(({data}) => {
          setState(state => ({
            ...state,
            data,
            isLoading: false,
          }));
        })
        .catch(() => {
          setState(state => ({
            ...state,
            isLoading: false,
          }));
        });
    }

    mediaRef.current.lm.setAllItemsCount(count);

    return () => wr.destroy();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  /**
   * Set all data to default when open Media
   */
  useEffect(() => {
    if (open) {
      setState(state => ({
        ...state,
        data: initialData,
        filterTime: initialFilterTime,
        selectedIndex: selected,
        selectedItem: initialData.rows[selected],
      }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);

  /**
   * Update state when initialData updated
   * without updating filterTime
   */
  useEffect(() => {
    if (open) {
      setState(state => ({
        ...state,
        data: initialData,
        selectedIndex: selected,
        selectedItem: initialData.rows[selected],
      }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initialData]);

  return (
    <div ref={mediaRef}>
      <MediaContext.Provider value={mediaContextData}>
        <Aside
          open={open}
          className={`media-aside js-media-aside ${className}`}
          isBackBtn
          backBtnType={showMobileView ? "arrowLeft" : "close"}
          onBack={onClose}
          title={t("Gallery")}
          contentType={"no-padding"}
          asideLeftContent={showDesktopView && <MediaSection sectionHeader={sectionHeader} />}
          withCustomScroll={false}
          headerRight={<MediaCalendarButton onClick={emitToggleCalendar} />}
        >
          <MediaCalendar
            open={showCalendar}
            timeLine={countByDate}
            selectedData={filterTime}
            onClose={emitToggleCalendar}
            onPickTime={handlePickTime}
          />
          <Grid
            data={rows}
            isLoading={isLoading}
            isMobile={showMobileView}
            onItemClick={handleGridItemClick}
            onLoadMore={handleLoadMore}
          />
        </Aside>
        {showMobileView && open && (
          <Timeline
            className={className}
            data={rows}
            isLoading={isLoading}
            open={showTimeline}
            timeLine={countByDate}
            itemOptions={timelineItemOptions}
            selectedCalendarData={filterTime}
            onClose={handleTimelineClose}
            onLoadMore={handleLoadMore}
            onPickTime={handlePickTime}
          />
        )}
      </MediaContext.Provider>
    </div>
  );
};

export default React.memo(PlatformContextConsumer(Media));

Media.defaultProps = {
  selected: 0,
  data: {
    rows: [],
    count: 0,
    countByDate: [],
  },
  localization: {
    t: v => v,
  },
  renderMobileViewBelow: MOBILE_VIEW_BELOW,
  onClose: () => null,
  onSelected: () => null,
  onLoadMore: null,
  requestMedia: () => Promise.resolve([]),
};
