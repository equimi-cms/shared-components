//@flow
import React from 'react';
import NoDataTitle from '../../../NoDataTitle/NoDataTitle';
import {PlatformContextConsumer} from '../../mediaContext/PlatformContext';
import type {IPlatformContext} from '../../mediaContext/PlatformContext';

interface INoDataTitleLocalizedProps extends IPlatformContext {
  className: String;
  children: React.ReactElement;
}

export const NoDataTitleLocalized = ({
  className,
  children,
  localization: {t},
}: INoDataTitleLocalizedProps) => {
  return <NoDataTitle children={t(children)} />;
};

export default React.memo(PlatformContextConsumer(NoDataTitleLocalized));

NoDataTitleLocalized.defaultProps = {
  children:'No_results'
};
