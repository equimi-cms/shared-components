import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {MediaSectionSlider} from './MediaSectionSlider';

const map = {};
document.addEventListener = jest.fn((event, cb) => {
  map[event] = cb;
});

const mockSelectedItem = {id: 1};

describe('MediaSectionSlider', () => {
  it('should not render', () => {
    const component = shallow(<MediaSectionSlider />);
    expect(component.find('.js-ms-slider').length).toBe(0);
  });

  it('should render', () => {
    const component = shallow(<MediaSectionSlider selectedItem={mockSelectedItem} />);
    expect(component.find('.js-ms-slider').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<MediaSectionSlider className="test" selectedItem={mockSelectedItem} />);
    expect(component.find('.js-ms-slider').hasClass('test')).toBeTruthy();
  });

  it('should call onPrev when click button left', () => {
    const onPrev = jest.fn();
    const component = shallow(<MediaSectionSlider onPrev={onPrev} selectedItem={mockSelectedItem} />);
    component.find('.js-slide-left').simulate('click');
    expect(onPrev).toHaveBeenCalled();
  });

  it('should call onPrev when press arrow left', () => {
    const onPrev = jest.fn();
    const component = mount(<MediaSectionSlider onPrev={onPrev} selectedItem={mockSelectedItem} />);
    map.keyup({keyCode: 37});
    expect(onPrev).toHaveBeenCalled();
  });

  it('should call onNext when click button right', () => {
    const onNext = jest.fn();
    const component = shallow(<MediaSectionSlider onNext={onNext} selectedItem={mockSelectedItem} />);
    component.find('.js-slide-right').simulate('click');
    expect(onNext).toHaveBeenCalled();
  });

  it('should call onNext when press arrow right', () => {
    const onNext = jest.fn();
    const component = mount(<MediaSectionSlider onNext={onNext} selectedItem={mockSelectedItem} />);
    map.keyup({keyCode: 39});
    expect(onNext).toHaveBeenCalled();
  });

  it('should render button left disabled when selectedIndex is 0', () => {
    const component = shallow(<MediaSectionSlider selectedIndex={0} selectedItem={mockSelectedItem} />);
    expect(component.find('.js-slide-left').hasClass('ms-slider__button--disabled')).toBeTruthy();
  });

  it('should render button right disabled when isSelectedLast is true', () => {
    const component = shallow(<MediaSectionSlider isSelectedLast selectedItem={mockSelectedItem} />);
    expect(component.find('.js-slide-right').hasClass('ms-slider__button--disabled')).toBeTruthy();
  });
});
