//@flow
import React, {useEffect} from 'react';
import './MediaSectionSlider.scss';
import classNames from 'classnames';
import {Image} from '../../../../../Image/Image';
import arrowLeft from './imgs/left.svg';
import arrowRight from './imgs/right.svg';
import SliderItem from './SliderItem/SliderItem';
import {MediaContextConsumer} from '../../../mediaContext/MediaContext';
import type {IMediaContext} from '../../../mediaContext/MediaContext';

interface IMediaSectionSliderProps extends IMediaContext {
  className: String;
}

export const MediaSectionSlider = ({
  className,
  selectedIndex,
  selectedItem,
  isSelectedLast,
  onPrev,
  onNext,
}: IMediaSectionSliderProps) => {
  const buttonLeftInActive = selectedIndex === 0;
  const buttonRightInActive = isSelectedLast;

  const handleKeyPress = e => {
    const key = {
      left: 37,
      right: 39,
      up: 38,
      down: 40,
    };
    if (e.keyCode === key.left) onPrev();
    if (e.keyCode === key.right) onNext();
  };
  useEffect(() => {
    document.addEventListener('keyup', handleKeyPress);
    return () => {
      document.removeEventListener('keyup', handleKeyPress);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedIndex]);

  if(!selectedItem) return null;

  return (
    <div className={classNames('ms-slider js-ms-slider', className)}>
      <button
        className={classNames(
          'ms-slider__button ms-slider__button--left js-slide-left',
          buttonLeftInActive && 'ms-slider__button--disabled'
        )}
        onClick={onPrev}
      >
        <Image src={arrowLeft} />
      </button>
      <div className="ms-slider__container">
        <SliderItem selectedItem={selectedItem} />
      </div>
      <button
        className={classNames(
          'ms-slider__button ms-slider__button--right js-slide-right',
          buttonRightInActive && 'ms-slider__button--disabled'
        )}
        onClick={onNext}
      >
        <Image src={arrowRight} />
      </button>
    </div>
  );
};

export default React.memo(MediaContextConsumer(MediaSectionSlider));

/* istanbul ignore next */
MediaSectionSlider.defaultProps = {
  className: '',
  onPrev: () => null,
  onNext: () => null,
};
