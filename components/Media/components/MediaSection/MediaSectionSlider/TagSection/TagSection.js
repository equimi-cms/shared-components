//@flow
import React from 'react';
import './TagSection.scss';

interface ITagSectionProps {
  children: React.ReactNode;
}

export const TagSection = ({children}: ITagSectionProps) => {
  return <div className="si-content-tag js-si-content">{children}</div>;
};

export default React.memo(TagSection);

TagSection.defaultProps = {};
