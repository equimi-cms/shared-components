import React from 'react';
import {shallow} from 'enzyme/build';
import {Tag} from './Tag';

describe('Tag', () => {
  it('should render', () => {
    const component = shallow(<Tag />);
    expect(component.find('.js-si-tag').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<Tag className="test" />);
    expect(component.find('.js-si-tag').hasClass('test')).toBeTruthy();
  });

  it('should render with children', () => {
    const component = shallow(<Tag>test</Tag>);
    expect(
      component
        .find('.js-si-tag')
        .props()
        .children.includes('test')
    ).toBeTruthy();
  });

  it('should render title', () => {
    const component = shallow(<Tag title={'test'} />);
    expect(component.find('.js-si-tag-title').props().children).toBe('test');
  });
});
