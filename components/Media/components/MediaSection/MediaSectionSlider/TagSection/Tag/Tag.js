//@flow
import React from 'react';
import classNames from 'classnames';
import {renderTypesAsClassNames} from '../../../../../../../utils/utils';

interface ITagProps {
  title: String;
  children: React.ReactNode;
  className?: React.ReactNode;
  type?: '' | 'mod-1';
}

export const Tag = ({title, children, className, type, ...props}: ITagProps) => {
  return (
    <div
      {...props}
      className={classNames(
        'si-content-tag__item js-si-tag',
        className,
        renderTypesAsClassNames(type, 'si-content-tag__item--')
      )}
    >
      <p className="si-content-tag__item-title js-si-tag-title">{title}</p>
      {children}
    </div>
  );
};

export default React.memo(Tag);

Tag.Content = ({children, as: As = 'div', ...other}) => (
  <As {...other} className="si-content-tag__item-content js-si-tag-content">
    {children}
  </As>
);

Tag.defaultProps = {
  className: '',
};
