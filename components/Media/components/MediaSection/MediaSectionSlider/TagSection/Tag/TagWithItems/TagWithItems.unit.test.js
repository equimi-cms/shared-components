import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {TagWithItems} from './TagWithItems';
import Tag from '../Tag';

describe('TagWithItems', () => {
  it('should not render', () => {
    const component = shallow(<TagWithItems />);
    expect(component.find(Tag).length).toBe(0);
  });

  it('should render Tag with 2 items', () => {
    const mockData = [{id: 1, name: '1'}, {id: 2, name: '2'}];
    const component = mount(<TagWithItems title={'test'} data={mockData} />);
    expect(component.find('.js-si-tag-content').length).toBe(2);
  });
});
