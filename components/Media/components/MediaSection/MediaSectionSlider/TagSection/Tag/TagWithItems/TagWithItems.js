//@flow
import React from 'react';
import {Tag} from '../Tag';
import {PlatformContextConsumer} from '../../../../../../mediaContext/PlatformContext';
import type {IPlatformContext} from '../../../../../../mediaContext/PlatformContext';
import type {Entity} from '../../../../../../../../utils/types';

interface ITagWithItemsProps extends IPlatformContext {
  data: Array<{id: String, name: String}>;
  title: String;
  entity: Entity;
}

export const TagWithItems = ({
  data,
  title,
  entity,
  localization: {t},
  navigation: {getLinkToEntity},
}: ITagWithItemsProps) => {
  if (!data.length) return null;

  return (
    <Tag
      className={'js-tag-'+entity}
      title={t(title, {count: data.length})}
      children={data.map((itemData, index) => {
        const {id} = itemData;
        return (
          <Tag.Content
            key={id || index}
            as={'a'}
            href={getLinkToEntity(entity, id, itemData)}
            target="_blank"
            children={itemData.name}
          />
        );
      })}
    />
  );
};

export default React.memo(PlatformContextConsumer(TagWithItems));

TagWithItems.defaultProps = {
  data: [],
  localization: {
    t: v => v,
  },
  navigation: {
    getLinkToEntity: () => null,
  },
};
