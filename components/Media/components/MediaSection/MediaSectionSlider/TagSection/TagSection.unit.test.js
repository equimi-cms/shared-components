import React from 'react';
import {shallow} from 'enzyme/build';
import {TagSection} from './TagSection';

describe('TagSection', () => {
  it('should render', () => {
    const component = shallow(<TagSection />);
    expect(component.find('.js-si-content').length).toBe(1);
  });

  it('should render with children', () => {
    const component = shallow(<TagSection>test</TagSection>);
    expect(component.find('.js-si-content').props().children).toBe('test');
  });
});
