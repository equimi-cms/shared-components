import React from 'react';
import {shallow} from 'enzyme/build';
import {SliderItemVideo} from './SliderItemVideo';

describe('SliderItemVideo', () => {
  it('should render', () => {
    const component = shallow(<SliderItemVideo />);
    expect(component.find('.js-slider-video').length).toBe(1);
  });
});
