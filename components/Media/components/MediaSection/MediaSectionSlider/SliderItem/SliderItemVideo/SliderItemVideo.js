//@flow
import React from 'react';
import ItemDescWithLink from '../ItemDescWithLink/ItemDescWithLink';
import type {IMedia} from '../../../../../../../utils/type';
import {getYoutubeVideoEmbedUrl} from '../../../../../utils/utils';

interface ISliderItemVideoProps {
  data: IMedia;
}

export const SliderItemVideo = ({data}: ISliderItemVideoProps) => {
  const {content: {videoId} = {videoId: null}} = data;

  return (
    <div className="ms-slider-item-cnt ms-slider-item-cnt--video js-slider-video">
      <div className="ms-slider-item ms-slider-item--video">
        <div className="ms-slider-item__video">
          <iframe
            title={data.title}
            src={getYoutubeVideoEmbedUrl(videoId)}
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </div>
      </div>
      <div className="si-content">
        <ItemDescWithLink data={data} />
      </div>
    </div>
  );
};

export default React.memo(SliderItemVideo);

SliderItemVideo.defaultProps = {
  data: {},
};
