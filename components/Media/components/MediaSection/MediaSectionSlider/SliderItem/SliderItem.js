//@flow
import React from 'react';
import './SliderItem.scss';
import SliderItemImage from './SliderItemImage/SliderItemImage';
import SliderItemVideo from './SliderItemVideo/SliderItemVideo';
import type {IMediaContext} from '../../../../mediaContext/MediaContext';
import {isImageType, isVideoType} from '../../../../utils/utils';

interface ISliderItemProps extends IMediaContext {}

export const SliderItem = ({selectedItem}: ISliderItemProps) => {
  if (isImageType(selectedItem)) {
    return <SliderItemImage data={selectedItem} />;
  } else if (isVideoType(selectedItem)) {
    return <SliderItemVideo data={selectedItem} />;
  } else {
    return null;
  }
};

export default React.memo(SliderItem);

SliderItem.defaultProps = {};
