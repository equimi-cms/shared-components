import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {ItemDescWithLink} from './ItemDescWithLink';

describe('ItemDescWithLink', () => {
  it('should render', () => {
    const component = shallow(<ItemDescWithLink />);
    expect(component.find('.js-desc').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<ItemDescWithLink className="test" />);
    expect(component.find('.js-desc').hasClass('test')).toBeTruthy();
  });

  it('should render Link', () => {
    const component = mount(<ItemDescWithLink  />);
    expect(component.find('.js-link').length).toBeTruthy();
  });

  it('should not render Link when entity id is the same as relatedTo', () => {
    const entityId = 1;
    const component = mount(<ItemDescWithLink data={{entityId}} relatedTo={entityId}  />);
    expect(component.find('.js-link').length).toBeFalsy();
  });
});
