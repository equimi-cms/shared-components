//@flow
import React from 'react';
import classNames from 'classnames';
import Link from '../../../../Link/Link';
import type {IMedia} from '../../../../../../../utils/type';
import upperFirst from 'lodash/upperFirst';
import {PlatformContextConsumer} from '../../../../../mediaContext/PlatformContext';
import type {IPlatformContext} from '../../../../../mediaContext/PlatformContext';
import {getPluralEntityTitle, getSingleEntityTitle} from '../../../../../../../utils/utils';
import {getEntityTitle} from '../../../../../../../utils/entity';

interface IItemDescWithLinkProps extends IPlatformContext {
  className: String;
  data: IMedia;
}

export const ItemDescWithLink = ({
  className,
  data,
  localization: {t},
  navigation: {getLinkToEntity},
  relatedTo,
}: IItemDescWithLinkProps) => {
  const {entityId, entityType, entity = {title: '', name: '', id: null}} = data;
  const entityTitle = upperFirst(getEntityTitle(entity));
  const singleEntity = getSingleEntityTitle(entityType);
  const href = getLinkToEntity(getPluralEntityTitle(entityType), entityId, entity);
  const showLink = relatedTo !== entityId;

  return (
    <p className={classNames('si-content__desc js-desc', className)}>
      {entityTitle && <span className="si-content__desc-content">{entityTitle}</span>}
      {showLink && (
        <Link
          as={'a'}
          target="_blank"
          href={href}
          children={t('Go_to_entity', {entity: upperFirst(singleEntity)})}
        />
      )}
    </p>
  );
};

export default React.memo(PlatformContextConsumer(ItemDescWithLink));

ItemDescWithLink.defaultProps = {
  data: {},
  localization: {
    t: v => v,
  },
  navigation: {
    getLinkToEntity: () => null,
  },
  relatedTo: null,
};
