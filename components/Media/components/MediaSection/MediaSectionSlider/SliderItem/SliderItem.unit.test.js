import React from 'react';
import {shallow} from 'enzyme/build';
import {SliderItem} from './SliderItem';
import SliderItemImage from './SliderItemImage/SliderItemImage';
import SliderItemVideo from './SliderItemVideo/SliderItemVideo';

describe('SliderItem', () => {
  it('should not render', () => {
    const component = shallow(<SliderItem />);
    expect(Object.keys(component).length).toBe(0);
  });

  it('should render SliderItemImage', () => {
    const component = shallow(<SliderItem selectedItem={{content: {image: '1.png'}}} />);
    expect(component.find(SliderItemImage).length).toBe(1);
  });

  it('should render SliderItemVideo', () => {
    const component = shallow(<SliderItem selectedItem={{content: {videoId: '123'}}} />);
    expect(component.find(SliderItemVideo).length).toBe(1);
  });
});
