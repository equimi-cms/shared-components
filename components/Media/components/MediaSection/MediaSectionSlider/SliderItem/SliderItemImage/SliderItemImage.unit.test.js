import React from 'react';
import {shallow} from 'enzyme/build';
import {SliderItemImage} from './SliderItemImage';

describe('SliderItemImage', () => {
  it('should render', () => {
    const component = shallow(<SliderItemImage />);
    expect(component.find('.js-slider-image').length).toBe(1);
  });
});
