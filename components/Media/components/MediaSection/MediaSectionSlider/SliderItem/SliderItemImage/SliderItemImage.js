//@flow
import React from 'react';
import {Image} from '../../../../../../../Image/Image';
import TagSection from '../../TagSection/TagSection';
import {Tag} from '../../TagSection/Tag/Tag';
import ResultItem from '../../../../../../../components/Results/ResultItem/ResultItem';
import type {IMedia} from '../../../../../../../utils/type';
import ItemDescWithLink from '../ItemDescWithLink/ItemDescWithLink';
import TagWithItems from '../../TagSection/Tag/TagWithItems/TagWithItems';
import {PlatformContextConsumer} from '../../../../../mediaContext/PlatformContext';
import type {IPlatformContext} from '../../../../../mediaContext/PlatformContext';
import {getImageSrcWithSize} from '../../../../../../../utils/utils';
import {Entities} from '../../../../../../../utils/types';

interface ISliderItemImageProps extends IPlatformContext {
  data: IMedia;
}

export const SliderItemImage = ({
  data,
  localization: {t},
  navigation: {getLinkToEntity},
}: ISliderItemImageProps) => {
  const {title, members = [], horses = [], results = [], content} = data;
  const image = getImageSrcWithSize(content, '1600x1600');

  return (
    <div className="ms-slider-item-cnt ms-slider-item-cnt--image js-slider-image">
      <picture className="ms-slider-item ms-slider-item--image">
        <Image className="ms-slider-item__image" alt={title} src={image} />
      </picture>
      <div className="si-content">
        {title && <h1 className="si-content__title js-image-title">{title}</h1>}
        <ItemDescWithLink data={data} />

        <TagSection>
          {results.length ? (
            <Tag title={t('Tagged_result', {count: results.length})} className={'js-tag-'+Entities.RESULT} type="mod-1">
              {results.map((itemData, index) => {
                const {
                  memberName,
                  id,
                  horseName,
                  level,
                  place,
                  date,
                  showName,
                  country,
                  className,
                } = itemData;
                return (
                  <Tag.Content
                    key={id || index}
                    as={'a'}
                    href={getLinkToEntity(Entities.RESULT, id, itemData)}
                    target="_blank"
                  >
                    <ResultItem
                      className="tag-result-item"
                      flagPath={'/'}
                      date={date}
                      countryName={country}
                      classOfResult={className}
                      level={level}
                      member={memberName}
                      horse={horseName}
                      show={showName}
                      place={place}
                    />
                  </Tag.Content>
                );
              })}
            </Tag>
          ) : null}

          <TagWithItems title={'Tagged_member'} entity={Entities.MEMBER} data={members} />
          <TagWithItems title={'Tagged_horse'} entity={Entities.HORSE} data={horses} />
        </TagSection>
      </div>
    </div>
  );
};

export default React.memo(PlatformContextConsumer(SliderItemImage));

SliderItemImage.defaultProps = {
  data: {},
  localization: {
    t: v => v,
  },
  navigation: {
    getLinkToEntity: () => null,
  },
};
