//@flow
import React from 'react';
import './MediaSectionHeader.scss';
import moment from 'moment';
import {PlatformContextConsumer} from '../../../mediaContext/PlatformContext';
import type {IPlatformContext} from '../../../mediaContext/PlatformContext';
import type {IMediaContext} from '../../../mediaContext/MediaContext';
import {MediaContextConsumer} from '../../../mediaContext/MediaContext';

interface IMediaSectionHeaderProps extends IPlatformContext, IMediaContext {
  children: React.ReactElement;
}

export const MediaSectionHeader = ({
  selectedItem,
  children:Content,
  localization: {t},
}: IMediaSectionHeaderProps) => {
  const {created, viewsCount = 0} = selectedItem;
  const formattedDate = moment(created).format('MMMM DD, YYYY');
  return (
    <header className="media-section-header js-media-header">
      <div className="media-section-header__info">
        <p className="js-views">
          {viewsCount} {t('Views')}
        </p>
        <time className="js-date" dateTime={created}>
          {formattedDate}
        </time>
      </div>
      <Content data={selectedItem}/>
    </header>
  );
};

export default React.memo(MediaContextConsumer(PlatformContextConsumer(MediaSectionHeader)));

MediaSectionHeader.defaultProps = {
  selectedItem: {},
  localization: {
    t: v => v,
  },
  children:()=>null
};
