import React from 'react';
import {shallow} from 'enzyme/build';
import {ShareButton} from './ShareButton';

describe('ShareButton', () => {
  it('should render', () => {
    const component = shallow(<ShareButton/>);
    expect(component.find('.js-share-btn').length).toBe(1);
  });
});
