//@flow
import React from 'react';
import classNames from 'classnames';
import {Image} from '../../../../../../../Image/Image';
import shareIcon from '../../imgs/share.svg';

interface IShareButtonProps {
  className: String;
  children: React.ReactElement;
}

export const ShareButton = ({className, children, ...props}: IShareButtonProps) => {
  return (
    <div {...props} className={classNames('media-section-button js-share-btn', className)}>
      <Image src={shareIcon} />
    </div>
  );
};

export default React.memo(ShareButton);

ShareButton.defaultProps = {};
