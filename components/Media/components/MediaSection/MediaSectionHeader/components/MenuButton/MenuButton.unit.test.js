import React from 'react';
import {shallow} from 'enzyme/build';
import {MenuButton} from './MenuButton';

describe('MenuButton', () => {
  it('should render', () => {
    const component = shallow(<MenuButton/>);
    expect(component.find('.js-menu-btn').length).toBe(1);
  });
});
