//@flow
import React from 'react';
import classNames from 'classnames';
import {Image} from '../../../../../../../Image/Image';
import menuIcon from '../../imgs/menu.svg';

interface IMenuButtonProps {
  className: String;
  children: React.ReactElement;
}

export const MenuButton = ({className, children, ...props}: IMenuButtonProps) => {
  return (
    <div {...props} className={classNames('media-section-button js-menu-btn', className)}>
      <Image src={menuIcon} />
    </div>
  );
};

export default React.memo(MenuButton);

MenuButton.defaultProps = {};
