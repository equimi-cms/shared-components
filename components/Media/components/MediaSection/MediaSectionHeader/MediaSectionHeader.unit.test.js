import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {MediaSectionHeader} from './MediaSectionHeader';

describe('MediaSectionHeader', () => {
  it('should render', () => {
    const component = shallow(<MediaSectionHeader />);
    expect(component.find('.js-media-header').length).toBe(1);
  });

  it('should render views', () => {
    const component = shallow(<MediaSectionHeader />);
    expect(component.find('.js-views').text()).toContain(0);
  });

  it('should render date created', () => {
    const component = shallow(
      <MediaSectionHeader selectedItem={{created: '2019-08-04T18:53:45.466Z'}} />
    );
    expect(component.find('.js-date').text()).toBe('August 04, 2019');
  });

  it('should render children with params', () => {
    let _data = null;
    const mockSelectedItem = {a: 1};
    const component = mount(
      <MediaSectionHeader selectedItem={mockSelectedItem}>
        {data => {
          _data = data;
          return <p className="test">test</p>;
        }}
      </MediaSectionHeader>
    );
    expect(_data).toEqual({data: mockSelectedItem});
    expect(component.find('.test').text()).toBe('test');
  });
});
