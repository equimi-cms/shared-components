//@flow
import React from 'react';
import './MediaSection.scss';
import classNames from 'classnames';
import MediaSectionHeader from './MediaSectionHeader/MediaSectionHeader';
import MediaSectionSlider from './MediaSectionSlider/MediaSectionSlider';
import Scroll from '../../../../components/Scroll/Scroll';

interface IMediaSectionProps {
  className: String;
  sectionHeader:React.ReactNode
}

export const MediaSection = ({className,sectionHeader, ...props}: IMediaSectionProps) => {
  return (
    <div {...props} className={classNames('media-section js-media-section', className)}>
      <Scroll>
        <MediaSectionHeader children={sectionHeader} />
        <MediaSectionSlider />
      </Scroll>
    </div>
  );
};

export default React.memo(MediaSection);

MediaSection.defaultProps = {
  className: '',
};
