import React from 'react';
import {shallow} from 'enzyme/build';
import {MediaSection} from './MediaSection';
import MediaSectionSlider from './MediaSectionSlider/MediaSectionSlider';
import MediaSectionHeader from './MediaSectionHeader/MediaSectionHeader';

describe('MediaSection', () => {
  it('should render', () => {
    const component = shallow(<MediaSection />);
    expect(component.find('.js-media-section').length).toBeTruthy();
  });

  it('should render with className', () => {
    const component = shallow(<MediaSection className="test" />);
    expect(component.find('.js-media-section').hasClass('test')).toBeTruthy();
  });

  it('should render MediaSectionHeader and MediaSectionSlider', () => {
    const component = shallow(<MediaSection>test</MediaSection>);
    expect(component.find(MediaSectionHeader).length).toBeTruthy();
    expect(component.find(MediaSectionSlider).length).toBeTruthy();
  });
});
