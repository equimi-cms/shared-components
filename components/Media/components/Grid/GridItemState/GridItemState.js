//@flow
import React, {useEffect} from 'react';
import {MediaContextConsumer} from '../../../mediaContext/MediaContext';
import type {IMediaContext} from '../../../mediaContext/MediaContext';

interface IGridItemStateProps extends IMediaContext {}

/**
 * Created for performance reason
 * Use to change grid item selected and focus state
 */
export const GridItemState = ({selectedItem, selectedIndex}: IGridItemStateProps) => {
  useEffect(() => {
    const gridItem = document.querySelectorAll('.js-media-aside .js-grid-item')[selectedIndex];
    const selectedItem = document.querySelector('.js-media-aside .grid-item--selected');

    selectedItem && selectedItem.classList.remove('grid-item--selected');

    if (gridItem) {
      gridItem.classList.add('grid-item--selected');
      gridItem.focus();
    }
  }, [selectedItem, selectedIndex]);
  return null;
};

export default React.memo(MediaContextConsumer(GridItemState));

GridItemState.defaultProps = {};
