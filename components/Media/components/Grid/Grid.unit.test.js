import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {Grid} from './Grid';
import GridItem, {GridItemType} from './GridItem/GridItem';

const mockData = [
  {
    title: 'Image title',
    content: {image: 'image.svg'},
  },
  {
    title: 'Video title',
    content: {videoId: 2},
  },
];

describe('Grid', () => {
  it('should render', () => {
    const component = shallow(<Grid />);
    expect(component.find('.js-grid-cnt').length).toBe(1);
  });

  it('should render list of GridItem', () => {
    const component = shallow(<Grid data={mockData} />);
    expect(component.find(GridItem).length).toBe(2);
  });

  it('should render first element image type', () => {
    const component = shallow(<Grid selectedIndex={0} data={mockData} />);
    expect(
      component
        .find(GridItem)
        .at(0)
        .props().type
    ).toStrictEqual(['image', 'common']);
  });

  it('should render second element video type', () => {
    const component = shallow(<Grid selectedIndex={0} data={mockData} />);
    expect(
      component
        .find(GridItem)
        .at(1)
        .props().type
    ).toStrictEqual(['video', 'common']);
  });

  it('should render under mobile each 4, 14, 22, 40, 50, 58... element with type bigItem', () => {
    const mockDataItems = new Array(50).fill('').map(item => mockData[0]);
    const component = shallow(<Grid isMobile data={mockDataItems} />);

    [4, 14, 22, 40, 50].forEach(el => {
      expect(
        component
          .find(GridItem)
          .at(el - 1)
          .props()
          .type.includes(GridItemType.bigItem)
      ).toBeTruthy();
    });
  });

  it('should call onItemClick', () => {
    const onItemClick = jest.fn();
    const component = mount(<Grid onItemClick={onItemClick} data={mockData} />);

    component
      .find('.js-grid-item')
      .at(0)
      .simulate('click');
    expect(onItemClick).toHaveBeenCalled();
  });
});
