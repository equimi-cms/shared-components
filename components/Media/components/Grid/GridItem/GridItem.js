//@flow
import React from 'react';
import './GridItem.scss';
import classNames from 'classnames';
import Image from '../../../../../Image/Image';
import playIcon from './imgs/youtube-official-icon.svg';
import {renderTypesAsClassNames} from '../../../../../utils/utils';
import {IMedia} from '../../../../../utils/type';
import {getMediaPreview} from '../../../utils/utils';

export const GridItemType = {
  image: 'image',
  video: 'video',
  videoSmall: 'video-small',
  bigItem: 'big-item',
};

interface IGridItemProps {
  className?: String;
  data: IMedia;
  isSelected?: Boolean;
  type: GridItemType | Array<GridItemType>;
  _ref: React.Ref;
  index: Number;
  onClick: (index: Number, data: IMedia, e: Event) => null;
}

export const GridItem = ({
  className,
  data,
  isSelected,
  type,
  _ref,
  index,
  onClick,
}: IGridItemProps) => {
  const _type: [GridItemType] = Array.isArray(type) ? type : [type];
  const isVideoType = _type.includes(GridItemType.video) || _type.includes(GridItemType.videoSmall);
  const isBigItem = _type.includes(GridItemType.bigItem);
  const {title,id} = data;
  const imageSrc = getMediaPreview(data, isBigItem ? '880x880' : '406x406');

  const overContent = isVideoType && (
    <div className="grid-item__content-over js-grid-item-play">
      <Image className="grid-item__content-over-img" src={playIcon} />
    </div>
  );
  const handleOnClick = e => onClick(index, data, e);

  return (
    <picture
      tabIndex={0}
      data-id={id}
      onClick={handleOnClick}
      ref={_ref}
      className={classNames(
        'grid-item js-grid-item',
        renderTypesAsClassNames(type, 'grid-item--type-'),
        isSelected && 'grid-item--selected',
        className
      )}
    >
      {imageSrc ? (
        <Image className="grid-item__img js-grid-item-image" alt={title} src={imageSrc} />
      ) : (
        <span className='grid-item__no-data'>Broken data</span>
      )}

      {overContent}
    </picture>
  );
};

export default React.memo(GridItem);

GridItem.defaultProps = {
  data: {},
  type: GridItemType.image,
  onClick: () => null,
};
