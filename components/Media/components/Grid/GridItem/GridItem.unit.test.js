import React from 'react';
import {shallow} from 'enzyme/build';
import {GridItem, GridItemType} from './GridItem';

const mockData = {
  title: 'Test',
  content: {
    videoId: 1,
    image: 'image.src',
  },
};

const mockDataVideo = {
  title: 'Test',
  content: {
    videoId: 1,
  },
};

describe('GridItem', () => {
  it('should render', () => {
    const component = shallow(<GridItem data={mockData} />);
    expect(component.find('.js-grid-item').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<GridItem className="test" data={mockData} />);
    expect(component.find('.js-grid-item').hasClass('test')).toBeTruthy();
  });

  it('should render as selected', () => {
    const component = shallow(<GridItem isSelected data={mockData} />);
    expect(component.find('.js-grid-item').hasClass('grid-item--selected')).toBeTruthy();
  });

  it('should render type as className', () => {
    const component = shallow(<GridItem type={['type-1']} data={mockData} />);
    expect(component.find('.js-grid-item').hasClass('grid-item--type-type-1')).toBeTruthy();
  });

  it('should render type image', () => {
    const component = shallow(<GridItem type={GridItemType.image} data={mockData} />);
    expect(component.find('.js-grid-item-play').length).toBeFalsy();
    expect(component.find('.js-grid-item-image').props().src).toContain(mockData.content.image);
  });

  it('should render type video', () => {

    const component = shallow(<GridItem type={GridItemType.video} data={mockDataVideo} />);
    expect(component.find('.js-grid-item-play').length).toBeTruthy();
    expect(component.find('.js-grid-item-image').props().src).toBe(
      'https://img.youtube.com/vi/1/hqdefault.jpg'
    );
  });

  it('should call onClick', () => {
    const onClick = jest.fn();
    const component = shallow(<GridItem index={5} onClick={onClick} data={mockData} />);
    component.find('.js-grid-item').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
