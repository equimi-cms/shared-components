//@flow
import React, {useMemo} from 'react';
import './Grid.scss';
import classNames from 'classnames';
import GridItem, {GridItemType} from './GridItem/GridItem';
import Scroll from '../../../../components/Scroll/Scroll';
import {IMedia} from '../../../../utils/type';
import {isImageType, isVideoType} from '../../utils/utils';
import GridItemState from './GridItemState/GridItemState';
import Loader from '../../../../Loader/Loader';
import NoDataTitleLocalized from '../NoDataTitleLocalized/NoDataTitleLocalized';

/**
 * Render images grid for mobile
 * Each 4, 14, 22, 40, 50, 58... element must to have class
 *
 * Grid layout
 *    0 0 0
 *    4 4 0
 *    4 4 0
 *    0 0 0
 *    0 0 0
 *    0 1 4
 *    0 1 4
 *    0 0 0
 *    0 0 0
 *    2 2 0
 *    2 2 0
 *    0 0 0
 *    0 0 0
 */
const startFromIndex = 4;

interface IGridProps {
  data: Array<IMedia>;
  isMobile: Boolean;
  isLoading: Boolean;
  scrollBoundary: String;
  onItemClick: (index: Number, data: IMedia) => void;
  onLoadMore: () => void;
}

export const Grid = ({
  data,
  isMobile,
  isLoading,
  scrollBoundary,
  onItemClick,
  onLoadMore,
}: IGridProps) => {
  let _gridIndex = startFromIndex;
  let _gridInc = 10;

  const isBigItem = index => {
    let isBigItem = false;
    if (_gridIndex === index + 1) {
      _gridIndex += _gridInc;
      _gridInc = _gridInc === 10 ? 8 : 10;
      isBigItem = true;
    }
    return isBigItem;
  };

  const handleScroll = ([isReachScrollBoundary]) => {
    isReachScrollBoundary && onLoadMore();
  };

  const list = useMemo(
    () =>
      data.map((itemData, index) => {
        const type = [];
        isImageType(itemData) && type.push(GridItemType.image);
        isVideoType(itemData) && type.push(GridItemType.video);
        return (
          <GridItem
            key={itemData.id || index}
            index={index}
            data={itemData}
            onClick={onItemClick}
            type={[...type, isMobile && isBigItem(index) ? GridItemType.bigItem : 'common']}
          />
        );
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [data, isMobile]
  );
  const loader = (
    <div className="indent align-center test-loader">
      <Loader size={24} />
    </div>
  );
  const renderData = data.length ? (
    <div className={classNames('grid-images')}>{list}</div>
  ) : (
    <NoDataTitleLocalized />
  );

  return (
    <div className={classNames('grid-images-cnt js-grid-cnt')}>
      <GridItemState />
      <Scroll boundary={scrollBoundary} onScrollBoundary={handleScroll}>
        {isLoading ? loader : renderData}
      </Scroll>
    </div>
  );
};

export default React.memo(Grid);

/* istanbul ignore next */
Grid.defaultProps = {
  data: [],
  scrollBoundary: '80%',
  onItemClick: () => null,
  onLoadMore: () => null,
};
