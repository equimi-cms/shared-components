//@flow
import React from 'react';
import './MediaCalendarButton.scss';
import CalendarButton from '../../../../components/Calendar/CalendarButton/CalendarButton';

interface IMediaCalendarButtonProps {
  onClick: () => void;
}

export const MediaCalendarButton = ({onClick}: IMediaCalendarButtonProps) => {
  return (
    <div className="media-calendar-btn">
      <CalendarButton className="js-media-calendar-btn" onClick={onClick} type={'mod-1'} />
    </div>
  );
};

export default React.memo(MediaCalendarButton);

MediaCalendarButton.defaultProps = {
  onClick: () => null,
};
