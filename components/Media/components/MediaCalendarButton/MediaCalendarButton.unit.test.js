import React from 'react';
import {shallow} from 'enzyme/build';
import {MediaCalendarButton} from './MediaCalendarButton';

describe('MediaCalendarButton', () => {
  it('should render', () => {
    const component = shallow(<MediaCalendarButton />);
    expect(component.find('.js-media-calendar-btn').length).toBe(1);
  });

  it('should call onClick', () => {
    const onClick = jest.fn();
    const component = shallow(<MediaCalendarButton onClick={onClick} />);
    component.find('.js-media-calendar-btn').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
