//@flow
import React from 'react';
import './Link.scss';
import classNames from 'classnames';
import linkIcon from './imgs/link.svg';
import {Image} from '../../../../Image/Image';
import {renderTypesAsClassNames} from '../../../../utils/utils';

interface ILinkProps {
  as: String;
  children: React.ReactNode;
  className?: String;
  type?: '' | 'mod-1';
}

export const Link = ({as: Component, children, className, type, ...props}: ILinkProps) => {
  return (
    <Component
      {...props}
      className={classNames('s-link js-link', renderTypesAsClassNames(type, 's-link--'), className)}
    >
      <Image className="s-link__img" src={linkIcon} />
      {children}
    </Component>
  );
};

export default React.memo(Link);

Link.defaultProps = {
  as: 'a',
};
