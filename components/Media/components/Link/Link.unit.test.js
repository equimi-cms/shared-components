import React from 'react';
import {shallow} from 'enzyme/build';
import {Link} from './Link';

describe('Link', () => {
  it('should render', () => {
    const component = shallow(<Link />);
    expect(component.find('.js-link').length).toBe(1);
  });

  it('should render as link by default', () => {
    const component = shallow(<Link />);
    expect(component.find('a.js-link').length).toBe(1);
  });

  it('should render as div', () => {
    const component = shallow(<Link as={'div'} />);
    expect(component.find('div.js-link').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<Link className="test" />);
    expect(component.find('.js-link').hasClass('test')).toBeTruthy();
  });

  it('should render with type', () => {
    const component = shallow(<Link type={'mod-1'} className="test" />);
    expect(component.find('.js-link').hasClass('s-link--mod-1')).toBeTruthy();
  });

  it('should render with children', () => {
    const component = shallow(<Link>test</Link>);
    expect(
      component
        .find('.js-link')
        .props()
        .children.includes('test')
    ).toBeTruthy();
  });
});
