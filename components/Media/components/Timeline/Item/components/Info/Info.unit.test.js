import React from 'react';
import {shallow} from 'enzyme/build';
import {Info} from './Info';

describe('Info', () => {
  it('should render', () => {
    const component = shallow(<Info />);
    expect(component.find('.js-info').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<Info className="test" />);
    expect(component.find('.js-info').hasClass('test')).toBeTruthy();
  });

  it('should render date and views', () => {
    const component = shallow(<Info date={'2019-08-04T18:53:45.466Z'} views={10} />);
    expect(component.find('.js-month-date').props().children).toBe('August 04');
    expect(component.find('.js-year').props().children).toBe('2019');
    expect(component.find('.js-views').props().children).toBe(10);
  });
});
