//@flow
import React from 'react';
import './Info.scss';
import classNames from 'classnames';
import moment from 'moment';
import {PlatformContextConsumer} from '../../../../../mediaContext/PlatformContext';
import type {IPlatformContext} from '../../../../../mediaContext/PlatformContext';

interface IInfoProps extends IPlatformContext{
  className: String;
  date: String;
  views: Number;
}

export const Info = ({className, date, views, localization:{t}}: IInfoProps) => {
  const m = moment(date);
  const formattedMonthAndDate = m.format('MMMM DD');
  const formattedYear = m.format('YYYY');
  return (
    <div className={classNames('js-info', className)}>
      <div className="t-item-info">
        <time dateTime={date}>
          <span className="t-item-info-value js-month-date">{formattedMonthAndDate}</span>{' '}
          <span className="js-year">{formattedYear}</span>
        </time>
        <span className="t-item-info-views">
          <span className="t-item-info-value js-views">{views}</span>{' '}
          {t('Views')}
        </span>
      </div>
    </div>
  );
};

export default React.memo(PlatformContextConsumer(Info));

Info.defaultProps = {
  localization:{
    t:v=>v
  },
  views: 0,
};
