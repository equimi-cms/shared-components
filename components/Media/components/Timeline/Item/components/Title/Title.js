//@flow
import React from "react";
import "./Title.scss";
import classNames from "classnames";
import linkIcon from "./imgs/link.svg";
import {getPluralEntityTitle} from "../../../../../../../utils/utils";
import type {IHorse, INews, IMember} from "../../../../../../../utils/type";
import type {Entity} from "../../../../../../../utils/types";
import type {IPlatformContext} from "../../../../../mediaContext/PlatformContext";
import {PlatformContextConsumer} from "../../../../../mediaContext/PlatformContext";
import SvgInlineImage from "../../../../../../../Image/SvgInlineImage/SvgInlineImage";

interface ITitleProps extends IPlatformContext {
  className: String;
  entityType: Entity;
  entity: IHorse | IMember | INews;
}

export const Title = ({
  className,
  entity,
  entityType,
  navigation: {navToEntity},
  relatedTo,
}: ITitleProps) => {
  const entityTitle = entity && (entity.title || entity.name);
  const allowNavigation = entity.id !== relatedTo;

  const handleClick = () => {
    if (!allowNavigation) return;
    navToEntity(getPluralEntityTitle(entityType), entity && entity.id, entity);
  };

  return (
    <div onClick={handleClick} className={classNames("t-item-title js-item-title", className)}>
      {entityTitle && <span className="t-item-title__text js-item-content">{entityTitle}</span>}
      {allowNavigation && (
        <div className="t-item-title__link">
          {" "}
          <SvgInlineImage src={linkIcon} />
        </div>
      )}
    </div>
  );
};

export default React.memo(PlatformContextConsumer(Title));

Title.defaultProps = {
  entity: {},
  allowNavigation: true,
  navigation: {
    navToEntity: () => null,
  },
};
