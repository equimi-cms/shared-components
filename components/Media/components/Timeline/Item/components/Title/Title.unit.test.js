import React from 'react';
import {shallow} from 'enzyme/build';
import {Title} from './Title';

describe('Title', () => {
  it('should render', () => {
    const component = shallow(<Title />);
    expect(component.find('.js-item-title').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<Title className="test" />);
    expect(component.find('.js-item-title').hasClass('test')).toBeTruthy();
  });

  it('should render with entityTitle', () => {
    const component = shallow(<Title entity={{title:'test'}}/>);
    expect(component.find('.js-item-content').props().children).toBe('test');
  });
});
