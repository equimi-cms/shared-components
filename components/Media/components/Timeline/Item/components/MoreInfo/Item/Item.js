//@flow
import React from 'react';
import './Item.scss';
import classNames from 'classnames';
import {PlatformContextConsumer} from '../../../../../../mediaContext/PlatformContext';
import type {IPlatformContext} from '../../../../../../mediaContext/PlatformContext';

interface IItemProps extends IPlatformContext{
  title: String;
  className: String;
  count: Number;
  children: React.ReactElement;
}

export const Item = ({title, className, count, children,localization:{t}}: IItemProps) => {
  if (!count) return null;
  return (
    <dl className={classNames('t-info-item js-item', className)}>
      <dt>
        {t(title,{count})}
      </dt>
      <dd className="js-content">{children}</dd>
    </dl>
  );
};

export default React.memo(PlatformContextConsumer(Item));

Item.defaultProps = {
  data: [],
  children: null,
  localization:{
    t:v=>v
  },
};
