import React from 'react';
import {shallow} from 'enzyme/build';
import {Item} from './Item';

describe('Item', () => {
  it('should not render', () => {
    const component = shallow(<Item />);
    expect(component.find('.js-item').length).toBe(0);
  });

  it('should render with className', () => {
    const component = shallow(<Item count={1} className="test" />);
    expect(component.find('.js-item').hasClass('test')).toBeTruthy();
  });

  it('should render when count >0', () => {
    const component = shallow(<Item count={1} />);
    expect(component.find('.js-item').length).toBe(1);
  });

  it('should render with children', () => {
    const component = shallow(<Item count={1}>test</Item>);
    expect(component.find('.js-content').props().children).toBe('test');
  });
});
