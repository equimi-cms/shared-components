//@flow
import React, {useState} from 'react';
import './MoreInfo.scss';
import classNames from 'classnames';
import Image from '../../../../../../../Image/Image';
import arrowIcon from './imgs/arrow.svg';
import Item from './Item/Item';
import {PlatformContextConsumer} from '../../../../../mediaContext/PlatformContext';
import type {IPlatformContext} from '../../../../../mediaContext/PlatformContext';

interface IMoreInfoProps extends IPlatformContext {
  className: String;
  open: Boolean;
  children: React.ReactNode;
}

export const MoreInfo = ({className,open, children, localization: {t}}: IMoreInfoProps) => {
  const [openContent, setOpenContent] = useState(open);

  const toggleOpenContent = () => setOpenContent(!openContent);

  return (
    <div
      className={classNames('more-info js-more-info', openContent && 'more-info--open', className)}
    >
      <div className="more-info__content js-mi-content">{openContent && children}</div>
      <span className="more-info__title js-mi-more-info" onClick={toggleOpenContent}>
        {t(openContent ? 'less_info' : 'more_info')}{' '}<Image className="more-info-arrow" src={arrowIcon} />
      </span>
    </div>
  );
};

export default React.memo(PlatformContextConsumer(MoreInfo));

MoreInfo.Item = Item;
MoreInfo.defaultProps = {
  localization:{
    t:v=>v
  },
};
