import React from 'react';
import {shallow} from 'enzyme/build';
import {MoreInfo} from './MoreInfo';

describe('MoreInfo', () => {
  it('should render', () => {
    const component = shallow(<MoreInfo />);
    expect(component.find('.js-more-info').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<MoreInfo className="test" />);
    expect(component.find('.js-more-info').hasClass('test')).toBeTruthy();
  });

  it('should render with children', () => {
    const component = shallow(<MoreInfo open>test</MoreInfo>);
    expect(component.find('.js-mi-content').props().children).toBe('test');
  });

  it('should render closed by default', () => {
    const component = shallow(<MoreInfo>test</MoreInfo>);
    expect(component.find('.js-more-info').hasClass('more-info--open')).toBeFalsy();
  });

  it('should render opened when click more info', () => {
    const component = shallow(<MoreInfo>test</MoreInfo>);
    expect(component.find('.js-mi-more-info').props().children[0]).toBe('more_info');
    component.find('.js-mi-more-info').simulate('click');
    expect(component.find('.js-more-info').hasClass('more-info--open')).toBeTruthy();
    expect(component.find('.js-mi-more-info').props().children[0]).toBe('less_info');
  });
});
