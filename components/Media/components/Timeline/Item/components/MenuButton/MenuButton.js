//@flow
import React from 'react';
import './MenuButton.scss';
import classNames from 'classnames';
import {Image} from '../../../../../../../Image/Image';
import menuIcon from './imgs/menu.svg';

interface IMenuButtonProps {
  className: String;
  children: React.ReactElement;
}

export const MenuButton = ({className, children, ...props}: IMenuButtonProps) => {
  return (
    <div {...props} className={classNames('t-menu-btn js-menu-btn', className)}>
      <Image src={menuIcon} />
    </div>
  );
};

export default React.memo(MenuButton);

MenuButton.defaultProps = {};
