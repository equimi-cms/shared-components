//@flow
import React from "react";
import "./ShareButton.scss";
import classNames from "classnames";
import shareIcon from "./imgs/share.svg";
import SvgInlineImage from "../../../../../../../Image/SvgInlineImage/SvgInlineImage";

export interface IShareData {
  title: String;
  text: String;
  url: String;
}

interface IShareButtonProps {
  className: String;
  shareData: IShareData;
}

export const ShareButton = ({className, shareData, ...props}: IShareButtonProps) => {
  //share works only under https
  const isShareAllowed = !!navigator.share;

  const handleShare = () => {
    if (isShareAllowed && shareData) navigator.share(shareData);
  };

  return (
    <div
      {...props}
      onClick={handleShare}
      className={classNames(
        "share-button js-share",
        !isShareAllowed && "share-button--disabled",
        className
      )}
    >
      <div className="share-button__img">
        <SvgInlineImage src={shareIcon} />
      </div>
    </div>
  );
};

export default React.memo(ShareButton);

ShareButton.defaultProps = {};
