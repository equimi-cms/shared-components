import React from 'react';
import {shallow} from 'enzyme/build';
import {ShareButton} from './ShareButton';

const mockData = {title: 'title', text: 'text', url: '1'};

describe('ShareButton', () => {
  it('should render', () => {
    const component = shallow(<ShareButton />);
    expect(component.find('.js-share').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<ShareButton className="test" />);
    expect(component.find('.js-share').hasClass('test')).toBeTruthy();
  });

  it('should render button disabled when navigator isn`t available', () => {
    navigator.share = null;

    const component = shallow(<ShareButton className="test" />);
    expect(component.find('.js-share').hasClass('share-button--disabled')).toBeTruthy();
  });

  it('should render button active when navigator available and shareData passed', () => {
    navigator.share = () => {};
    const component = shallow(<ShareButton shareData={mockData} className="test" />);
    expect(component.find('.js-share').hasClass('share-button--disabled')).toBeFalsy();
  });

  it('should call share', () => {
    navigator.share = jest.fn();
    const component = shallow(<ShareButton shareData={mockData} className="test" />);
    component.find('.js-share').simulate('click');
    expect(navigator.share).toHaveBeenCalledWith(mockData);
  });
});
