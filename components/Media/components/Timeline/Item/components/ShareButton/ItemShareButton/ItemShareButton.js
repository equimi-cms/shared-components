//@flow
import React from "react";
import {getPluralEntityTitle} from "../../../../../../../../utils/utils";
import ShareButton from "../ShareButton";
import type {IMediaContext} from "../../../../../../mediaContext/MediaContext";
import {MediaContextConsumer} from "../../../../../../mediaContext/MediaContext";
import type {IPlatformContext} from "../../../../../../mediaContext/PlatformContext";
import {PlatformContextConsumer} from "../../../../../../mediaContext/PlatformContext";
import {getEntityTitle} from "../../../../../../../../utils/entity";

interface IItemShareButtonProps extends IMediaContext, IPlatformContext {}

export const ItemShareButton = ({data, navigation: {getLinkToEntity}}: IItemShareButtonProps) => {
  const {title: _title, desc, entity = {id: null}, entityType} = data;

  const shareTitle = _title || getEntityTitle(entity);
  const title = shareTitle ? {title: shareTitle} : {};
  const text = desc ? {text: desc} : {};

  const shareData = {
    ...title,
    ...text,
    url:
      window.location.origin + getLinkToEntity(getPluralEntityTitle(entityType), entity.id, entity),
  };
  return <ShareButton shareData={shareData} />;
};

export default React.memo(PlatformContextConsumer(MediaContextConsumer(ItemShareButton)));

ItemShareButton.defaultProps = {
  data: {},
  navigation: {
    getLinkToEntity: () => null,
  },
};
