import React from 'react';
import {shallow} from 'enzyme/build';
import {ItemShareButton} from './ItemShareButton';
import ShareButton from '../ShareButton';

describe('ItemShareButton', () => {
  it('should render', () => {
    const component = shallow(<ItemShareButton />);
    expect(component.find(ShareButton).length).toBe(1);
  });

  it('should pass correct share data', () => {
    const mockData = {
      title: 'title1',
      desc: 'desc1',
      entity: {id: 1},
      entityType: 'member',
    };
    const component = shallow(<ItemShareButton data={mockData} navigation={{getLinkToEntity:()=>'/test'}} />);
    expect(component.find(ShareButton).props().shareData).toEqual({
      title:mockData.title,
      text:mockData.desc,
      url:'http://localhost/test'
    });
  });
});
