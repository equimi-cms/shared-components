//@flow
import React from "react";
import "../Item.scss";
import classNames from "classnames";
import Image from "../../../../../../Image/Image";
import MoreInfo from "../components/MoreInfo/MoreInfo";
import Title from "../components/Title/Title";
import Info from "../components/Info/Info";
import type {IMedia} from "../../../../../../utils/type";
import Item from "../components/MoreInfo/Item/Item";
import {getImageSrcWithSize} from "../../../../../../utils/utils";
import ItemShareButton from "../components/ShareButton/ItemShareButton/ItemShareButton";

interface IItemImageProps {
  className: string;
  data: IMedia;
  itemOptions: React.ReactElement;
}

export const ItemImage = ({className, data, itemOptions: ItemOptions}: IItemImageProps) => {
  const {
    title,
    created,
    results = [],
    members = [],
    horses = [],
    entity = {id: null},
    entityType,
    viewsCount = 0,
  } = data;
  const image = getImageSrcWithSize(data.content, "880x880");

  const resultsCount = results.length;
  const membersCount = members.length;
  const horsesCount = horses.length;

  return (
    <div tabIndex={0} className={classNames("t-item js-t-item", className)}>
      <Image className="t-item__img" alt={title} src={image} />
      <div className="t-item__container">
        <div className="t-item__content">
          <Title entity={entity} entityType={entityType} />
          <p className="t-item__desc">{title}</p>
          <Info date={created} views={viewsCount} />

          {resultsCount || membersCount || horsesCount ? (
            <MoreInfo className="t-item__more-info js-more-info">
              <Item className="js-results" title={"Results"} count={resultsCount}>
                {resultsCount &&
                  results.map(({showName, level}, index) => (
                    <p className="t-item-uppercase js-item" key={index}>
                      {showName} {level}
                    </p>
                  ))}
              </Item>
              <Item className="js-members" title={"Members"} count={membersCount}>
                {membersCount &&
                  members.map(({name}, index) => (
                    <p className="js-item" key={index}>
                      {name}
                    </p>
                  ))}
              </Item>
              <Item className="js-horses" title={"Horses"} count={horsesCount}>
                {horsesCount &&
                  horses.map(({name}, index) => (
                    <p className="js-item" key={index}>
                      {name}
                    </p>
                  ))}
              </Item>
            </MoreInfo>
          ) : null}
        </div>
        <ItemOptions data={data} isTimeline />
      </div>
    </div>
  );
};

export default React.memo(ItemImage);

ItemImage.defaultProps = {
  itemOptions: ItemShareButton,
  className: "",
};
