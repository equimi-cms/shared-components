import React from 'react';
import {shallow} from 'enzyme/build';
import {ItemImage} from './ItemImage';
import {mockMember} from '../../../../../../../../_e2e/PublicPages/Members/mockData/mockData';

const mockData = {
  title: 'title',
  created: '2019-08-04T18:53:45.466Z',
  content: {
    image: '',
  },
  results: [],
  members: [],
  horses: [],
};

describe('ItemImage', () => {
  it('should render', () => {
    const component = shallow(<ItemImage data={mockData} />);
    expect(component.find('.js-t-item').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<ItemImage data={mockData} className="test" />);
    expect(component.find('.js-t-item').hasClass('test')).toBeTruthy();
  });

  it('should render without more info when no data', () => {
    const component = shallow(<ItemImage data={mockData} />);
    expect(component.find('.js-more-info').length).toBeFalsy();
  });

  it('should render with more info', () => {
    const mockDataWithMember = {...mockData, members: [mockMember]};
    const component = shallow(<ItemImage data={mockDataWithMember} />);
    expect(component.find('.js-more-info').length).toBeTruthy();
  });

  it('should render results', () => {
    const data = {...mockData, results: [{showName: '1', level: '1'}, {showName: '2', level: '2'}]};
    const component = shallow(<ItemImage data={data} />);
    expect(component.find('.js-results .js-item').length).toBe(2);
  });

  it('should render members', () => {
    const data = {...mockData, members: [{name: '1'}, {name: '2'}]};
    const component = shallow(<ItemImage data={data} />);
    expect(component.find('.js-members .js-item').length).toBe(2);
  });

  it('should render horses', () => {
    const data = {...mockData, horses: [{name: '1'}, {name: '2'}]};
    const component = shallow(<ItemImage data={data} />);
    expect(component.find('.js-horses .js-item').length).toBe(2);
  });
});
