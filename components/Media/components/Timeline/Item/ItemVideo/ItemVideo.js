//@flow
import React, {useState} from "react";
import "../Item.scss";
import classNames from "classnames";
import Image from "../../../../../../Image/Image";
import Title from "../components/Title/Title";
import Info from "../components/Info/Info";
import playIcon from "../imgs/youtube-official-icon.svg";
import type {IMedia} from "../../../../../../utils/type";
import {getYoutubeVideoEmbedUrl, getYoutubeVideoPreview} from "../../../../utils/utils";
import {PlatformContextConsumer} from "../../../../mediaContext/PlatformContext";
import ItemShareButton from "../components/ShareButton/ItemShareButton/ItemShareButton";

interface IItemVideoProps {
  className: String;
  data: IMedia;
  itemOptions: React.ReactElement;
}

export const ItemVideo = ({className, data, itemOptions: ItemOptions}: IItemVideoProps) => {
  const [renderVideo, setRenderVideo] = useState(false);
  const emitRenderVideo = () => setRenderVideo(true);
  const {title, created, entity, entityType, viewsCount = 0} = data;
  const {videoId} = data.content;

  return (
    <div tabIndex={0} className={classNames("t-item js-t-item", className)}>
      <div className="t-item__video">
        <Image className="t-item__video-img" src={getYoutubeVideoPreview(videoId)} />
        <div className="t-item__video-play-cnt js-play-button" onClick={emitRenderVideo}>
          <Image className="t-item__video-play" src={playIcon} />
        </div>
        {renderVideo && (
          <iframe
            title={title}
            width="560"
            height="315"
            src={getYoutubeVideoEmbedUrl(videoId, "?autoplay=1")}
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        )}
      </div>
      <div className="t-item__container">
        <div className="t-item__content">
          <Title entity={entity} entityType={entityType} />
          <Info date={created} views={viewsCount} />
        </div>
        <ItemOptions data={data} isTimeline />
      </div>
    </div>
  );
};

export default React.memo(PlatformContextConsumer(ItemVideo));

ItemVideo.defaultProps = {
  className: "",
  itemOptions: ItemShareButton,
};
