import React from 'react';
import {shallow} from 'enzyme/build';
import {ItemVideo} from './ItemVideo';

const mockData = {
  title: 'title',
  created: '2019-08-04T18:53:45.466Z',
  content: {
    id: 1,
  },
};

describe('ItemVideo', () => {
  it('should render', () => {
    const component = shallow(<ItemVideo data={mockData} />);
    expect(component.find('.js-t-item').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<ItemVideo data={mockData} className="test" />);
    expect(component.find('.js-t-item').hasClass('test')).toBeTruthy();
  });

  it('should render iframe when click play button', () => {
    const component = shallow(<ItemVideo data={mockData} className="test" />);

    expect(component.find('iframe').length).toBeFalsy();
    component.find('.js-play-button').simulate('click');
    expect(component.find('iframe').length).toBeTruthy();
  });
});
