//@flow
import React, {createRef, useEffect, useMemo, useState} from "react";
import "./Timeline.scss";
import MediaCalendarButton from "../MediaCalendarButton/MediaCalendarButton";
import {Aside} from "../../../../components/Aside/Aside";
import ItemImage from "./Item/ItemImage/ItemImage";
import ItemVideo from "./Item/ItemVideo/ItemVideo";
import type {IMediaContext} from "../../mediaContext/MediaContext";
import {MediaContextConsumer} from "../../mediaContext/MediaContext";
import Scroll from "../../../../components/Scroll/Scroll";
import MediaCalendar from "../MediaCalendar/MediaCalendar";
import {isImageType} from "../../utils/utils";
import type {IPlatformContext} from "../../mediaContext/PlatformContext";
import {PlatformContextConsumer} from "../../mediaContext/PlatformContext";
import Loader from "../../../../Loader/Loader";
import NoDataTitleLocalized from "../NoDataTitleLocalized/NoDataTitleLocalized";

interface ITimelineProps extends IMediaContext, IPlatformContext {
  open: Boolean;
  isLoading: Boolean;
  data: [];
  selectedCalendarData: {};
  timeLine: [];
  scrollBoundary: String;
  itemOptions: React.ReactNode;
  onClose: () => void;
  onLoadMore: () => void;
  onPickTime: () => void;
  className?: String;
}

/**
 * Timeline
 * For performance reason Timeline always in dom after it's opened
 * When user select media from the Grid component it will be instantly navigated to Timeline
 */
export const Timeline = ({
  open,
  isLoading,
  data,
  selectedCalendarData,
  timeLine,
  scrollBoundary,
  itemOptions,
  onClose,
  onLoadMore,
  onPickTime,
  selectedIndex,
  localization: {t},
  className,
}: ITimelineProps) => {
  const [showCalendar, setShowCalendar] = useState(false);
  const scrollRef = createRef();

  const handleScroll = ([isReachScrollBoundary]) => {
    isReachScrollBoundary && onLoadMore();
  };

  const emitToggleCalendar = () => setShowCalendar(!showCalendar);

  useEffect(() => {
    if (!open) return;
    const item = document.querySelectorAll(".js-t-item")[selectedIndex];
    const scroll = scrollRef.current;
    if (item && scroll) scroll.scrollTop(item.offsetTop);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);

  const renderList = useMemo(
    () =>
      data.map((itemData, index) => {
        return isImageType(itemData) ? (
          <ItemImage itemOptions={itemOptions} data={itemData} key={index} />
        ) : (
          <ItemVideo itemOptions={itemOptions} data={itemData} key={index} />
        );
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [data]
  );

  const loader = (
    <div className="indent align-center test-loader">
      <Loader size={24} />
    </div>
  );
  const renderData = data.length ? (
    <div className="timeline__content">{renderList}</div>
  ) : (
    <NoDataTitleLocalized />
  );

  return (
    <Aside
      open={open}
      preload
      className={`js-aside-timeline ${className}`}
      isBackBtn
      backBtnType={"arrowLeft"}
      asideType={["transparent-overlay"]}
      title={t("Media_Timeline")}
      contentType={"no-padding"}
      hideScrollOnOpen={false}
      withCustomScroll={false}
      removeFromDomOnClose={false}
      headerRight={<MediaCalendarButton onClick={emitToggleCalendar} />}
      onBack={onClose}
    >
      <MediaCalendar
        open={showCalendar}
        timeLine={timeLine}
        selectedData={selectedCalendarData}
        onClose={emitToggleCalendar}
        onPickTime={onPickTime}
      />
      <div className="timeline">
        <Scroll scrollRef={scrollRef} boundary={scrollBoundary} onScrollBoundary={handleScroll}>
          {isLoading ? loader : renderData}
        </Scroll>
      </div>
    </Aside>
  );
};

export default React.memo(PlatformContextConsumer(MediaContextConsumer(Timeline)));

Timeline.defaultProps = {
  open: false,
  data: [],
  timeLine: [],
  scrollBoundary: "90%",
  localization: {
    t: (v) => v,
  },
  onClose: () => null,
  onPickTime: () => null,
  onLoadMore: () => null,
};
