import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {Timeline} from './Timeline';
import ItemImage from './Item/ItemImage/ItemImage';
import ItemVideo from './Item/ItemVideo/ItemVideo';
import MediaCalendar from '../MediaCalendar/MediaCalendar';

const mockData = [
  {
    title: 'Image title',
    content: { image: 'image.svg'},
  },
  {
    title: 'Video title',
    content: {videoId: 2},
  },
];

describe('Timeline', () => {
  it('should render', () => {
    const component = shallow(<Timeline />);
    expect(component.find('.js-aside-timeline').length).toBe(1);
  });

  it('should render image and video items', () => {
    const component = shallow(<Timeline data={mockData} />);
    expect(component.find(ItemImage).length).toBe(1);
    expect(component.find(ItemVideo).length).toBe(1);
  });

  it('should toggle calendar view when click calendar button', () => {
    window.unitTestMode = true;
    const component = mount(<Timeline data={mockData} />);
    //open
    component
      .find('.js-media-calendar-btn')
      .at(0)
      .simulate('click');
    expect(component.find('.js-media-calendar').at(0).props().open).toBeTruthy();
    //close
    component
      .find('.js-media-calendar-btn')
      .at(0)
      .simulate('click');
    expect(component.find('.js-media-calendar').at(0).props().open).toBeFalsy();
  });

  it('should call onClose when press back button', () => {
    window.unitTestMode = true;
    const onClose = jest.fn();
    const component = mount(<Timeline onClose={onClose} data={mockData} />);
    component
      .find('.test-back-btn')
      .at(0)
      .simulate('click');
    expect(onClose).toHaveBeenCalled();
  });
});
