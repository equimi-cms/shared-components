//@flow
import React, {useCallback} from 'react';
import Calendar from '../../../../components/Calendar/Calendar';
import {useMobileStatus} from '../../../../hooks/MobileStatus';
import {useTabletStatus} from '../../../../hooks/TabletStatus';
import {PlatformContextConsumer} from '../../mediaContext/PlatformContext';
import type {IPlatformContext} from '../../mediaContext/PlatformContext';

interface IMediaCalendarProps extends IPlatformContext {
  open: Boolean;
  timeLine: [];
  onClose: () => null;
  onPickTime: (data: {
    dateFrom: String | null,
    dateTo: String | null,
    year: Number,
    month?: Number,
  }) => null;
}

export const MediaCalendar = ({
  open,
  timeLine,
  onClose,
  localization: {t},
  onPickTime,
  ...otherProps
}: IMediaCalendarProps) => {
  const isMobile = useMobileStatus();
  const isTablet = useTabletStatus();

  const handlePickYear = useCallback(
    year =>
      onPickTime({
        dateFrom: new Date(year, 0).getTime(),
        dateTo: new Date(year + 1, 0).getTime(),
        year,
      }),
    [onPickTime]
  );
  const handlePickMonthAndYear = useCallback(
    ({year, month}) =>
      onPickTime({
        dateFrom: new Date(year, month).getTime(),
        dateTo: new Date(year, month + 1).getTime(),
        year,
        month,
      }),
    [onPickTime]
  );

  const handleClear = useCallback(
    () => onPickTime({dateFrom: null, dateTo: null}),
    [onPickTime]
  );

  return (
    <Calendar
      {...otherProps}
      open={open}
      onClose={onClose}
      className={'media-calendar js-media-calendar'}
      calendarBtnClass={'.js-media-calendar-btn'}
      timeLine={timeLine}
      clearBtnText={t('Clear')}
      countOfEntitiesTitle={t('media')}
      applyBtnText={t('Apply')}
      cancelBtnText={t('Cancel')}
      searchDataTitle={t('All_media')}
      emptySearchDataTitle={t('Select_month_or_year_you_want_to_display')}
      isMobile={isMobile}
      isTablet={isTablet}
      onPickMonthAndYear={handlePickMonthAndYear}
      onPickYear={handlePickYear}
      onClear={handleClear}
    />
  );
};

export default React.memo(PlatformContextConsumer(MediaCalendar));

/* istanbul ignore next */
MediaCalendar.defaultProps = {
  timeLine: [],
  localization: {
    t: v => v,
  },
  onPickTime: () => null,
  onClose: () => null,
};
