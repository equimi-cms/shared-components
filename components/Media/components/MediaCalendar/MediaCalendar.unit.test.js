import React from 'react';
import {shallow} from 'enzyme/build';
import {MediaCalendar} from './MediaCalendar';
import Calendar from '../../../../components/Calendar/Calendar';

describe('MediaCalendar', () => {
  it('should render', () => {
    const component = shallow(<MediaCalendar />);
    expect(component.find(Calendar).length).toBe(1);
  });
});
