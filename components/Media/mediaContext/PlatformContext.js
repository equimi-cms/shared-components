import React, { Component } from "react";
import type { Entity } from "../../../utils/types";

export interface IPlatformContext {
  localization: { t: () => void };
  //entity id
  relatedTo: Number;
  navigation: {
    navToEntity: (entityType: Entity, entityId: String) => void,
    getLinkToEntity: (entityType: Entity, entityId: String, entity) => String
  };
}

const context: IPlatformContext = {
  localization: {
    t: v => v
  },
  relatedTo: null,
  navigation: {
    navToEntity: () => null,
    getLinkToEntity: () => null
  }
};

export const PlatformContext = React.createContext(context);

export const PlatformContextConsumer = WrapperComponent =>
  class Wrp extends Component {
    render() {
      return (
        <PlatformContext.Consumer>
          {props => <WrapperComponent {...props} {...this.props} />}
        </PlatformContext.Consumer>
      );
    }
  };
