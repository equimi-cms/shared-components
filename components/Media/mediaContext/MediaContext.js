import React, { Component } from "react";

export interface IMediaContext {
  selectedItem: {};
  selectedIndex: Number;
  isSelectedLast: Boolean;
  onNext: () => void;
  onPrev: () => void;}

const context: IMediaContext = {
  selectedItem: {},
  selectedIndex: 0,
  isSelectedLast: false,
  onNext: () => null,
  onPrev: () => null
};

export const MediaContext = React.createContext(context);

export const MediaContextConsumer = WrapperComponent =>
  class Wr extends Component {
    render() {
      return (
        <MediaContext.Consumer>
          {props => <WrapperComponent {...props} {...this.props} />}
        </MediaContext.Consumer>
      );
    }
  };
