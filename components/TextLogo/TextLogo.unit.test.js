import React from 'react';
import {shallow} from 'enzyme/build';
import {TextLogo} from './TextLogo';
import * as fontService from '../../utils/font/font';

describe('TextLogo', () => {
  it('should render', () => {
    const component = shallow(<TextLogo />);
    expect(component.find('.test-logo').length).toBe(1);
  });

  it('should render with className', () => {
    const component = shallow(<TextLogo className="test" />);
    expect(component.find('.test-logo').hasClass('test')).toBeTruthy();
  });

  it('should render with children', () => {
    const component = shallow(<TextLogo>test</TextLogo>);
    expect(component.find('.test-logo').props().children).toBe('test');
  });

  it('should call getLogoFont with fontTitle and fontWeight', () => {
    fontService.getLogoFont = jest.fn();
    shallow(<TextLogo fontTitle={'fontTitle'} fontWeight={'fontWeight'} />);
    expect(fontService.getLogoFont).toHaveBeenCalledWith('fontTitle', 'title', 'fontWeight', false);
  });
});
