//@flow
import React, { useMemo } from "react";
import "./TextLogo.scss";
import classNames from "classnames";
import { getGoogleFontSrc, getLogoFont } from "../../utils/font/font";
import { useMobileStatus } from "../../../../hooks/MobileStatus";

export interface ITextLogoProps {
  className?: string;
  fontTitle: string;
  fontWeight: number;
  children: React$Component;
  headComponent: React$Component;
}

export const TextLogo = ({
  className,
  fontTitle,
  fontWeight,
  children,
  headComponent: HeadComponent
}: ITextLogoProps) => {
  const isMobile = useMobileStatus();

  const fontSrc = useMemo(
    () =>
      getGoogleFontSrc([
        {
          title: fontTitle,
          weight: fontWeight
        }
      ]),
    [fontTitle, fontWeight]
  );

  const { fontFamily, styles } = useMemo(
    () => getLogoFont(fontTitle, "title", fontWeight, isMobile) || {},
    [fontTitle, fontWeight, isMobile]
  );

  const logoStyle = useMemo(() => ({ fontFamily, ...styles }), [
    fontFamily,
    styles
  ]);

  return (
    <>
      <HeadComponent>
        <link href={fontSrc} rel="stylesheet" />
      </HeadComponent>
      <span className={classNames("g-text-logo test-logo", className)} style={logoStyle}>
        {children}
      </span>
    </>
  );
};

export default React.memo(TextLogo);

TextLogo.defaultProps = {};
