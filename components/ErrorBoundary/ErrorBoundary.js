//@flow
import React from "react";
import useErrorBoundary from "use-error-boundary";

interface IErrorBoundaryProps {
  children?: any;
}

const ErrorBoundary = ({ children }: IErrorBoundaryProps) => {
  const { ErrorBoundary, didCatch } = useErrorBoundary();

  return (
    <>
      {didCatch ? (
        <p>
          There was an error while rendering this section. We are fixing it...
        </p>
      ) : (
        <ErrorBoundary>{children}</ErrorBoundary>
      )}
    </>
  );
};

export default React.memo(ErrorBoundary);

ErrorBoundary.defaultProps = {};
