//@flow
import React from 'react';
import './NoDataTitle.scss';
import classNames from 'classnames';

interface INoDataTitleProps {
  className: String;
  children: React.ReactElement;
}

export const NoDataTitle = ({className, children, ...props}: INoDataTitleProps) => {
  return (
    <div {...props} className={classNames('no-data-title', className)}>
      {children}
    </div>
  );
};

export default React.memo(NoDataTitle);

NoDataTitle.defaultProps = {

};
