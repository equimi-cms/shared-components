//@flow
import React, {useCallback} from 'react';
import './ServiceItem.scss';
import classNames from 'classnames';
import Image from '../../../Image/Image';
import placeholderImage from './imgs/placeholder-icon.svg';
import SvgInlineImage from '../../../Image/SvgInlineImage/SvgInlineImage';
import type {IService} from '../../../utils/type';

interface IServiceItemProps {
  className?: String;
  type?: 'dashboard' | 'sidebar';
  title?: String;
  imgSrc?: String;
  status?: any;
  children?: Element;
  translate: (v: string) => void;
  onClick: (data: IService) => void;
  allData: IService;
}

export const ServiceItem = ({
  className,
  translate,
  status,
  type,
  title,
  imgSrc,
  onClick,
  allData,
  ...props
}: IServiceItemProps) => {
  const handleClick = useCallback(() => {
    onClick(allData);
  }, [onClick, allData]);

  return (
    <div
      {...props}
      className={classNames(
        'm-service-item test-service-item',
        className,
        type && 'm-service-item--' + type
      )}
      onClick={handleClick}
    >
      <div className="m-service-item__img-cnt">
        {imgSrc ? (
          <Image className="m-service-item__img" src={imgSrc} />
        ) : (
          <SvgInlineImage className="m-service-item__img-placeholder" src={placeholderImage} />
        )}
      </div>
      <h6 className="m-service-item__title">{title}</h6>
    </div>
  );
};

export default React.memo(ServiceItem);

ServiceItem.defaultProps = {
  className: '',
  type: 'dashboard',
  title: '',
  imgSrc: '',
  status: '',
  children: '',
  allData: {},
  translate: v => v,
  onClick: () => null,
};
