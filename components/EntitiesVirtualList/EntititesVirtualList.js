//@flow
import React from "react";
import { FixedSizeList } from "react-window";
import { Scroll } from "../Scroll/Scroll";

export interface IItemProps {
  data: any;
  index: Number;
  style: any;
}
interface IEntitiesVirtualListProps<IItemProps> {
  className: string;
  scrollClassName: string;
  height: number;
  entities: [];
  renderItem: IItemProps => JSX.Element;
  itemSize: number;
  otherBoundaries?: [string] | string;
  onOtherScrollBoundaries: () => void;
  onLoadItemsScrollBoundary: () => void;
  loadItemsWhileScrollBoundaryIs: string;
  onScrollGone: () => void;
  style?: () => void;
  onItemClick: (itemData: any) => void;
}
export class EntitiesVirtualList extends React.Component<
  IEntitiesVirtualListProps<IItemProps>
> {
  listRef: React.RefObject<any> = React.createRef();
  loadItemsBoundary: String = this.props.loadItemsWhileScrollBoundaryIs;

  handleScroll: MouseWheelEvent = ({ target }: EventTarget) => {
    const { scrollTop } = target;

    this.listRef.current.scrollTo(scrollTop);
  };

  onScrollBoundary: [string] = res => {
    const isLoadItemsBoundaryTaken: Boolean = res.find(el =>
      el.hasOwnProperty(this.loadItemsBoundary)
    )[this.loadItemsBoundary];
    isLoadItemsBoundaryTaken && this.props.onLoadItemsScrollBoundary();
    this.props.onOtherScrollBoundaries(
      res.filter(el => !el.hasOwnProperty(this.loadItemsBoundary))
    );
  };
  render(): JSX.Element {
    const {
      renderItem: RenderItem,
      scrollClassName,
      loadItemsWhileScrollBoundaryIs,
      height,
      className,
      entities,
      itemSize,
      style,
      otherBoundaries,
      onScrollGone,
      onItemClick
    }: IEntitiesVirtualListProps = this.props;
    let boundary = [];
    boundary.push(loadItemsWhileScrollBoundaryIs);
    otherBoundaries &&
      (Array.isArray(otherBoundaries)
        ? (boundary = boundary.concat(otherBoundaries))
        : boundary.push(otherBoundaries));
    return (
      <Scroll
        className={scrollClassName}
        onScrollBoundaryWithKeys={this.onScrollBoundary}
        boundary={boundary}
        autoHeight={true}
        autoHeightMax={height}
        onScroll={this.handleScroll}
        onScrollGone={onScrollGone}
      >
        <FixedSizeList
          className={className}
          height={height}
          itemCount={entities.length}
          itemData={{ entities, onClick: onItemClick }}
          itemSize={itemSize}
          ref={this.listRef}
          style={{ ...style, overflow: false }}
        >
          {RenderItem}
        </FixedSizeList>
      </Scroll>
    );
  }
}

EntitiesVirtualList.defaultProps = {
  className: "",
  scrollClassName: "",
  renderItem: () => null,
  /**
   * can be string or array of string
   * 100% or [100%,80%]
   */
  loadItemsWhileScrollBoundaryIs: "100%", //%,px
  /**
   * when boundary is array of [80,50]
   * the onScrollBoundary will be called with array of [{80 : false},{50 : true}] when reach the boundary 50
   * the onScrollBoundary will be called with array of [{80 : true},{50 : true}] when reach the boundary 80
   */
  onLoadItemsScrollBoundary: () => null,
  onOtherScrollBoundaries: () => null,
  /**
   * called when scroll is hidden
   */
  onScrollGone: () => null,
  height: 40,
  entities: [],
  itemSize: 40,
  onItemClick: () => null
};
