import React from 'react';
import {shallow, mount} from 'enzyme';
import {EntitiesVirtualList, IItemProps} from './EntititesVirtualList';
import {FixedSizeList} from 'react-window';
import {Scroll} from '../Scroll/Scroll';

describe('EntitiesVirtualList', () => {
  it('should render list and scroll with given `className`', () => {
    const mockListClassName = 'test-list';
    const mockScrollClassName = 'test-scroll';
    const component = shallow(
      <EntitiesVirtualList className={mockListClassName} scrollClassName={mockScrollClassName} />
    );
    expect(component.find('.' + mockListClassName)).toHaveLength(1);
    expect(component.find('.' + mockScrollClassName)).toHaveLength(1);
  });
  it('should get `entities , itemSize and renderItem` props and pass `height, itemCount, itemData, itemSize, ref, style` props to the `FixedSizedList` and `data, style, index` to the `renderItem`', () => {

    const mockEntities = [{number: 100}, {number: 300}, {number: 500}];
    const mockItemSize = 300;
    const MockRenderItem = ({data, index, style}: IItemProps) => <div>{data.entities[index].number}</div>;
    const component = mount(
      <EntitiesVirtualList
        entities={mockEntities}
        itemSize={mockItemSize}
        renderItem={MockRenderItem}
      />
    );
    const renderItem = component.find(FixedSizeList).props().children;
    expect(component.find(FixedSizeList).props().itemData.entities).toEqual(mockEntities);
    expect(component.find(FixedSizeList).props().itemSize).toEqual(mockItemSize);
    expect(renderItem).toEqual(MockRenderItem);
    expect(component.find(MockRenderItem)).toHaveLength(mockEntities.length);
    expect(component.find(MockRenderItem).map(el => el.text())).toEqual(
      mockEntities.map(el => el.number.toString())
    );
  });
  it('should get `otherBoundaries ,  loadItemsWhileScrollBoundaryIs` calculate `boundary` and pass it to the `Scroll` and get `onOtherScrollBoundaries,onLoadScrollBoundaries,` and call it', () => {
    const mockOtherBoundaries = ['80%', '100%', '40%'];
    const mockLoadBoundary = '30%';
    const mockOnScrollGone = jest.fn();
    const mockOnLoadItemsScrollBoundary = jest.fn();
    const mockOnOtherScrollBoundaries = jest.fn();
    const expectedBoundary = [mockLoadBoundary].concat(mockOtherBoundaries);
    const component = mount(
      <EntitiesVirtualList
        loadItemsWhileScrollBoundaryIs={mockLoadBoundary}
        otherBoundaries={mockOtherBoundaries}
        onOtherScrollBoundaries={mockOnOtherScrollBoundaries}
        onScrollGone={mockOnScrollGone}
        onLoadItemsScrollBoundary={mockOnLoadItemsScrollBoundary}
      />
    );
    const scrollProps = component.find(Scroll).props();
    expect(scrollProps.boundary).toEqual(expectedBoundary);
    component.instance().onScrollBoundary([{'80%': true}, {'30%': false}]);
    expect(mockOnOtherScrollBoundaries).toHaveBeenCalled();
    expect(mockOnLoadItemsScrollBoundary).not.toHaveBeenCalled();
    component.instance().onScrollBoundary([{'40%': true}, {'30%': true}]);
    expect(mockOnOtherScrollBoundaries).toHaveBeenCalled();
    expect(mockOnLoadItemsScrollBoundary).toHaveBeenCalled();
    expect(scrollProps.onScrollGone).toEqual(mockOnScrollGone);
  });
});
