/* eslint-disable import/first */
import React, {useState} from 'react';
import {storiesOf} from '@storybook/react';
import {EntitiesVirtualList} from './EntititesVirtualList';

storiesOf('Shared/EntitiesVirtualList', module)
  .add('default', () => <List />)
  .add('with other boundaries', () => <List otherBoundaries={['20%', '30%']} />);
interface IListProps {
  otherBoundaries: [string] | string;
}
const List = (props: IListProps) => {
  const loadItemsBoundary = '80%';
  const [LoadBoundaryTaken, setLoadBoundaryTaken] = useState(false);
  const [FirstBoundaryTaken, setFirstBoundaryTaken] = useState(false);
  const [SecondBoundaryTaken, setSecondBoundaryTaken] = useState(false);
  const handleOtherScrollBoundaries = res => {
    const isFirstBoundaryTaken = res.find(el => el.hasOwnProperty('20%'))['20%'];
    isFirstBoundaryTaken && setFirstBoundaryTaken(isFirstBoundaryTaken);
    const isSecondBoundaryTaken = res.find(el => el.hasOwnProperty('30%'))['30%'];
    isSecondBoundaryTaken && setSecondBoundaryTaken(isSecondBoundaryTaken);
  };
  const handleLoadScrollBoundary = () => {
    setLoadBoundaryTaken(true);
  };
  const entities = new Array(500);
  const Row = ({index, style}: IItemProps) => <div style={style}>Row {index}</div>;
  const {otherBoundaries} = props;
  return (
    <div style={{width: 400, background: ' #F2F2F2'}}>
      <EntitiesVirtualList
        otherBoundaries={otherBoundaries}
        onOtherScrollBoundaries={handleOtherScrollBoundaries}
        loadItemsWhileScrollBoundaryIs={loadItemsBoundary}
        onLoadItemsScrollBoundary={handleLoadScrollBoundary}
        renderItem={Row}
        width={400}
        itemSize={45}
        entities={entities}
        height={300}
      />
      {FirstBoundaryTaken ? <div>FirstBoundary taken</div> : null}
      {SecondBoundaryTaken ? <div>SecondBoundary taken</div> : null}
      {LoadBoundaryTaken ? <div>LoadBoundary taken</div> : null}
    </div>
  );
};
