/* eslint-disable import/first */
import React, {useState} from 'react';
import {storiesOf} from '@storybook/react';
import {SearchBar} from './SearchBar';
import {Aside} from '../Aside/Aside';

const mockNewsTimeLine = [
  {'2018-01': 3},
  {'2018-04': 0},
  {'2018-07': 0},
  {'2018-10': 0},
  {'2018-02': 0},
  {'2018-05': 0},
  {'2018-08': 3},
  {'2018-11': 0},
  {'2018-03': 0},
  {'2018-06': 0},
  {'2018-09': 0},
  {'2018-12': 0},
  {'2017-01': 3},
  {'2017-04': 1},
  {'2017-07': 2},
  {'2017-10': 3},
  {'2017-02': 8},
  {'2017-05': 3},
  {'2017-08': 3},
  {'2017-11': 8},
  {'2017-03': 0},
  {'2017-06': 0},
  {'2017-09': 1},
  {'2017-12': 5},
];
storiesOf('Shared/SearchBar', module)
  .add('default', () => (
    <Aside contentType="no-padding" open={true}>
      <SearchBar />
    </Aside>
  ))
  .add('with time line of count for month and years', () => {
    return (
      <Aside contentType="no-padding" open={true}>
        <SearchBar timeLine={mockNewsTimeLine} countOfEntitiesTitle="news" />
      </Aside>
    );
  })
  .add('with custom text in search data', () => {
    return (
      <Aside contentType="no-padding" open={true}>
        <SearchBar
          emptySearchDataTitle="select date by clicking calendar button"
          searchDataTitle="you select all in"
          inputPlaceholder="enter your query here"
          timeLine={mockNewsTimeLine}
          countOfEntitiesTitle="news"
        />
      </Aside>
    );
  })
  .add('with actions', () => <WithActions />);
const WithActions = () => {
  const [message, setMessage] = useState('');
  const isYear2020 = (year: String | Number) => year === 2020;
  const handlePickYear = (year: Number) => {
    return isYear2020(year) && setMessage('Congrats!');
  };
  const handleSearch = (query: string) =>
    isYear2020(parseInt(query)) && setMessage('Even better congrats!!!');
  const handleClear = () => setMessage('');
  return (
    <Aside contentType="no-padding" open={true}>
      <SearchBar
        emptySearchDataTitle="you should select 2020 year"
        searchDataTitle={message ? ` happy` : 'you should select 2020 year  ,but you select '}
        inputPlaceholder="enter 2020 here"
        timeLine={mockNewsTimeLine}
        countOfEntitiesTitle="news"
        onPickYear={handlePickYear}
        onSearch={handleSearch}
        onClear={handleClear}
      />
      <div>{message}</div>
    </Aside>
  );
};
