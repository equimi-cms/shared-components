import React from 'react';
import {shallow,mount} from 'enzyme';
import {SearchBar} from './SearchBar';
import {AsideSearch} from '../../components/Aside/components/AsideSearch/AsideSearch';

describe('SearchBar', () => {
  const setup = props => shallow(<SearchBar {...props} />);
  it('should renders', () => {
    const component = setup();
    expect(component.find('.test-entities-search')).toHaveLength(1);
  });
  it('should render search input and handle search', () => {
    const mockOnSearch = jest.fn();
    const mockQuery = 'test';
    const comp = setup({onSearch: mockOnSearch});
    expect(comp.find(AsideSearch)).toHaveLength(1);
    comp.instance().handleSearch(mockQuery);
    expect(mockOnSearch).toHaveBeenCalled();
    expect(comp.state().text).toEqual(mockQuery);
  });
  it('should render calendar button and change it to active while calendar is open', () => {
    const comp = mount(<SearchBar />);
    expect(comp.find('.test-calendar-button')).toHaveLength(1);

    comp.setState({isCalendarOpen: true});

    expect(comp.find('.test-calendar-button_active')).toHaveLength(1);
  });
  it('should not render search data block on mobile while is empty', () => {
    const comp = mount(<SearchBar isMobile />);
    expect(comp.find('.test-entities-search-data')).toHaveLength(0);
  });

  it('should open and close calendar while clicking calendar toggle button', () => {
    const comp = mount(<SearchBar />);
    expect(comp.state().isCalendarOpen).toBe(false);
    comp.find('.test-calendar-button').simulate('click');
    expect(comp.state().isCalendarOpen).toBe(true);
    comp.find('.test-calendar-button').simulate('click');
    expect(comp.state().isCalendarOpen).toBe(false);
  });
});
