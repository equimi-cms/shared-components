//@flow
import React, {Component} from 'react';
import {AsideSearch} from '../Aside/components/AsideSearch/AsideSearch';
import CalendarButton from '../Calendar/CalendarButton/CalendarButton';
import Calendar from '../Calendar/Calendar';
import './SearchBar.scss';

export interface ISearchBarProps {
  isMobile: Boolean;
  isTablet: Boolean;

  onSearch: () => void;
  onPickMonthAndYear: (date: Date) => void;
  onPickYear: (date: Date) => void;
  onToggleCalendar: (isOpen: boolean) => void;
  onClear: () => void;
  timeLine: [];

  inputPlaceholder: String;
  searchDataTitle: any;
  emptySearchDataTitle: any;
  countOfEntitiesTitle: any;
  setIsCalendarDataVisible: Boolean => void;
  searchBtnText?: any;

  clearBtnText?: any;
  applyBtnText?: any;
  cancelBtnText?: any;
}

export class SearchBar extends Component<ISearchBarProps> {
  state = {
    text: '',
    isCalendarOpen: false,
  };

  handleSearch = value => {
    this.props.onSearch && this.setState({text: value}, () => this.props.onSearch(this.state.text));
  };
  handleToggleCalendar = () =>
    this.setState(
      state => ({
        isCalendarOpen: !state.isCalendarOpen,
      }),
      () => this.props.onToggleCalendar(this.state.isCalendarOpen)
    );

  emitCloseCalendar = () => {
    this.setState({isCalendarOpen: false}, this.props.onToggleCalendar(false));
  };

  render() {
    const {
      isMobile,
      isTablet,
      inputPlaceholder,
      searchDataTitle,
      emptySearchDataTitle,
      countOfEntitiesTitle,
      timeLine,
      searchBtnText,
      clearBtnText,
      applyBtnText,
      cancelBtnText,
      onPickMonthAndYear,
      onPickYear,
      setIsCalendarDataVisible,
      onClear
    } = this.props;
    const {isCalendarOpen} = this.state;
    return (
      <>
        <div className="entities-search test-entities-search">
          <div className="entities-search-container">
            <AsideSearch
              disableAutoFocus={isMobile && true}
              inputClassName="entities-search-input test-entities-search-input"
              isMobile={isMobile}
              isSearchBtn={false}
              placeholder={inputPlaceholder}
              onSearch={this.handleSearch}
              onChange={this.handleChange}
              hideClearButton={true}
              searchBtnText={searchBtnText}
            />

            <CalendarButton
              isActive={isCalendarOpen && !isMobile}
              className="entities-list-calendar-button js-calendar-button"
              onClick={this.handleToggleCalendar}
            />
          </div>
          <Calendar
            open={isCalendarOpen}
            onClose={this.emitCloseCalendar}
            isMobile={isMobile}
            isTablet={isTablet}
            timeLine={timeLine}
            searchDataTitle={searchDataTitle}
            emptySearchDataTitle={emptySearchDataTitle}
            countOfEntitiesTitle={countOfEntitiesTitle}
            onPickMonthAndYear={onPickMonthAndYear}
            setIsCalendarDataVisible={setIsCalendarDataVisible}
            onClear={onClear}
            onPickYear={onPickYear}
            clearBtnText={clearBtnText}
            applyBtnText={applyBtnText}
            cancelBtnText={cancelBtnText}
            calendarBtnClass='.js-calendar-button'
          />
        </div>
      </>
    );
  }
}
SearchBar.defaultProps = {
  setIsCalendarDataVisible: Boolean => null,
  timeLine: [],
  onClear: () => null,
  onSearch: () => null,
  onPickMonthAndYear: (date: Date) => null,
  onPickYear: (date: Date) => null,
  onToggleCalendar: (isOpen: boolean) => null,
  isMobile: false,

  isTablet: false,
  inputPlaceholder: '',
  searchDataTitle: '',
  emptySearchDataTitle: 'emptySearchDataTitle',
  countOfEntitiesTitle: '',
};
