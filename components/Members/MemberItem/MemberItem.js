//@flow
import React from "react";
import "./MemberItem.scss";
import classNames from "classnames";
import Image from "../../../Image/Image";
import SvgInlineImage from "../../../Image/SvgInlineImage/SvgInlineImage";
import memberPlaceholderImage from "./imgs/member.svg";
import type {IMember} from "../../../utils/type";

interface IMemberItemProps {
  className?: String;
  type?: "dashboard" | "sidebar";
  title?: String;
  imgSrc?: String;
  children?: React.Element;
  onClick?: (member: IMember) => void;
  allMemberData: IMember;
}

export const MemberItem = ({
  className,
  status,
  type,
  title,
  imgSrc,
  allMemberData,
  onClick,
  ...props
}: IMemberItemProps) => {
  const handleClick = () => {
    onClick(allMemberData);
  };
  return (
    <div
      {...props}
      className={classNames(
        "m-member-item test-member-item",
        className,
        type && "m-member-item--" + type
      )}
      onClick={handleClick}
    >
      <div className="m-member-item__img-cnt">
        {imgSrc ? (
          <Image className="m-member-item__img test-member-image" src={imgSrc} />
        ) : (
          <SvgInlineImage
            className="m-member-item__img-placeholder test-member-placeholder"
            src={memberPlaceholderImage}
          />
        )}
      </div>
      <h6 className="m-member-item__title test-member-title">{title}</h6>
    </div>
  );
};

export default React.memo(MemberItem);

MemberItem.defaultProps = {
  className: "",
  type: "dashboard",
  title: "",
  imgSrc: "",
  children: "",
  onClick: () => null,
  allMemberData: {},
};
