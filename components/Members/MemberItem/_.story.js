/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {MemberItem} from './MemberItem';

const MockItem = (props)=>(
  <MemberItem
    title={'Member title'}
    imgSrc={'https://via.placeholder.com/550'}
    {...props}
  />
);

storiesOf('Shared/Members/MemberItem', module)
  .add('default without data', () => (
      <MemberItem/>
  ))
  .add('with data', () => (
    <MockItem/>
  ))
  .add('with type dashboard', () => (
    <MockItem type={'dashboard'}/>
  ))
  .add('with type sidebar', () => (
    <MockItem type={'sidebar'}/>
  ));

