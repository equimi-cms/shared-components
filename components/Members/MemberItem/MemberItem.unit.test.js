import React from 'react';
import {shallow} from 'enzyme';
import {MemberItem} from './MemberItem';

describe('MemberItem', () => {

  it('should renders with default type `dashboard`', () => {
    const component = shallow(
      <MemberItem/>
    );
    expect(component.find('.test-member-item').length).toBe(1);
    expect(component.find('.test-member-item').hasClass('m-member-item--dashboard')).toBeTruthy();

  });

  it('should render className when pass prop className `test`', () => {
    const component = shallow(
      <MemberItem className={'test'}/>
    );
    expect(component.find('.test-member-item').hasClass('test')).toBeTruthy();
  });

  it('should renders with type', () => {
    const component = shallow(
      <MemberItem type={'sidebar'}/>
    );
    expect(component.find('.test-member-item').hasClass('m-member-item--sidebar')).toBeTruthy();
  });

  it('should render title', () => {
    const component = shallow(
      <MemberItem title={'testTitle'}/>
    );
    expect(component.find('.test-member-title').props().children).toBe('testTitle');
  });

  it('should render image when pass imgSrc', () => {
    const component = shallow(
      <MemberItem imgSrc={'testImage.png'}/>
    );
    expect(component.find('.test-member-image').length).toBe(1);
  });

  it('should render placeholder when no imgSrc', () => {
    const component = shallow(
      <MemberItem imgSrc={null}/>
    );
    expect(component.find('.test-member-image').length).toBe(0);
    expect(component.find('.test-member-placeholder').length).toBe(1);
  });
});
