// @flow
import React, {Fragment, PureComponent} from "react";
import {Aside} from "../../../components/Aside/Aside";
import {Button} from "../../../components/AsideButton/Button";
import "./MemberAside.scss";
import AsideSearchCnt from "../../../components/Aside/components/AsideSearchCnt/AsideSearchCnt";
import {LoadMore} from "../../../utils/LoadMore/LoadMore";
import MemberItem from "../MemberItem/MemberItem";
import {getImageSrcWithSize} from "../../../utils/utils";
import type {INews, IMember} from "../../../utils/type";
import AsideFiltering from "../../Aside/components/AsideFiltering/AsideFiltering";


interface IMemberAsideProps {
  className?: String;
  show?: Boolean;
  onClose?: () => void;
  onSelectItem?: () => void;
  searchMembers?: (
    query: any,
  ) => Promise<{payload: {rows: [], count: Number}}>;
  loadMoreMembers?: (
    query: any,
    nextOffset: Number,
  ) => Promise<{payload: {rows: [], count: Number}}>;
  isMobile?: Boolean;
  searchData?: {
    count: Number,
    rows: Array<INews>,
  };
  relation: {
    newsId: string,
    horseId: string,
    resultId: string,
    serviceId: string,
  };
  roles: [?string],
  asideProps?: {};
  asideSearchCntProps?: {};
  listItemClass?: String;
  itemClass?: String;
  children?: React.Element;
  listItem?: React.Element;
  listItemProps?: {};
  contentWrapper?: React.Element;
  onMemberClick?: (member: IMember) => void;
}

interface IMemberAsideState {
  showLoader: Boolean;
  role: string;
  showFiltering: boolean;
}

export class MemberAside extends PureComponent<IMemberAsideProps,
  IMemberAsideState> {
  constructor() {
    super();
    this.state = {
      showLoader: false,
      role: "",
      showFiltering: false,
    };
    this.searchQuery = "";
    this.lm = new LoadMore();
  }

  componentDidMount() {
    this.search("");
  }

  componentDidUpdate(
    prevProps: IMemberAsideProps,
    prevState: IMemberAsideState,
    sn: any,
  ) {
    if (this.props.show && prevProps.show !== this.props.show) {
      this.setState({role: "", showFiltering: false}, () => this.search(""));
    }
  }

  defaultValue = undefined;
  search = (query: string) => {
    const {role} = this.state;
    this.searchQuery = query;
    this.lm.reset();
    const {relation} = this.props;
    this.setState({showLoader: true}, () =>
      this.props
        .searchMembers(
          query,
          this.defaultValue,
          this.defaultValue,
          relation,
          role,
        )
        .then(({payload: {rows, count}}) =>
          this.lm.setAllItemsCount(count).setLoadedItemsCount(rows.length),
        )
        .then(() => this.setState({showLoader: false})),
    );
  };

  handleSearchByRole = (value: string) => {
    if (value !== this.state.role) {
      this.setState({role: value}, () => this.search(this.searchQuery));
    }
    this.handleToggleFiltering();
  };

  handleToggleFiltering = () => {
    const {showFiltering, role} = this.state;
    const setShow = () => this.setState((prevState) => ({
      showFiltering: !prevState.showFiltering,
    }));
    if (!showFiltering) {
      if (role !== "") {
        this.setState({role: ""}, () => this.search(this.searchQuery));
      } else {
        setShow();
      }
    } else {
      setShow();
    }
  };

  onLoadMore = () => {
    const {role} = this.state;
    const {relation} = this.props;
    if (this.state.showLoader || !this.lm.canLoadMore()) return;
    this.setState({showLoader: true}, () => {
      this.props
        .loadMoreMembers(
          this.searchQuery,
          this.lm.getNextOffset(),
          this.defaultValue,
          relation,
          role,
        )
        .then(({payload: {rows}}) =>
          this.lm.setLoadedItemsCount(rows.length),
        )
        .then(() => this.setState({showLoader: false}));
    });
  };

  render() {
    const {showLoader, role, showFiltering} = this.state;
    const {
      show,
      isMobile,
      contentWrapper,
      listItem: ListItem,
      onClose,
      listItemProps,
      itemClass,
      listItemClass,
      searchData,
      children,
      asideSearchCntProps,
      asideProps,
      onMemberClick,
      className,
      roles,
    } = this.props;
    const searchList = searchData.rows.map((data, index) => {
      const {id, name, title, coverImage} = data;
      return ListItem ? (
        <ListItem
          key={id}
          data={data}
          {...listItemProps}
          onClick={onMemberClick}
        />
      ) : (
        <MemberItem
          key={id}
          type="sidebar"
          imgSrc={getImageSrcWithSize(coverImage, "372x184")}
          title={name || title}
          onClick={onMemberClick}
          allMemberData={data}
        />
      );
    });
    return (
      <Fragment>
        <Aside
          open={show}
          className={`test-aside-members ${className}`}
          onOverlay={onClose}
          headerLeft={
            <Button type={isMobile ? "arrowLeft" : "close"} onClick={onClose}/>
          }
          contentType="no-padding"
          withCustomScroll={false}
          {...asideProps}
        >
          <AsideSearchCnt
            isMobile={isMobile}
            isLoading={showLoader}
            data={searchData ? searchList : []}
            itemClass={itemClass}
            listItemClass={listItemClass}
            onSearch={this.search}
            onLoadMore={this.onLoadMore}
            contentWrapper={contentWrapper}
            {...asideSearchCntProps}
          />
          {roles && roles.length > 0 && <AsideFiltering value={role} onSearch={this.handleSearchByRole} show={showFiltering}
                                               data={roles.filter(eachRole => !!eachRole).map(eachRole => ({
                                                 title: eachRole,
                                                 value: eachRole,
                                               }))}
                                               onToggleFiltering={this.handleToggleFiltering}/>}
        </Aside>
        {children}
      </Fragment>
    );
  }
}

MemberAside.defaultProps = {
  show: false,
  isMobile: false,
  onClose: () => null,
  onSelectItem: () => null,
  searchMembers: () =>
    new Promise((resolve) => resolve({payload: {rows: [], count: 0}})),
  loadMoreMembers: () =>
    new Promise((resolve) => resolve({payload: {rows: [], count: 0}})),
  searchData: {
    rows: [],
    count: 0,
  },
  listItem: null,
  listItemProps: null,
  listItemClass: "m-member-aside-list",
  itemClass: "m-member-aside-list__item",
  contentWrapper: null,
  roles: [],
};

