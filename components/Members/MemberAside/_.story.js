/* eslint-disable import/first */
import React from 'react';
import {storiesOf} from '@storybook/react';
import {MemberAside} from './MemberAside';

const mockMembers = {"rows":[{"id":"3e1ae8e4-6fc9-4387-8e9b-b79e897c097b","isPublished":true,"isDraftViewed":false,"name":"Lauren Hough","description":"American Olympian Lauren Hough joined the “million dollar club” with her win in the Great American $1 Million Grand Prix on Sunday, March 26, 2017 at the HITS Ocala Circuit in Ocala, Florida. Hough rode her longtime partner Ohlala, a 13-year-old Swedish Warmbloodmare","disciplines":[""],"coverImage":{"image":"member-cover-3e1ae8e4-6fc9-4387-8e9b-b79e897c097b.png","sizes":{"372x184":"member-cover-3e1ae8e4-6fc9-4387-8e9b-b79e897c097b-372x184.png","406x406":"member-cover-3e1ae8e4-6fc9-4387-8e9b-b79e897c097b-406x406.png"},"options":{"x":0,"y":0,"zoom":0,"width":1399,"canvas":{"top":66.35714285714283,"left":74.99999999999994,"width":600.0000000000001,"height":337.28571428571433,"naturalWidth":1400,"naturalHeight":787},"height":787,"rotate":0,"scaleX":1,"scaleY":1},"original":"member-cover-3e1ae8e4-6fc9-4387-8e9b-b79e897c097b-src.png"},"videos":null,"horses":[]},{"id":"14572b7a-ce77-4c2c-80e7-1b2b31f37046","isPublished":true,"isDraftViewed":false,"name":"Lauren Hough","description":"American Olympian Lauren Hough joined the “million dollar club” with her win in the Great American $1 Million Grand Prix on Sunday, March 26, 2017 at the HITS Ocala Circuit in Ocala, Florida. Hough rode her longtime partner Ohlala, a 13-year-old Swedish Warmbloodmare","disciplines":[""],"coverImage":{"image":"member-cover-14572b7a-ce77-4c2c-80e7-1b2b31f37046.png","sizes":{"372x184":"member-cover-14572b7a-ce77-4c2c-80e7-1b2b31f37046-372x184.png","406x406":"member-cover-14572b7a-ce77-4c2c-80e7-1b2b31f37046-406x406.png"},"options":{"x":0,"y":0,"zoom":0,"width":1399,"canvas":{"top":66.35714285714283,"left":74.99999999999994,"width":600.0000000000001,"height":337.28571428571433,"naturalWidth":1400,"naturalHeight":787},"height":787,"rotate":0,"scaleX":1,"scaleY":1},"original":"member-cover-14572b7a-ce77-4c2c-80e7-1b2b31f37046-src.png"},"videos":null,"horses":[]},{"id":"eecd3b38-ae84-46a1-8337-9ed911ca8a29","isPublished":true,"isDraftViewed":false,"name":"Scott Brash","description":"Scott Brash has risen to the highest level of show jumping sport by way of his humble beginnings in Peebles, Scotland. Born to Caroline and Stan Brash on December 5, 1985, Brash and his older sister Lea grew up riding their ponies longtime partner Ohlala, a 13-year-old Swedish mare","disciplines":["Jumping"],"coverImage":{},"videos":null,"horses":[]}],"count":3};

storiesOf('Shared/Members/MemberAside', module)
  .add('default hidden', () => (
    <MemberAside/>
  ))
  .add('visible', () => (
    <MemberAside show/>
  ))
  .add('with title', () => (
    <MemberAside
      show
      asideProps={{title:'Title'}}
    />
  ))
  .add('with data', () => (
    <div>
      <style dangerouslySetInnerHTML={{
        __html:
          `
          .m-search-aside-list {
            height: calc(100% - 62px);
            width: 100%;
          }
        `,
      }}
      />
      <MemberAside
        show
        asideProps={{
          title:'Title',
          noResultsTitle:'no results',
          clearBtnText:'clear',
          applyBtnText:'apply',
          cancelBtnText:'cancel',
          headerRight:<p>Right content</p>,
        }}
        asideSearchCntProps={{placeholder:'search placeholder'}}
        searchData={mockMembers}
      />
    </div>
  ));


