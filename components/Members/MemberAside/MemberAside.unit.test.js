import React from 'react';
import {shallow} from 'enzyme';
import {MemberAside} from './MemberAside';
import AsideSearchCnt from '../../Aside/components/AsideSearchCnt/AsideSearchCnt';

describe('ResultAside', () => {
  it('should renders', () => {
    const component = shallow(
      <MemberAside showAside/>
    );
    expect(component.find('Aside').length).toBe(1);
  });

  it('should render asideProps', () => {
    const component = shallow(
      <MemberAside showAside asideProps={{test:1}}/>
    );
    expect(component.find('Aside').props().test).toEqual(1);
  });

  it('should render AsideSearchCnt', () => {
    const component = shallow(
      <MemberAside showAside/>
    );
    expect(component.find(AsideSearchCnt).length).toBe(1);
  });

  it('should render asideSearchCntProps', () => {
    const component = shallow(
      <MemberAside showAside asideSearchCntProps={{test:1}}/>
    );
    expect(component.find(AsideSearchCnt).length).toBe(1);
  });

  it('should render children', () => {
    const component = shallow(
      <MemberAside showAside>
        <p className='test-p'>test</p>
      </MemberAside>
    );
    expect(component.find('.test-p').length).toBe(1);
  });
});
