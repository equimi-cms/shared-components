// @flow
import React, {PureComponent} from "react";
import "./HorseAside.scss";
import AsideHorseList from "../HorseList/AsideHorseList";
import {LoadMore} from "../../../utils/LoadMore/LoadMore";
import Button from "../../AsideButton/Button";
import {Aside} from "../../Aside/Aside";
import type {ISearchHorsePayload} from "../utils/types";
import type {IHorse} from "../../../utils/type";

interface IHorseAsideProps {
  show?: Boolean;
  className?: String;
  useFiltering?: Boolean;
  onClose?: () => void;
  isMobile?: Boolean;
  searchData?: [];
  contentWrapper?: React.Element;
  contentAfterList?: React.Element;
  listItemClass?: String;
  itemClass?: String;
  relation: {
    newsId: string,
    memberId: string,
    serviceId: string,
  };
  currencySymbol?: String;
  listItem?: React.Element;
  listItemProps?: {};
  searchHorses: ISearchHorsePayload => Promise<[]>;
  loadMoreSearchHorses: (ISearchHorsePayload, Number) => Promise<[]>;
  searchPlaceholder: String;
  asideTitle: String;
  noEntitiesTitle: String;
  translate: (v: string) => void;
  onHorseClick: (horse: IHorse) => void;
}

interface IHorseAsideState {
  showLoader: Boolean;
}

export class HorseAside extends PureComponent<IHorseAsideProps, IHorseAsideState> {
  queryInitialValue = {
    q: "",
    status: "",
    relation: {
      newsId: "",
      memberId: "",
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      showLoader: false,
      searchData: {rows: [], count: 0},
    };

    this.lm = new LoadMore();
  }

  componentDidMount() {
    this.searchQuery = {
      ...this.queryInitialValue,
      relation: this.props.relation,
    };
    this.search(this.searchQuery);
  }

  componentDidUpdate(prevProps: IHorseAsideProps, prevState: IHorseAsideState, sn: any) {
    if (this.props.show && prevProps.show !== this.props.show) {
      this.search(this.searchQuery);
    }
  }

  search = (query: ISearchHorsePayload) => {
    const {relation} = this.props;
    this.searchQuery = {...query, relation};
    this.lm.reset();
    this.setState({showLoader: true}, () => {
      return this.props
        .searchHorses(this.searchQuery)
        .then(({rows = [], count}) => {
          this.lm.setAllItemsCount(count).setLoadedItemsCount(rows.length);
          return this.setState({searchData: {rows, count}});
        })
        .then(() => this.setState({showLoader: false}));
    });
  };

  onLoadMore = () => {
    if (this.state.showLoader || !this.lm.canLoadMore()) return;

    this.setState({showLoader: true}, () => {
      this.props
        .loadMoreSearchHorses(this.searchQuery, this.lm.getNextOffset())
        .then(data => {
          let rows;
          if (data.payload) {
            rows = data.payload.rows;
          } else {
            rows = data.rows;
          }

          return this.setState(prevState => {
            const newRowsData = [...prevState.searchData.rows, ...rows];
            this.lm.setLoadedItemsCount(newRowsData.length);
            return {
              searchData: {rows: newRowsData},
            };
          });
        })
        .then(() => this.setState({showLoader: false}));
    });
  };

  render() {
    const {showLoader, searchData} = this.state;
    const {
      searchPlaceholder,
      noEntitiesTitle,
      useFiltering,
      translate,
      asideTitle,
      show,
      isMobile,
      onPlusButtonClick,
      onClose,
      listItemClass,
      currencySymbol,
      itemClass,
      listItemProps,
      usePlusButton,
      contentWrapper,
      onHorseClick,
      listItem: ListItem,
      contentAfterList,
      className,
      ...rest
    } = this.props;

    return (
      <>
        <Aside
          open={show}
          className={`test-aside-horses ${className}`}
          title={asideTitle}
          onOverlay={onClose}
          headerLeft={<Button type={isMobile ? "arrowLeft" : "close"} onClick={onClose} />}
          headerRight={
            usePlusButton && (
              <Button
                className="horse-button__add test-horse-aside-btn-add"
                onClick={onPlusButtonClick}
                type="plus"
              />
            )
          }
          contentType="no-padding"
          withCustomScroll={false}
          {...rest}
        >
          <AsideHorseList
            currencySymbol={currencySymbol}
            useFiltering={useFiltering}
            translate={translate}
            onLoadMore={this.onLoadMore}
            onSearch={this.search}
            isMobile={isMobile}
            searchPlaceholder={searchPlaceholder}
            noEntitiesTitle={noEntitiesTitle}
            searchData={searchData}
            contentWrapper={contentWrapper}
            listItemClass={listItemClass}
            itemClass={itemClass}
            listItem={ListItem}
            listItemProps={listItemProps}
            showLoader={showLoader}
            onHorseClick={onHorseClick}
            contentAfterList={contentAfterList}
          />
        </Aside>
      </>
    );
  }
}

HorseAside.defaultProps = {
  searchPlaceholder: "",
  noEntitiesTitle: "",
  asideTitle: "",
  show: false,
  isMobile: false,
  useFiltering: false,
  usePlusButton: false,
  currencySymbol: "",
  listItem: null,
  listItemProps: null,
  listItemClass: "m-horse-aside-list",
  itemClass: "m-horse-aside-list__item",
  contentWrapper: null,
  onClose: () => null,
  onPlusButtonClick: () => null,
  translate: () => null,
  onHorseClick: () => null,
};
