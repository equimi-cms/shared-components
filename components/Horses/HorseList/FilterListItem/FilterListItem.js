import React from 'react';

interface IFilterListItemProps {
  onClick: Number => void;
  value: Number;
  className: String;
  title: String;
}
const FilterListItem = ({onClick, value, className, title}: IFilterListItemProps) => {
  const handleClick = () => onClick(value);
  return (
    <li onClick={handleClick} key={value} className={className}>
      {title}
    </li>
  );
};

export default FilterListItem;
