//@flow
import React, {PureComponent} from "react";
import HorseItem from "../HorseItem/HorseItem";
import AsideSearchCnt from "../../Aside/components/AsideSearchCnt/AsideSearchCnt";
import {getImageSrcWithSize} from "../../../utils/utils";
import type {ISearchHorsePayload} from "../utils/types";
import {STATUS} from "../../../utils/data";

import type {IHorse} from "../../../utils/type";
import AsideFiltering from "../../Aside/components/AsideFiltering/AsideFiltering";

interface IAsideHorseListProps {
  onSearch: ISearchHorsePayload => void;
  onLoadMore: () => void;
  isMobile: Boolean;
  useFiltering?: Boolean;
  searchPlaceholder: String;
  noEntitiesTitle: String;
  searchData?: [];
  contentWrapper?: React.Element;
  contentAfterList?: React.Element;
  listItemClass?: String;
  itemClass?: String;
  listItem?: React.Element;
  listItemProps?: {};
  currencySymbol?: String;
  showLoader: Boolean;
  translate: (v: string) => void;
  onHorseClick: (horse: IHorse) => void;
}

export class AsideHorseList extends PureComponent<IAsideHorseListProps> {
  state = {
    showFiltering: false,
    q: "",
    status: "",
  };
  runSearch = () => {
    const {q, status} = this.state;
    this.props.onSearch({q, status});
  };
  handleSearchByQuery = async (query: String) => {
    this.setState({q: query}, this.runSearch);
  };
  handleSearchByStatus = (value: Number) => {
    if (value !== this.state.status) {
      this.setState({status: value}, this.runSearch);
    }
    this.handleToggleFiltering();
  };
  handleToggleFiltering = () => {
    const {showFiltering, status} = this.state;
    if (!showFiltering) {
      if (status !== "") {
        this.setState({status: ""}, this.runSearch);
      } else {
        this.setState(prevState => ({
          showFiltering: !prevState.showFiltering,
        }));
      }
    } else {
      this.setState(prevState => ({
        showFiltering: !prevState.showFiltering,
      }));
    }
  };

  render() {
    const {showFiltering, status} = this.state;
    const {
      onSearch,
      onLoadMore,
      isMobile,
      noEntitiesTitle,
      searchPlaceholder,
      useFiltering,
      translate,
      listItemClass,
      itemClass,
      listItemProps,
      contentWrapper,
      currencySymbol,
      listItem: ListItem,
      searchData,
      showLoader,
      onHorseClick,
      contentAfterList,
      ...rest
    } = this.props;
    const getSearchData = searchData.rows.map(data => {
      const {id, status, name, coverImage} = data;
      return ListItem ? (
        <ListItem key={id} data={data} {...listItemProps} />
      ) : (
        <HorseItem
          key={id}
          currencySymbol={currencySymbol}
          type="sidebar"
          imgSrc={getImageSrcWithSize(coverImage, "372x184")}
          status={status}
          title={name}
          translate={translate}
          onClick={onHorseClick}
          allHorseData={data}
        />
      );
    });

    const searchList = searchData ? getSearchData : [];
    return (
      <>
        <AsideSearchCnt
          isMobile={isMobile}
          isLoading={showLoader}
          placeholder={searchPlaceholder}
          data={searchList}
          listItemClass={listItemClass}
          itemClass={itemClass}
          onSearch={this.handleSearchByQuery}
          onLoadMore={onLoadMore}
          contentWrapper={contentWrapper}
          noEntitiesTitle={noEntitiesTitle}
          hideClearButton={isMobile}
          contentAfterList={contentAfterList}
          {...rest}
        />
        {useFiltering && (
          <AsideFiltering
            value={status}
            onSearch={this.handleSearchByStatus}
            show={showFiltering}
            translate={translate}
            data={STATUS.map(({title, value}) => ({title: translate(title), value}))}
            onToggleFiltering={this.handleToggleFiltering}
          />
        )}
      </>
    );
  }
}

AsideHorseList.defaultProps = {
  useFiltering: false,
  searchPlaceholder: "",
  noEntitiesTitle: "",
  isMobile: false,
  listItem: null,
  listItemProps: null,
  listItemClass: "m-horse-aside-list",
  itemClass: "m-horse-aside-list__item",
  contentWrapper: null,
  onClose: () => null,
  translate: v => v,
  onHorseClick: () => null,
};
export default AsideHorseList;
