//@flow
import React, {memo, useCallback, useMemo} from "react";
import "./HorseStatusLabel.scss";
import classNames from "classnames";
import {renderTypesAsClassNames} from "../../../utils/utils";
import {HORSE_STATUS} from "../../../utils/data";
import ForSaleLabel from "./components/ForSaleLabel/ForSaleLabel";
import BreedingLabel from "./components/BreedingLabel/BreedingLabel";
import SoldLabel from "./components/SoldLabel/SoldLabel";

interface IHorseStatusLabelProps {
  isForSale: boolean;
  isForBreeding: boolean;
  isSold: boolean;
  price: string;
  currencySymbol?: String;
  studFee: string;
  type?: String | "to-right" | "entity-header";
}

export const HorseStatusLabel = ({
  isForSale,
  translate,
  labelType,
  isForBreeding,
  isSold,
  price,
  studFee,
  currencySymbol,
}: IHorseStatusLabelProps) => {
  const checkLengthAcceptable = useCallback((text) => text.toString().length > 9, []);
  const isPriceTooLong = useMemo(() => isForSale && checkLengthAcceptable(price), [
    isForSale,
    price,
    checkLengthAcceptable,
  ]);
  const isStudFeeToLong = useMemo(() => isForBreeding && checkLengthAcceptable(studFee), [
    isForBreeding,
    studFee,
    checkLengthAcceptable,
  ]);
  const useLongMode = useMemo(
    () => (isStudFeeToLong && isForSale) || (isPriceTooLong && isForBreeding),
    [isForSale, isForBreeding, isPriceTooLong, isStudFeeToLong]
  );
  const _labelType = useMemo(() => [...labelType, useLongMode && "too-long"], [
    labelType,
    useLongMode,
  ]);
  if (!isForSale && !isForBreeding && !isSold) {
    return null;
  }
  return (
    <div
      className={classNames([
        "horse-status-label test-horse-status-label",
        {"test-status-for-breeding": isForBreeding},
        {"test-status-for-sale": isForSale},
        {"test-status-sold": isSold},
        renderTypesAsClassNames(_labelType, "horse-status-label--"),
      ])}
    >
      {isSold ? (
        <SoldLabel
          className={"test-sold"}
          translate={translate}
          labelType={_labelType}
          status={HORSE_STATUS.SOLD}
        />
      ) : (
        <>
          {isForSale && (
            <ForSaleLabel
              currencySymbol={currencySymbol}
              translate={translate}
              labelType={_labelType}
              price={price}
            />
          )}
          {isForBreeding && (
            <BreedingLabel
              currencySymbol={currencySymbol}
              translate={translate}
              labelType={_labelType}
              studFee={studFee}
            />
          )}
        </>
      )}
    </div>
  );
};
HorseStatusLabel.defaultProps = {
  currencySymbol: "£",
  labelType: [""],
  price: "",
  studFee: "",
};
export default memo(HorseStatusLabel);
