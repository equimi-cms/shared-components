import React from "react";
import { mount } from "enzyme/build";
import HorseLabel from "../HorseLabel/HorseLabel";
import SoldLabel from "./SoldLabel";

describe("SoldLabel", () => {
  it("should render HorseLabel and pass here labelType", () => {
    const mockType = ["entity-item"];
    const component = mount(<SoldLabel labelType={mockType} />);
    expect(component.find(HorseLabel).props()).toMatchObject({
      type: mockType,
    });
  });
  it("should render status title", () => {
    const mockTranslate = jest.fn();
    const component = mount(<SoldLabel translate={mockTranslate} />);
    expect(mockTranslate).toHaveBeenCalled();
    expect(component.find(".test-sold").length).toBe(1);
  });
});
