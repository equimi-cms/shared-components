import React, { memo, useMemo } from "react";
import { getStatusByValue } from "../../../../../utils/utils";
import { HORSE_STATUS } from "../../../../../utils/data";
import { HorseLabel } from "../HorseLabel/HorseLabel";

interface ISoldLabelProps {
  price: string;
  currencySymbol?: String;
  labelType?: ["to-right" | "entity-header" | "entity-item" | "entity-title"];
}

const SoldLabel = ({
  translate,
  labelType,
}: ISoldLabelProps) => {
  const statusTitle = useMemo(
    () => getStatusByValue(HORSE_STATUS.SOLD).title,
    []
  );
  return (
    <HorseLabel type={labelType}>
      <span className="test-sold">{translate(statusTitle)}</span>
    </HorseLabel>
  );
};

SoldLabel.defaultProps = {
  translate : (text) => text
}

export default memo(SoldLabel);
