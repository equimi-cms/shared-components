import React from "react";
import { mount } from "enzyme/build";
import HorseLabel from "../HorseLabel/HorseLabel";
import BreedingLabel from "./BreedingLabel";
import Label from "../Label/Label";

describe("BreedingLabel", () => {
  it("should render HorseLabel and pass here isBreeding - true , labelType", () => {
    const mockType = ["entity-item"];
    const component = mount(<BreedingLabel labelType={mockType} />);
    expect(component.find(HorseLabel).props()).toMatchObject({
      type: mockType,
      isBreeding: true,
    });
  });
  it("should render Label with needed props", () => {
    const mockStudFee = "1000";
    const mockCurrencySymbol = "$";
    const mockTranslate = jest.fn();
    const component = mount(
      <BreedingLabel
        studFee={mockStudFee}
        currencySymbol={mockCurrencySymbol}
        translate={mockTranslate}
      />
    );
    expect(mockTranslate).toHaveBeenCalled();
    expect(component.find(Label).props()).toMatchObject({
      price: mockStudFee,
      currencySymbol: mockCurrencySymbol,
    });
  });
});
