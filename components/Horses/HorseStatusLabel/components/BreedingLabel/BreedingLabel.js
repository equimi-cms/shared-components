import React, { memo, useMemo } from "react";
import { getStatusByValue } from "../../../../../utils/utils";
import { HORSE_STATUS } from "../../../../../utils/data";
import { HorseLabel } from "../HorseLabel/HorseLabel";
import Label from "../Label/Label";

interface IBreedingLabelProps {
  studFee: string;
  currencySymbol?: String;
  labelType?: ["to-right" | "entity-header" | "entity-item" | "entity-title"];
}

const BreedingLabel = ({
  translate,
  labelType,
  studFee,
  currencySymbol,
}: IBreedingLabelProps) => {
  const statusTitle = useMemo(
    () => getStatusByValue(HORSE_STATUS.FOR_BREEDING).title,
    []
  );
  return (
    <HorseLabel type={labelType} isBreeding>
      <Label
        priceTitle={translate("HORSES.Stud_fee")}
        priceClass={"test-stud-fee"}
        price={studFee}
        statusTitle={translate(statusTitle)}
        statusClass={"test-for-breeding"}
        currencySymbol={currencySymbol}
      />
    </HorseLabel>
  );
};

BreedingLabel.defaultProps = {
  translate : (text) => text
}

export default memo(BreedingLabel);
