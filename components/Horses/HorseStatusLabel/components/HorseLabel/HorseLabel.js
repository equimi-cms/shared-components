//@flow
import React from "react";
import "./HorseLabel.scss";
import classNames from "classnames";
import { renderTypesAsClassNames } from "../../../../../utils/utils";

interface IHorseLabelProps {
  className?: String;
  type?: Array | "to-right" | "entity-header" | "entity-item" | "entity-title";
  currencySymbol?: String;
}

export const HorseLabel = ({
  isBreeding,
  className,
  type,
  children,
  ...props
}: IHorseLabelProps) => {
  return (
    <span
      {...props}
      className={classNames(
        "horse-label js-horse-label",
        isBreeding && "horse-label--mod-1 js-horse-label--mod-1",
        className,
        renderTypesAsClassNames(type, "horse-label--mod-")
      )}
    >
      {children}
    </span>
  );
};

export default React.memo(HorseLabel);

HorseLabel.defaultProps = {
  isBreeding: false,
  children: null,
  type: [],
};
