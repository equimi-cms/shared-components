import React from "react";
import { mount } from "enzyme/build";
import HorseLabel from "./HorseLabel";

describe("HorseLabel", () => {
  it("should render children component", () => {
    const MockChildren = () => (
      <div className="test-children">test children</div>
    );
    const component = mount(
      <HorseLabel isBreeding>
        <MockChildren />
      </HorseLabel>
    );
    expect(component.find(".js-horse-label").length).toBe(1);
    expect(component.find(".test-children").length).toBe(1);
  });
  it("should render state for breeding", () => {
    const component = mount(<HorseLabel isBreeding />);
    expect(component.find(".js-horse-label").length).toBe(1);
    expect(component.find(".js-horse-label--mod-1").length).toBe(1);
  });
});
