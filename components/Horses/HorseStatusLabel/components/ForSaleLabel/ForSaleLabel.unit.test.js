import React from "react";
import { mount } from "enzyme/build";
import HorseLabel from "../HorseLabel/HorseLabel";
import ForSaleLabel from "./ForSaleLabel";
import Label from "../Label/Label";

describe("ForSaleLabel", () => {
  it("should render HorseLabel and pass here labelType", () => {
    const mockType = ["entity-item"];
    const component = mount(<ForSaleLabel labelType={mockType} />);
    expect(component.find(HorseLabel).props()).toMatchObject({
      type: mockType,
    });
  });
  it("should render Label with needed props", () => {
    const mockPrice = "1000";
    const mockCurrencySymbol = "$";
    const mockTranslate = jest.fn();
    const component = mount(
      <ForSaleLabel
        price={mockPrice}
        currencySymbol={mockCurrencySymbol}
        translate={mockTranslate}
      />
    );
    expect(mockTranslate).toHaveBeenCalled();
    expect(component.find(Label).props()).toMatchObject({
      price: mockPrice,
      currencySymbol: mockCurrencySymbol,
    });
  });
});
