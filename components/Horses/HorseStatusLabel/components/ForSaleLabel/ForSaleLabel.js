import React, { memo, useMemo } from "react";
import { getStatusByValue } from "../../../../../utils/utils";
import { HORSE_STATUS } from "../../../../../utils/data";
import { HorseLabel } from "../HorseLabel/HorseLabel";
import Label from "../Label/Label";

interface IForSaleLabelProps {
  price: string;
  translate: (string) => string;
  currencySymbol?: string;
  labelType?: ["to-right" | "entity-header" | "entity-item" | "entity-title"];
}

const ForSaleLabel = ({
  translate,
  labelType,
  price,
  currencySymbol,
}: IForSaleLabelProps) => {
  const statusTitle = useMemo(
    () => getStatusByValue(HORSE_STATUS.FOR_SALE).title,
    []
  );

  return (
    <HorseLabel type={labelType}>
      <Label
        priceTitle={translate("HORSES.Sale_price")}
        priceClass={"test-price"}
        price={price}
        statusTitle={translate(statusTitle)}
        statusClass={"test-for-sale"}
        currencySymbol={currencySymbol}
      />
    </HorseLabel>
  );
};

ForSaleLabel.defaultProps = {
  translate : (text) => text
}

export default memo(ForSaleLabel);
