import React from "react";
import { shallow } from "enzyme/build";
import Label from "./Label";

describe("Label", () => {
  it("should render status when no price", () => {
    const mockStatusClass = "test-status";
    const component = shallow(<Label statusClass={mockStatusClass} />);

    expect(component.find(`.${mockStatusClass}`).length).toBe(1);
  });
  it("should render price when it is", () => {
    const mockPriceClass = "test-price";
    const mockCurrencySymbol = "$";
    const component = shallow(<Label priceClass={mockPriceClass} />);
    const getPrice = (comp) => comp.find(`.${mockPriceClass}`);
    expect(getPrice(component).length).toBeFalsy();
    component.setProps({ price: "3000", currencySymbol: mockCurrencySymbol });
    component.update();
    expect(getPrice(component).length).toBeTruthy();
    expect(getPrice(component).text()).toContain(mockCurrencySymbol);
  });
});
