import React, { memo } from "react";
import {formatPriceWithSpaces} from '../../../../../utils/price';

interface ILabelProps {
  price?: string;
  currencySymbol?: string;
  statusTitle?: string;
  statusClass?: string;
  priceClass?: string;
  priceTitle?: string;
}

const Label = ({
  price,
  statusTitle,
  statusClass,
  priceClass,
  priceTitle,
  currencySymbol,
}: ILabelProps) => {
  return (
    <>
      {!price ? (
        <span className={statusClass}>{statusTitle}</span>
      ) : (
        <>
          <span>{priceTitle}</span>&nbsp;
          <b className={priceClass}>
            {formatPriceWithSpaces(price)}
            {currencySymbol}
          </b>
        </>
      )}
    </>
  );
};
Label.defaultProps = {
  statusTitle: "",
  statusClass: "",
  priceClass: "",
  priceTitle: "",
};
export default memo(Label);
