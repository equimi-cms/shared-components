import React from "react";
import { shallow } from "enzyme/build";
import HorseStatusLabel from "./HorseStatusLabel";
import BreedingLabel from "./components/BreedingLabel/BreedingLabel";
import ForSaleLabel from "./components/ForSaleLabel/ForSaleLabel";
import SoldLabel from "./components/SoldLabel/SoldLabel";

describe("HorseStatusLabel", () => {
  it("should not render when no status", () => {
    const component = shallow(
      <HorseStatusLabel
        isForBreeding={false}
        isForSale={false}
        isSold={false}
      />
    );
    expect(component.find(".test-horse-status-label").length).toBeFalsy();
  });

  it("should render Breeding with stud fee", () => {
    const mockStudFee = "300";
    const component = shallow(
      <HorseStatusLabel isForBreeding studFee={mockStudFee} />
    );
    expect(component.find(BreedingLabel).length).toBeTruthy();
    expect(component.find(BreedingLabel).props().studFee).toBe(mockStudFee);
  });

  it("should render ForSaleLabel with price", () => {
    const mockPrice = "300";
    const component = shallow(<HorseStatusLabel isForSale price={mockPrice} />);
    expect(component.find(ForSaleLabel).length).toBeTruthy();
    expect(component.find(ForSaleLabel).props().price).toBe(mockPrice);
  });

  it("should render SoldLabel and not render other while it is true", () => {
    const mockPrice = "300";
    const component = shallow(
      <HorseStatusLabel isForSale isForBreeding isSold price={mockPrice} />
    );
    expect(component.find(SoldLabel).length).toBeTruthy();
    expect(component.find(ForSaleLabel).length).toBeFalsy();
    expect(component.find(BreedingLabel).length).toBeFalsy();
  });

  it("should render add `too-long` modifier to labelType", () => {
    const mockType = ["entity-title"];
    const component = shallow(
      <HorseStatusLabel
        isForSale
        labelType={mockType}
        isForBreeding
        studFee={"10000000000000000"}
      />
    );
    expect(component.find(BreedingLabel).props().labelType).toContain(
      "too-long"
    );
  });
});
