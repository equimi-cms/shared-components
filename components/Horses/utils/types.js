export interface IStatus {
  title: String;
  id: Number;
}

export interface ISearchHorsePayload {
  q: String;
  status: IStatus;
}
