//@flow
import React from "react";
import "./HorseItem.scss";
import classNames from "classnames";
import Image from "../../../Image/Image";
import horsePlaceholderImage from "./imgs/horse.svg";
import SvgInlineImage from "../../../Image/SvgInlineImage/SvgInlineImage";
import type { IHorse } from "../../../utils/type";
import { HorseStatusLabel } from "../HorseStatusLabel/HorseStatusLabel";

interface IHorseItemProps {
  className?: String;
  type?: "dashboard" | "sidebar";
  title?: String;
  imgSrc?: String;
  status?: any;
  children?: Element;
  translate: (v: string) => void;
  onClick: (horse: IHorse) => void;
  allHorseData: IHorse;
}

export const HorseItem = ({
  className,
  translate,
  status,
  type,
  title,
  imgSrc,
  onClick,
  allHorseData,
  currencySymbol,
  ...props
}: IHorseItemProps) => {
  const { isForBreeding, isForSale, isSold, studFee, price } = allHorseData;
  const handleClick = () => {
    onClick(allHorseData);
  };
  return (
    <div
      {...props}
      className={classNames(
        "m-horse-item test-horse-item",
        className,
        type && "m-horse-item--" + type
      )}
      onClick={handleClick}
    >
      <HorseStatusLabel
        currencySymbol={currencySymbol}
        translate={translate}
        labelType={["entity-item"]}
        isForSale={isForSale}
        isForBreeding={isForBreeding}
        isSold={isSold}
        price={price}
        studFee={studFee}
      />

      <div className="m-horse-item__img-cnt">
        {imgSrc ? (
          <Image className="m-horse-item__img" src={imgSrc} />
        ) : (
          <SvgInlineImage
            className="m-horse-item__img-placeholder"
            src={horsePlaceholderImage}
          />
        )}
      </div>
      <h6 className="m-horse-item__title">{title}</h6>
    </div>
  );
};

export default React.memo(HorseItem);

HorseItem.defaultProps = {
  className: "",
  type: "dashboard",
  title: "",
  imgSrc: "",
  status: "",
  children: "",
  allHorseData: {},
  translate: (v) => v,
  onClick: () => null,
};
