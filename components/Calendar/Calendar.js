//@flow
import React, {Component} from 'react';
import './Calendar.scss';
import classNames from 'classnames';
import CalendarSelectedPeriod from './CalendarSelectedPeriod/CalendarSelectedPeriod';
import {Portal} from 'react-portal';
import CardButton from '../Card/CardButton/CardButton';
import ReactCalendar from 'react-calendar';
import SvgInlineImage from '../../Image/SvgInlineImage/SvgInlineImage';
import ArrowRight from './imgs/arrow-right.svg';
import ArrowLeft from './imgs/arrow-left.svg';
import moment from 'moment';

const arrowRight = <SvgInlineImage src={ArrowRight} />;
const arrowLeft = <SvgInlineImage src={ArrowLeft} />;

interface ICalendarProps {
  isMobile: Boolean;
  isTablet: Boolean;
  open: Boolean;
  onClose: () => void;
  searchDataTitle: any;
  emptySearchDataTitle: any;
  clearBtnText?: any;
  applyBtnText?: any;
  cancelBtnText?: any;

  /**
   * Use for sync data between 2 or more calendar components
   */
  selectedData?: {
    dateFrom: String,
    dateTo: String,
    year: Number,
    month: Number,
  };
  className?: String;

  calendarBtnClass?: String;

  setIsCalendarDataVisible: Boolean => void;
  onPickMonthAndYear: (date: Date) => void;
  onPickYear: (date: Date) => void;
  onClear: () => void;
  timeLine: [];
}

export class Calendar extends Component<ICalendarProps> {
  state = {
    text: '',
    selectedMonthAndYear: null,
    selectedYear: null,
    temporarySelectedMonthAndYear: null,
    temporarySelectedYear: null,
    isCalendarOpen: false,
  };

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutsideCalendar, true);
  }
  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutsideCalendar, true);
  }
  componentDidUpdate(prevProps: ICalendarProps, prevState, sn) {
    const {selectedMonthAndYear, selectedYear} = this.state;
    const {setIsCalendarDataVisible, selectedData} = this.props;
    setIsCalendarDataVisible(!selectedMonthAndYear && !selectedYear);

    /**
     * Sync selectedData from props with selectedData from local state
     * Update local state when selectedData from props is updated
     */
    const setSelectedData = () => {
      const isSelectedMonthAndYear =
        selectedData.hasOwnProperty('month') && selectedData.hasOwnProperty('year');
      const isSelectedYear =
        !selectedData.hasOwnProperty('month') && selectedData.hasOwnProperty('year');

      //Reset state
      if (!isSelectedMonthAndYear && !isSelectedYear && (selectedYear || selectedMonthAndYear)) {
        this.setState({selectedYear: null, selectedMonthAndYear: null});
      }

      //Update Month and Year
      if (isSelectedMonthAndYear) {
        let updateMonthAndYear = false;
        if (selectedMonthAndYear) {
          const prevSelectedYear = selectedMonthAndYear.getFullYear();
          const prevSelectedMonth = selectedMonthAndYear.getMonth();
          if (selectedData.year !== prevSelectedYear || selectedData.month !== prevSelectedMonth) {
            updateMonthAndYear = true;
          }
        } else {
          updateMonthAndYear = true;
        }
        updateMonthAndYear &&
          this.setState({
            selectedYear: null,
            selectedMonthAndYear: new Date(selectedData.year, selectedData.month),
          });
      }

      //Update Year
      if (isSelectedYear) {
        let updateYear = false;
        if (selectedYear) {
          if (selectedData.year !== selectedYear) {
            updateYear = true;
          }
        } else {
          updateYear = true;
        }

        updateYear &&
          this.setState({
            selectedMonthAndYear: null,
            selectedYear: selectedData.year,
          });
      }
    };

    setSelectedData();
  }

  handleClearSelected = () => {
    const isTouched = this.state.selectedMonthAndYear || this.state.selectedYear;

    isTouched &&
      this.setState(
        {
          selectedYear: null,
          selectedMonthAndYear: null,
          temporarySelectedMonthAndYear: null,
          temporarySelectedYear: null,
        },
        this.props.onClear
      );
  };

  handlePickMonthAndYear = (date, cb) => {
    this.props.onPickMonthAndYear &&
      this.props.onPickMonthAndYear({
        year: new Date(date).getFullYear(),
        month: new Date(date).getMonth(),
      });
    this.setState({selectedYear: null, selectedMonthAndYear: new Date(date)}, () => cb && cb());
  };

  handlePickYear = (date, cb) => {
    this.props.onPickYear && this.props.onPickYear(new Date(date).getFullYear());
    this.setState(
      {selectedMonthAndYear: null, selectedYear: new Date(date).getFullYear()},
      () => cb && cb()
    );
  };
  getCountOfEntitiesInMonthOfYear = ({year, month}) => {
    month++;
    month = month < 10 ? `0${month}` : month;
    const givenDate = `${year}-${month}`;
    const resultedDate = this.props.timeLine.find(el => el.hasOwnProperty(givenDate));
    const count = !!resultedDate && Object.values(resultedDate)[0];
    return count > 0 && count;
  };
  getCountOfEntitiesInYear = year => {
    const dates = this.props.timeLine.filter(el => {
      const elYear = !!Object.keys(el)[0] && Object.keys(el)[0].split('-')[0];
      return year.toString() === elYear.toString();
    });
    let count = 0;
    dates.forEach(el => {
      if (Object.values(el)[0]) count += Object.values(el)[0];
    });
    return count > 0 && count;
  };

  handleClickOutsideCalendar = event => {
    const {open, onClose, calendarBtnClass} = this.props;
    const isClickOutsideCalendar = !event.target.closest('.js-calendar-section');
    const isClickCalendarButton = event.target.closest(calendarBtnClass);
    if (open && isClickOutsideCalendar && !isClickCalendarButton) onClose();
  };
  calculateTitleContent = props => {
    let count;

    if (props.view === 'year')
      count = this.getCountOfEntitiesInMonthOfYear({
        year: props.date.getFullYear(),
        month: props.date.getMonth(),
      });
    else if (props.view === 'decade')
      count = this.getCountOfEntitiesInYear(props.date.getFullYear());
    return (
      <p className="entities-list-calendar-news-count">
        <span className="entities-list-calendar-news-count-digit">{count} </span>
        {!!count && this.props.countOfEntitiesTitle}
      </p>
    );
  };
  handleChange = date => {
    this.props.isMobile || this.props.isTablet
      ? this.setState({temporarySelectedMonthAndYear: date, temporarySelectedYear: null})
      : this.handlePickMonthAndYear(date);
  };
  handleClickYear = date => {
    this.props.isMobile || this.props.isTablet
      ? this.setState({temporarySelectedMonthAndYear: null, temporarySelectedYear: date})
      : this.handlePickYear(date);
  };

  handleCancel = () => {
    this.setState(
      {temporarySelectedYear: null, temporarySelectedMonthAndYear: null},
      this.props.onClose
    );
  };

  handleApply = () => {
    const {temporarySelectedMonthAndYear, temporarySelectedYear} = this.state;
    const {onClose} = this.props;
    if (temporarySelectedMonthAndYear) {
      this.handlePickMonthAndYear(temporarySelectedMonthAndYear, () =>
        this.setState({temporarySelectedMonthAndYear: null}, onClose)
      );
    } else if (temporarySelectedYear) {
      this.handlePickYear(temporarySelectedYear, () =>
        this.setState({temporarySelectedYear: null}, onClose)
      );
    } else {
      onClose();
    }
  };

  render() {
    const {
      isMobile,
      isTablet,
      searchDataTitle,
      emptySearchDataTitle,
      clearBtnText,
      applyBtnText,
      cancelBtnText,
      open,
      className,
    } = this.props;
    const {selectedMonthAndYear, selectedYear, temporarySelectedMonthAndYear} = this.state;

    const isSelectedPeriod = selectedMonthAndYear || selectedYear;

    const selectedPeriodTitle = selectedMonthAndYear ? (
      moment(selectedMonthAndYear).format('MMMM, YYYY')
    ) : (
      <>
        {searchDataTitle} {selectedYear}
      </>
    );

    return (
      <>
        {open && !isSelectedPeriod && (
          <CalendarSelectedPeriod
            isClearBtnActive={false}
            clearBtnText={clearBtnText}
            children={emptySearchDataTitle}
          />
        )}
        {isSelectedPeriod && (
          <CalendarSelectedPeriod
            isClearBtnActive
            onClear={this.handleClearSelected}
            clearBtnText={clearBtnText}
            children={selectedPeriodTitle}
          />
        )}

        <Portal>
          <div className="basic-calendar js-calendar-section">
            <ReactCalendar
              value={
                temporarySelectedMonthAndYear ? temporarySelectedMonthAndYear : selectedMonthAndYear
              }
              prevLabel={arrowLeft}
              nextLabel={arrowRight}
              minDetail="decade"
              maxDetail="year"
              className={classNames([
                'entities-list-calendar',
                open && 'entities-list-calendar-show',
                className,
              ])}
              activeStartDate={
                selectedMonthAndYear || (selectedYear && new Date(selectedYear, 0, 1))
              }
              locale={'en'}
              next2Label={null}
              prev2Label={null}
              onClickYear={this.handleClickYear}
              tileContent={this.calculateTitleContent}
              tileClassName="entities-list-calendar-title test-calendar-title"
              onChange={this.handleChange}
            />
            {open && (isMobile || isTablet) && (
              <>
                <CardButton
                  className="entities-list-calendar__apply-button test-entities-list-calendar__apply-button"
                  onClick={this.handleApply}
                >
                  {applyBtnText}
                </CardButton>
                <CardButton
                  className="entities-list-calendar__cancel-button test-entities-list-calendar__cancel-button"
                  onClick={this.handleCancel}
                >
                  {cancelBtnText}
                </CardButton>
              </>
            )}
          </div>
        </Portal>
      </>
    );
  }
}

export default React.memo(Calendar);

Calendar.defaultProps = {
  timeLine: [],
  selectedData: {},
  onClear: () => null,
  onClose: () => null,
  setIsCalendarDataVisible: (data: Boolean) => null,
  onPickMonthAndYear: (date: Date) => null,
  onPickYear: (date: Date) => null,
};
