import React from 'react';
import {shallow} from 'enzyme/build';
import {Calendar} from './Calendar';

describe('Calendar', () => {
  it('should find count of entities in month and year using time line', () => {
    const mockNewsTimeLine = [
      {'2018-01': 3},
      {'2018-04': 0},
      {'2018-07': 0},
      {'2018-10': 0},
      {'2018-02': 0},
      {'2018-05': 0},
      {'2018-08': 3},
      {'2018-11': 0},
      {'2018-03': 0},
      {'2018-06': 0},
      {'2018-09': 0},
      {'2018-12': 0},
      {'2017-01': 3},
      {'2017-04': 1},
      {'2017-07': 2},
      {'2017-10': 3},
      {'2017-02': 8},
      {'2017-05': 3},
      {'2017-08': 3},
      {'2017-11': 8},
      {'2017-03': 0},
      {'2017-06': 0},
      {'2017-09': 1},
      {'2017-12': 5},
    ];
    const comp = shallow(<Calendar timeLine={mockNewsTimeLine} />);
    // month 11-1 cause first month equal to 0
    expect(comp.instance().getCountOfEntitiesInMonthOfYear({year: 2017, month: 11 - 1})).toBe(8);
    expect(comp.instance().getCountOfEntitiesInYear(2018)).toBe(6);
  });
});
