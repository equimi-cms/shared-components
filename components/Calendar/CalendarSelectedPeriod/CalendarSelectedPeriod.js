//@flow
import React from 'react';
import './CalendarSelectedPeriod.scss';
import classNames from 'classnames';

interface ICalendarSelectedPeriodProps {
  onClear?: () => void;
  isClearBtnActive?: Boolean;
  clearBtnText?: String;
  children?: React$Node;
}

export const CalendarSelectedPeriod = ({
  onClear,
  isClearBtnActive,
  clearBtnText,
  children,
}: ICalendarSelectedPeriodProps) => {
  return (
    <div className="entities-search-data test-entities-search-data">
      <span>{children}</span>
      <div
        onClick={onClear}
        className={classNames([
          isClearBtnActive
            ? 'entities-search-data-clear-button_active test-esd-clear-button_active'
            : 'entities-search-data-clear-button test-esd-clear-button',
        ])}
      >
        {clearBtnText}
      </div>
    </div>
  );
};

export default React.memo(CalendarSelectedPeriod);

CalendarSelectedPeriod.defaultProps = {
  onClear: () => null,
  clearBtnText: 'Clear',
};
