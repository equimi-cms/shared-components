import React from 'react';
import './CalendarButton.scss';
import SvgInlineImage from '../../../Image/SvgInlineImage/SvgInlineImage';
import calendarIcon from './imgs/calendar.svg';
import classNames from 'classnames';
import {renderTypesAsClassNames} from '../../../utils/utils';

interface IToggleCalendarButtonProps {
  isActive?: Boolean;
  onClick?: (e: MouseEvent) => void;
  className?: String;
  type?: '' | 'mod-1';
}

export const CalendarButton = ({isActive, onClick, className,type}: IToggleCalendarButtonProps) => (
  <div
    onClick={onClick}
    className={classNames(
      'calendar-btn test-calendar-button',
      isActive && 'calendar-btn--active test-calendar-button_active',
      className,
      renderTypesAsClassNames(type,'calendar-btn--')
    )}
  >
    <SvgInlineImage src={calendarIcon} />
  </div>
);

CalendarButton.defaultProps = {
  isActive: false,
  onClick: () => null,
};

export default React.memo(CalendarButton);
