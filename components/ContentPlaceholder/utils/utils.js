/**
 *
 * @param type
 * @returns {any}
 */
export const getImgByType = (type: string) => {
  if (!type) return null;
  //use try catch to prevent unit tests fail
  try {
    return require(`../imgs/${type}.svg`);
  } catch (e) {
    //keep silent
  }
};
