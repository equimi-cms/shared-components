import React from 'react';
import {ContentPlaceholder} from './ContentPlaceholder';
import {shallow} from 'enzyme';
import * as utils from './utils/utils';

describe('ContentPlaceholder', () => {
  it('should renders', () => {
    const Main = shallow(<ContentPlaceholder />);
    expect(Main.find('.test-placeholder').length).toBe(1);
  });

  it('should renders without image when no type passed', () => {
    const Main = shallow(<ContentPlaceholder />);
    expect(Main.find('.test-image').length).toBe(0);
  });

  it('should renders with type', () => {
    utils.getImgByType = () => 'news';
    const Main = shallow(<ContentPlaceholder type={'news'} />);
    expect(Main.find('.test-image').length).toBe(1);
    expect(Main.find('.m-content-placeholder--type-news').length).toBe(1);
  });
});
