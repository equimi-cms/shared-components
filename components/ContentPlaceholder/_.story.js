/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {ContentPlaceholder} from './ContentPlaceholder';
import {Dashboard} from '../../../../../.storybook/config';

storiesOf(Dashboard + '/ContentPlaceholder', module)
  .add('default', () => <ContentPlaceholder />)
  .add('default type news', () => <ContentPlaceholder type={'news'} />);
