import React from 'react';
import './ContentPlaceholder.scss';
import classNames from 'classnames';
import {renderTypesAsClassNames} from 'utils/utils';

import ImagePlaceholder from './imgs/placeholder.svg';
import ImagePlaceholderMob from './imgs/placeholder-mob.svg';
import {useMobileStatus} from '../../hooks/MobileStatus';
import {Image} from '../../Image/Image';
import type {Entity} from '../../utils/types';
import {getImgByType} from './utils/utils';

interface IContentPlaceholderProps {
  className?: String;
  type?: Entity;
}

export const ContentPlaceholder = ({className, type, ...props}: IContentPlaceholderProps) => {
  const isMobile = useMobileStatus();
  const image = getImgByType(type);
  return (
    <div
      {...props}
      className={classNames(
        'm-content-placeholder test-placeholder',
        renderTypesAsClassNames(type, 'm-content-placeholder--type-'),
        className
      )}
    >
      {image && <Image className="m-content-placeholder__img test-image" src={image} />}
      <Image
        className="m-content-placeholder__img-placehodler test-placeholder-image"
        src={isMobile ? ImagePlaceholderMob : ImagePlaceholder}
      />
    </div>
  );
};

export default React.memo(ContentPlaceholder);

ContentPlaceholder.defaultProps = {
  className: '',
};
