//@flow
import React, {PureComponent} from 'react';
import './Aside.scss';
import classNames from 'classnames';
import {PortalWithState} from 'react-portal';
import {Button} from '../AsideButton/Button';
import {hideMainScroll, isUnitTestMode, renderTypesAsClassNames} from '../../utils/utils';
import Scroll from '../Scroll/Scroll';
import DelayedContent from '../DelayedContent/DelayedContent';

export const ASIDE_CLASS_ANIM_IN = 'm-aside--anim-in';
export const ASIDE_CLASS_ANIM_OUT = 'm-aside--anim-out';


interface IAsideProps {
  open: Boolean;
  className?: String;
  usePortal?: Boolean;
  translate?: () => void;
  /**
   * Allow to delay content render process
   * Useful when need increase aside animation performance
   * delayedContent 200ms will render content after aside animation finished
   */
  delayedContent?: Number;
  /**
   * When update the property it will re-render the aside content
   */
  showOverlay?: Boolean;
  children?: React.ReactNode;
  asideLeftContent?: React.ReactNode;
  title?: String | React.ReactNode;
  isBackBtn?: Boolean;
  backBtnType?: ['close' | 'text' | 'arrowLeft'];
  onBack?: () => void;
  isSubmitBtn?: Boolean;
  onSubmit?: () => void;
  onOverlay?: () => void;
  onClosed?: () => void;
  /**
   * transparent-overlay - when you need action on overlay, but overlay should be hidden
   */
  asideType?: ['' | 'no-shadow' | 'transparent-overlay' | 'title-mod-1'];
  contentType?: ['' | 'no-h-padding' | 'no-padding' | 'content-h-auto' | 'content-mod-1'];
  headerType?: ['' | 'entity'];
  headerLeft?: any;
  headerRight?: any;
  /**
   * Use to prevent showing main scroll when close aside
   */
  hideScrollOnOpen?: Boolean;
  showScrollOnClose?: Boolean;
  /**
   * Use to render aside
   * Hidden for the user
   */
  preload?: Boolean;
  removeFromDomOnClose?: Boolean;
  withCustomScroll?: Boolean;
}

export class Aside extends PureComponent<IAsideProps> {
  constructor(props: IAsideProps) {
    super(props);
    this.portalState = {
      openPortal: () => {},
      closePortal: () => {},
    };
    this.timer = setTimeout(() => {}, 0);
    this.isAsideChange = false;
    this.state = {
      overlay: false,
    };
  }

  componentDidMount() {
    const {open, preload} = this.props;
    if (open) {
      this.open();
    } else {
      if (preload) this.portalState.openPortal();
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
    this.isAsideChange = this.props.open !== nextProps.open;
    if (!this.isAsideChange) return;
    nextProps.open ? this.open() : this.close();
  }

  componentWillUnmount() {
    this.props.showScrollOnClose && hideMainScroll(false);
    this.portalState.closePortal();
  }

  /**
   * Overlay hidden by default
   * Overlay will be rendered when
   * user open the aside by updating the prop 'open'
   */
  renderOverlay() {
    this.setState({overlay: true});
  }

  close = () => {
    const {removeFromDomOnClose, showScrollOnClose, onClosed} = this.props;

    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.setState({overlay: false}, () => {
        showScrollOnClose && hideMainScroll(false);
        removeFromDomOnClose && this.portalState.closePortal();
        onClosed();
      });
    }, 200); //wait for animation before close
  };

  open = () => {
    clearTimeout(this.timer);
    hideMainScroll(this.props.hideScrollOnOpen);
    this.portalState.openPortal();
    this.renderOverlay();
  };

  getOverlayClass = () => {
    const {asideType} = this.props;
    if (Array.isArray(asideType)) {
      return asideType.includes('transparent-overlay') && 'overlay--transparent';
    } else {
      return asideType === 'transparent-overlay' && 'overlay--transparent';
    }
  };

  getContent = portalState => {
    const {
      isBackBtn,
      backBtnType,
      isSubmitBtn,
      onBack,
      onSubmit,
      open,
      title,
      children,
      contentType,
      asideType,
      headerType,
      headerLeft,
      headerRight,
      withCustomScroll,
      usePortal,
      delayedContent,
      className,
      asideLeftContent,
      translate: Translate,
      removeFromDomOnClose,
            preload,
    } = this.props;
    const backBtn = isBackBtn && (
      <Button className="test-back-btn" type={backBtnType} onClick={onBack} />
    );

    const submitBtn = isSubmitBtn && (
      <Button
        type="text"
        className={'test-done'}
        translate={Translate}
        title="Done"
        onClick={onSubmit}
      />
    );

    if (portalState) {
      this.portalState = portalState;
    }
    const _children = (
      <div
        className={classNames(
          'm-aside-content',
          renderTypesAsClassNames(contentType, 'm-aside-content--type-')
        )}
      >
        {children}
      </div>
    );
    // Scroll component is not recognised as valid under jest testing
    const childrenWrapper = withCustomScroll ? <Scroll>{_children}</Scroll> : _children;
    const contentWrapper = delayedContent ? (
      <DelayedContent timeBeforeShow={delayedContent}>{childrenWrapper}</DelayedContent>
    ) : (
      childrenWrapper
    );
    // You have to manually handle Animation of the popup when don't use Portal
    const animateOpenClose = usePortal && (open ? ASIDE_CLASS_ANIM_IN : ASIDE_CLASS_ANIM_OUT);
    const isTypeEntity = headerType ? headerType.includes('entity') : false;
    const content = (
      <>
        {asideLeftContent && (
          <div
            className={classNames(
              'm-aside-left test-aside-left',
              open ? 'm-aside-left--open' : 'm-aside-left--close'
            )}
          >
            {asideLeftContent}
          </div>
        )}

        <aside
          className={classNames(
            'm-aside m-aside--over test-aside',
            !removeFromDomOnClose && 'm-aside--type-in-dom',
            preload && 'm-aside--hidden',
            renderTypesAsClassNames(asideType, 'm-aside--type-'),
            animateOpenClose,
            className
          )}
        >
          <div className="m-aside-cnt">
            <header
              className={classNames(
                'm-aside-header',
                renderTypesAsClassNames(headerType, 'm-aside-header--type-')
              )}
            >
              <div className="m-aside-header__left">{headerLeft ? headerLeft : backBtn}</div>
              <div className="m-aside-header__middle">
                <span className="m-aside-header__title">
                  {React.isValidElement(title) ? (
                    title
                  ) : Translate ? (
                    <Translate value={title} />
                  ) : (
                    title
                  )}
                </span>
              </div>
              <div className="m-aside-header__right">{headerRight || submitBtn}</div>
            </header>
            <div className={classNames('m-aside-cnt__scroll',isTypeEntity && 'm-aside-cnt__scroll--type-entity')}>{contentWrapper}</div>
          </div>
        </aside>
      </>
    );
    return <>{portalState ? portalState.portal(content) : content}</>;
  };

  render() {
    const {onOverlay, open, showOverlay, usePortal} = this.props;

    const overlayContent = (
      <div
        className={classNames(
          'overlay overlay--aside',
          this.getOverlayClass(),
          open
            ? 'overlay--anim-in test-overlay--anim-in'
            : 'overlay--anim-out test-overlay--anim-out',
          showOverlay
            ? 'overlay--anim-in test-overlay--anim-in'
            : 'overlay--anim-out test-overlay--anim-out'
        )}
        onClick={onOverlay}
      >
        {}
      </div>
    );

    const overlay = this.state.overlay && (
      <>
        {usePortal ? (
          <PortalWithState defaultOpen={true}>
            {portalState => portalState.portal(overlayContent)}
          </PortalWithState>
        ) : (
          overlayContent
        )}
      </>
    );

    const renderPortal = !isUnitTestMode();

    const portalClosed = renderPortal ? (
      <PortalWithState defaultOpen={false}>{this.getContent}</PortalWithState>
    ) : (
      this.getContent()
    );

    return (
      <>
        {overlay}{' '}
        {usePortal ? portalClosed : this.getContent()}
      </>
    );
  }
}

Aside.defaultProps = {
  open: false,
  usePortal: true,
  delayedContent: 0,
  backBtnType: 'arrowLeft',
  hideScrollOnOpen: true,
  showScrollOnClose: true,
  contentType: '',
  showOverlay: true,
  title: '',
  isBackBtn: false,
  isSubmitBtn: false,
  preload: false,
  withCustomScroll: true,
  removeFromDomOnClose: true,
  headerType: [],

  onBack: /* istanbul ignore next */ () => {},
  onSubmit: /* istanbul ignore next */ () => {},
  onOverlay: /* istanbul ignore next */ () => {},
  onClosed: /* istanbul ignore next */ () => {},
};
