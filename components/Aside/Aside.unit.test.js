import React from 'react';
import {Aside} from './Aside';
import {mount, shallow} from 'enzyme';
import * as utils from '../../utils/utils';

global.scroll = () => {};

utils.hideMainScroll = jest.fn();

describe('Aside', () => {
  it('should renders', () => {
    const Main = mount(
      <Aside open={true} withCustomScroll={false}>
        <div className="content">content</div>
      </Aside>
    );
    expect(Main.find('.content').text()).toBe('content');
  });

  it('should renders with overlay', () => {
    const Main = mount(
      <Aside open={true} withCustomScroll={false}>
        <div className="content">content</div>
      </Aside>
    );
    expect(Main.find('.overlay').length).toBe(1);
  });

  it('should renders without overlay when pass prop showOverlay false', () => {
    const Main = mount(
      <Aside open={true} showOverlay={false} withCustomScroll={false}>
        <div className="content">content</div>
      </Aside>
    );
    expect(Main.find('.overlay').hasClass('overlay--anim-out')).toBeTruthy();
  });
  it('should call onOverlay prop when click overlay section', () => {
    const onOverlay = jest.fn();
    const Main = mount(
      <Aside open={true} isBackBtn={true} onOverlay={onOverlay} withCustomScroll={false}>
        <div className="content">content</div>
      </Aside>
    );
    Main.find('.overlay').simulate('click');
    expect(onOverlay).toBeCalled();
  });

  it('should call onClosed after portal is closed', () => {
    jest.useFakeTimers();

    const onClosed = jest.fn();
    const Main = mount(
      <Aside open={true} isBackBtn={true} withCustomScroll={false} onClosed={onClosed}>
        <div className="content">content</div>
      </Aside>
    );
    Main.instance().close();
    jest.runAllTimers();

    expect(onClosed).toBeCalled();
  });

  it('should renders with title when pass prop title', () => {
    const Main = mount(
      <Aside open={true} title={'title'} withCustomScroll={false}>
        <div className="content">content</div>
      </Aside>
    );
    expect(Main.find('.m-aside-header__title').text()).toBe('title');
  });

  it('should renders with back button when pass prop isBackBtn', () => {
    const Main = mount(
      <Aside open={true} isBackBtn withCustomScroll={false}>
        <div className="content">content</div>
      </Aside>
    );
    expect(Main.find('.m-aside-header__left Button').props().type).toBe('arrowLeft');
  });

  it('should call onBack prop when click back btn', () => {
    const onBack = jest.fn();
    const Main = mount(
      <Aside open={true} isBackBtn={true} onBack={onBack} withCustomScroll={false}>
        <div className="content">content</div>
      </Aside>
    );
    Main.find('.m-aside-header__left .m-button').simulate('click');
    expect(onBack).toBeCalled();
  });

  it('should renders with submit button when pass prop isSubmitBtn', () => {
    const Main = mount(
      <Aside open={true} isSubmitBtn withCustomScroll={false}>
        <div className="content">content</div>
      </Aside>
    );
    const btnProps = Main.find('.m-aside-header__right Button').props();
    expect(btnProps.type).toBe('text');
    expect(btnProps.title).toBe('Done');
  });

  it('should call onSubmit prop when click submit btn', () => {
    const onSubmit = jest.fn();
    const Main = mount(
      <Aside open={true} isSubmitBtn onSubmit={onSubmit} withCustomScroll={false}>
        <div className="content">content</div>
      </Aside>
    );
    Main.find('.m-aside-header__right .m-button').simulate('click');
    expect(onSubmit).toBeCalled();
  });

  it('should renders with custom content in the left header section when pass prop headerLeft', () => {
    const Main = mount(<Aside open={true} headerLeft={<div className="test">1</div>} />);
    expect(Main.find('.m-aside-header__left .test').length).toBe(1);
  });

  it('should renders with custom content in the right header section when pass prop headerRight', () => {
    const Main = mount(<Aside open={true} headerRight={<div className="test">1</div>} />);
    expect(Main.find('.m-aside-header__right .test').length).toBe(1);
  });

  it('should renders with portal by default ', () => {
    const Main = shallow(<Aside open={true} />);
    expect(Main.find('PortalWithState').length).toBe(2);
  });

  it('should renders without portal when pass usePortal false', () => {
    const Main = shallow(<Aside open={true} usePortal={false} />);
    expect(Main.find('PortalWithState').length).toBe(0);
    expect(Main.find('.m-aside').length).toBe(1);
  });

  it('should hide main scroll on open by default', () => {
    const Main = shallow(<Aside open={true} usePortal={false} />);
    expect(utils.hideMainScroll).toHaveBeenCalledWith(true)
  });

  it('should not hide main scroll on open when pass hideScrollOnOpen false', () => {
    const Main = shallow(<Aside open={true} usePortal={false} hideScrollOnOpen={false}/>);
    expect(utils.hideMainScroll).toHaveBeenCalledWith(false)
  });

  it('should not render asideLeftContent by default', () => {
    const Main = shallow(<Aside open={true} usePortal={false} hideScrollOnOpen={false}/>);
    expect(Main.find('.test-aside-left').length).toBe(0)
  });

  it('should render asideLeftContent', () => {
    const Main = shallow(<Aside open={true} asideLeftContent={<p className='test-aside-left-content'>test</p>} usePortal={false} hideScrollOnOpen={false}/>);
    expect(Main.find('.test-aside-left-content').text()).toBe('test')
  });

});
