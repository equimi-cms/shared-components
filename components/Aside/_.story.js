/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import {Aside} from './Aside';

storiesOf('Shared/Aside', module)
  .add('default hidden', () => <Aside>123</Aside>)
  .add('open', () => <Aside open={true}>Content</Aside>)
  .add('with title', () => (
    <Aside open={true} title={'Title'}>
      Content
    </Aside>
  ))
  .add('with backBtn', () => (
    <Aside open={true} isBackBtn title={'Title'}>
      Content
    </Aside>
  ))
  .add('without overlay', () => (
    <Aside open={true} isBackBtn showOverlay={false} title={'Title'}>
      Content
    </Aside>
  ))
  .add('with submit button', () => (
    <Aside open={true} isBackBtn isSubmitBtn title={'Title'}>
      Content
    </Aside>
  ))
  .add('with delayedContent 200ms', () => (
    <Aside open={true} delayedContent={200} isBackBtn isSubmitBtn title={'Title'}>
      Delayed Content <br /> rendered after 200ms
    </Aside>
  ))
  .add('with delayedContent 1200ms', () => (
    <Aside open={true} delayedContent={1200} isBackBtn isSubmitBtn title={'Title'}>
      Delayed Content <br /> rendered after 1200ms
    </Aside>
  ))
  .add('with contentType no-h-padding', () => (
    <Aside open={true} isBackBtn isSubmitBtn contentType="no-h-padding" title={'Title'}>
      Content
    </Aside>
  ));
