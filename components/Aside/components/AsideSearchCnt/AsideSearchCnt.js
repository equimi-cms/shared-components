// @flow
import React from "react";
import "./AsideSearchCnt.scss";
import classNames from "classnames";
import Loader from "../../../../Loader/Loader";
import AsideSearch from "../AsideSearch/AsideSearch";
import Scroll from "../../../Scroll/Scroll";

const defaultContentWrapper = props => <>{props.children}</>;

interface IAsideSearchCntProps {
  isMobile: Boolean;
  isLoading: Boolean;
  placeholder: any;
  listClass?: String;
  contentWrapper?: React.Element;
  contentAfterList?: React.Element;
  itemClass: String;
  listItemClass: String;
  onSearch: () => void;
  onLoadMore: () => void;
  data: Array<React.Element>;
  noEntitiesTitle: String;
  hideClearButton: Boolean;
}

export const AsideSearchCnt = ({
  hideClearButton,
  isMobile,
  contentWrapper,
  contentAfterList,
  placeholder,
  isLoading,
  itemClass,
  onSearch,
  listClass,
  listItemClass,
  data,
  onLoadMore,
  noEntitiesTitle
}: IAsideSearchCntProps) => {
  const renderData = data.map(el =>
    itemClass ? (
      <div className={itemClass} key={el.key}>
        {el}
      </div>
    ) : (
      el
    )
  );
  const onScrollBoundary = ([isReached]) => isReached && onLoadMore();
  const noResults = !data.length && (
    <div className="m-search-aside-list__no-results test-no-results">
      {noEntitiesTitle}
    </div>
  );
  const loader = (
    <div className="indent align-center js-loader-cnt">
      <Loader size={24} />
    </div>
  );
  const ContentWrapper = contentWrapper || defaultContentWrapper;

  return (
    <>
      <AsideSearch
        isMobile={isMobile}
        isSearchBtn={false}
        disableAutoFocus={isMobile}
        placeholder={placeholder}
        onSearch={onSearch}
        hideClearButton={hideClearButton}
      />

      <div
        className={classNames([
          "m-search-aside-list test-aside-list",
          listClass
        ])}
      >
        <Scroll boundary="80%" onScrollBoundary={onScrollBoundary}>
          <ContentWrapper>
            {!noResults && <div className={listItemClass}>{renderData}</div>}
            {isLoading ? loader : noResults}
            {contentAfterList}
          </ContentWrapper>
        </Scroll>
      </div>
    </>
  );
};

export default React.memo(AsideSearchCnt);

AsideSearchCnt.defaultProps = {
  hideClearButton: false,
  topContent: null,
  isMobile: false,
  isLoading: false,
  contentWrapper: defaultContentWrapper,
  itemClass: "m-search-aside-list__item",
  listItemClass: "",
  data: [],
  onSearch: /* istanbul ignore next */ () => null,
  onLoadMore: /* istanbul ignore next */ () => null,
  placeholder: ""
};
