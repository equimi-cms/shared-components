import React from 'react';
import {shallow,mount} from 'enzyme/build';
import AsideSearchCnt from './AsideSearchCnt';
import AsideSearch from '../AsideSearch/AsideSearch';

describe('AsideSearchCnt', () => {
  it('should render', () => {
    const component = shallow(<AsideSearchCnt data={[]} />);
    expect(component.find(AsideSearch).length).toBe(1);
  });

  it('should render loader when isLoading', () => {
    const component = shallow(<AsideSearchCnt data={[]} isLoading />);
    expect(component.find('.js-loader-cnt').length).toBe(1);
  });

  it('should render no results when isLoading false', () => {
    const component = shallow(<AsideSearchCnt data={[]} isLoading={false} />);
    expect(component.find('.test-no-results').length).toBe(1);
  });

  it('should render listClass', () => {
    const component = shallow(<AsideSearchCnt data={[]} listClass={'test'} />);
    expect(component.find('.test-aside-list').hasClass('test')).toBeTruthy();
  });

  it('should render list items with itemClass', () => {
    const component = shallow(
      <AsideSearchCnt
        itemClass={'test-item'}
        data={[<div>1</div>, <div>2</div>]}
        listClass={'test'}
      />
    );
    expect(component.find('.test-item').length).toBe(2);
  });

  it('should render with contentWrapper', () => {
    const component = mount(
      <AsideSearchCnt
        contentWrapper={({children}) => <div className="wrapper">{children}</div>}
        data={[<div>1</div>, <div>2</div>]}
        listClass={'test'}
      />
    );
    expect(component.find('.wrapper').length).toBe(1);
  });
});
