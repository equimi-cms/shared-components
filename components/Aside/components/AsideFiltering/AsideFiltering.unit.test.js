import React from "react";
import {mount, shallow} from "enzyme/build";
import AsideFiltering from "./AsideFiltering";

describe("AsideFiltering", () => {
  it("should render the list of filters while show is true and data is right", () => {
    const mockData = [
      {title: "test", value: "test"},
      {title: "test2", value: "test2"},
    ];
    const component = shallow(<AsideFiltering show data={mockData} />);
    expect(component.find(".test-filter-list-show").length).toBe(1);
    expect(component.find(".js-filter-item").length).toBe(2);
  });

  it("should render active button if value is not empty", () => {
    const mockData = [
      {title: "test", value: "test"},
      {title: "test2", value: "test2"},
    ];
    const component = shallow(<AsideFiltering show data={mockData} value={mockData[1].value} />);
    expect(component.find(".test-entity-filter-active-button").length).toBe(1);
  });

  it("should call onToggleFiltering when click filter button", () => {
    const mockOnToggleFiltering = jest.fn();
    const component = mount(
      <AsideFiltering
        data={[
          {title: "test", value: "test"},
          {title: "test2", value: "test2"},
        ]}
        onToggleFiltering={mockOnToggleFiltering}
      />
    );
    component.find(".test-entity-filter-button").simulate("click");
    expect(mockOnToggleFiltering).toHaveBeenCalled();
  });
});
