import React, {memo} from "react";
import filterButton from "../../../Horses/HorseList/imgs/filter-results-button.svg";
import FilterListItem from "../../../Horses/HorseList/FilterListItem/FilterListItem";
import SvgInlineImage from "../../../../Image/SvgInlineImage/SvgInlineImage";
import classNames from "classnames";
import "./AsideFiltering.scss";


interface IAsideFilteringProps {
  onToggleFiltering: () => void,
  value: string,
  data: [{title: string, value: string}],
  show: boolean,
  onSearch: () => void,
}

const AsideFiltering = ({onToggleFiltering, value, data, show, onSearch}: IAsideFilteringProps) => {
  if (!data.length) return null;
  return (
    <>
      <div
        onClick={onToggleFiltering}
        className={classNames([
          "entity-filter__button test-entity-filter test-entity-filter-button",
          value !== "" && "entity-filter__button--active test-entity-filter-active-button",
        ])}
      >
        <SvgInlineImage src={filterButton}/>
      </div>
      <div
        className={classNames(
          "entity-filter__list-cnt",
          show && "entity-filter__list-cnt--show test-filter-list-show",
        )}
      >
        <ul className={classNames("entity-filter__list js-filter-list")}>
          {data.map(({title, value}) => (
            <FilterListItem
              onClick={onSearch}
              key={value}
              value={value}
              className="entity-filter__item js-filter-item"
              title={title}
            />
          ))}
        </ul>
        <div
          onClick={onToggleFiltering}
          className={classNames(
            show && "overlay--anim-in",
            "overlay entity-filter__overlay",
          )}
        />
      </div>
    </>
  );

};

AsideFiltering.defaultProps = {
  onToggleFiltering: () => null,
  value: "",
  data: [],
  show: false,
  onSearch: () => null,
};

export default memo(AsideFiltering);
