import React from 'react';
import {shallow,mount} from 'enzyme/build';
import {AsideSearch} from './AsideSearch';

describe('AsideSearch', () => {
  it('should render', () => {
    const component = shallow(<AsideSearch/>);
    expect(component.find('.test-aside-search').length).toBe(1);
  });


  it('should call onSearch when click search button', () => {
    const onSearch = jest.fn();
    const component = mount(<AsideSearch onSearch={onSearch}/>);
    component.find('.test-s-input-cnt__btn').simulate('click');
    expect(onSearch).toHaveBeenCalledWith('');
  });
});
