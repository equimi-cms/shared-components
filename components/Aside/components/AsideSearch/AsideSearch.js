//@flow
import React from "react";
import "./AsideSearch.scss";
import classNames from "classnames";
import SearchInput from "../../../Inputs/SearchInput/SearchInput";
import type { ISearchInputProps } from "../../../Inputs/SearchInput/SearchInput";

interface IAsideSearch extends ISearchInputProps {
  className?: String;
  searchBtnText?: any;
  onSearch: (val: String) => void;
  disableAutoFocus: Boolean;
}

/**
 * Wrapper over SearchInput
 */
export const AsideSearch = ({
  inputClassName,
  searchBtnText,
  className,
  onSearch,
  debounceTime,
  hideClearButton,
  disableAutoFocus,
  ...props
}: IAsideSearch) => {
  return (
    <div className={classNames("m-aside-search test-aside-search", className)}>
      <SearchInput
        disableAutoFocus={disableAutoFocus}
        hideClearButton={hideClearButton}
        inputClassName={inputClassName}
        {...props}
        onSearch={onSearch}
        onChange={onSearch}
        debounceTime={debounceTime}
        searchBtnText={searchBtnText}
      />
    </div>
  );
};

export default React.memo(AsideSearch);

AsideSearch.defaultProps = {
  className: "",
  debounceTime: 300,
  onSearch: /* istanbul ignore next */() => null
};
