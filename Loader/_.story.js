/* eslint-disable import/first */ import React from 'react';
import {storiesOf} from '@storybook/react';
import Loader from './Loader';
import {Common} from '../../../../.storybook/config';

storiesOf(Common + '/Loader', module)
  .add('default', () => <Loader />)
  .add('size 50px', () => <Loader size="30px" />)
  .add('type full', () => (
    <div>
      <p>Position relative to the parent block</p>
      <div style={{width: '400px', height: '400px', position: 'relative'}}>
        <Loader type="full" />
      </div>
    </div>
  ))
  .add('type full-screen', () => (
    <div>
      <p>Position to the screen</p>
      <Loader type="full-screen" />
    </div>
  ));
