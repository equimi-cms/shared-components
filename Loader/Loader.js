import React, { useMemo } from "react";
import { Image } from "../Image/Image";
import classNames from "classnames";
import "./Loader.scss";
import { renderTypesAsClassNames } from "../utils/utils";

interface ILoaderProps {
  className: string;
  size: string | number;
  type: "" | "full-screen" | "full" | "full-over-all" | "transparent-bg";
  icon: "primary" | "secondary";
  show: boolean;
}

export const Loader = ({ show, size, type, icon, className }: ILoaderProps) => {
  const style = useMemo(() => ({ width: size }), [size]);

  const rootClass = useMemo(
    () =>
      classNames(
        "loader",
        renderTypesAsClassNames(type, "loader--"),
        className
      ),
    [type, className]
  );

  const loaderIcon = useMemo(() => require(`./imgs/${icon}.gif`), [icon]);

  if (!show) return null;
  return (
    <span className={rootClass}>
      <span className="loader__cnt">
        <Image
          style={style}
          className="loader__img"
          alt="loader"
          src={loaderIcon}
        />
      </span>
    </span>
  );
};

export default React.memo(Loader);

Loader.defaultProps = {
  className: "",
  size: "44px",
  icon: "primary",
  type: "",
  show: true,
};
