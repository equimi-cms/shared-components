import React from "react";
import { shallow } from "enzyme";
import Loader from "./Loader";

describe("Loader", () => {
  it("should renders", () => {
    const component = shallow(<Loader show />);
    expect(component.find(".loader").length).toEqual(1);
  });

  it("should renders in full screen when set type full-screen", () => {
    const component = shallow(<Loader show type={"full-screen"} />);
    expect(
      component.find(".loader").hasClass("loader--full-screen")
    ).toBeTruthy();
  });

  it("should renders when size set to 24px", () => {
    const component = shallow(<Loader show size="24px" />);
    expect(
      component.find("Image").dive().find("img").prop("style").width
    ).toEqual("24px");
  });

  it("should be hidden when prop show set to false", () => {
    const component = shallow(<Loader show={false} />);
    expect(component.text()).toBe("");
  });
});
