import {Entities} from "../utils/types";

const maxImageSizeInMb = 10;

export default {
  editPhotoOptions: {
    minZoom: 0,
    maxZoom: 2,
    step: 0.1,
  },
  maxImageSizeInMb,
  loadData: {
    count: 20, // number of items to load
  },
  withServer: true, //allow use app without server
  maxImageSizeInByte: 1024 * 1024 * maxImageSizeInMb,
  coverPhotoRatio: 16 / 9,
  aboutPhotoRatio: 1,
  logoRatio: 1,
  supportedFileTypes: ["jpg", "jpeg", "png"],
  phoneWidth: 479, //px
  tabletWidth: 1000, //px
  priceSymbol: "£",
  accentColor: {
    hex: "#3a8772",
    hsl: [164, 40, 38],
  },
  imageSrc: process.env.REACT_APP_IMAGE_SRC,
  sectionsOrder: [
    {type: Entities.NEWS, render: true},
    {type: Entities.SERVICE, render: true},
    {type: Entities.HORSE, render: true},
    {type: Entities.TEAM, render: true},
    {type: Entities.SPONSOR, render: true},
    {type: Entities.RESULT, render: true},
  ],
  /**
   * Don't forget to update TEMPLATES in root/webpack.config.js
   */
  TEMPLATES: {
    default: {
      name: "default",
      title: "Standard_White",
      type: "basic",
      allowAccentColor: true,
      coverPhoto: {
        aspectRatioWidth: 2,
        aspectRatioHeight: 1,
      },
    },
    darkDefault: {
      name: "darkDefault",
      title: "Standard_Black",
      type: "pro",
      allowAccentColor: true,
      coverPhoto: {
        aspectRatioWidth: 2,
        aspectRatioHeight: 1,
      },
    },
    winterGrey: {
      name: "winterGrey",
      title: "Winter_Grey",
      type: "business",
      coverPhoto: {
        aspectRatioWidth: 24,
        aspectRatioHeight: 10,
      },
    },
  },
};
