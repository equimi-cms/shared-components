export const Entities = {
    NEWS: 'news',
    HORSE: 'horses',
    TEAM: 'team',
    MEMBER: 'members',
    RESULT: 'results',
    SPONSOR: 'sponsors',
    SERVICE: 'services',
    CONTACTS: 'contacts',
    ALL: 'all',
};

export type Entity =
    | Entities.NEWS
    | Entities.HORSE
    | Entities.TEAM
    | Entities.MEMBER
    | Entities.RESULT
    | Entities.SPONSOR
    | Entities.SERVICE
    | Entities.ALL;

export const Pages = {
    HOMEPAGE: 'Homepage',
    FOOTER: "Footer",
    LANDING: "Landing"
};

export type Page =
    | Pages.HOMEPAGE
    | Pages.TEAM
    | Pages.HORSES
    | Pages.NEWS
    | Pages.RESULTS
    | Pages.SPONSORS
    | Pages.SERVICE
    | Pages.LANDING
