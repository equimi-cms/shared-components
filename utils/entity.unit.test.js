import {getAlias, getEntityTitle} from './entity';

describe('getAlias', () => {
  it('should return alias when pass string', () => {
    expect(getAlias('alias')).toBe('alias');
  });
  it("should return alias when pass object {urlAlias:'alias'}", () => {
    expect(getAlias({urlAlias: 'alias'})).toBe('alias');
  });

  it("should return '' when pass object without urlAlias {urlAliasTest:'alias'}", () => {
    expect(getAlias({urlAliasTest: 'alias'})).toBe('');
  });
});

describe('getEntityTitle', () => {
  it('should return title', () => {
    expect(getEntityTitle({title: '1'})).toBe('1');
  });

  it('should return title when title is empty', () => {
    expect(getEntityTitle({title: ''})).toBe('');
  });

  it('should return name', () => {
    expect(getEntityTitle({name: '2'})).toBe('2');
  });

  it('should return name when name is empty', () => {
    expect(getEntityTitle({name: ''})).toBe('');
  });

  it('should return title as showName and className', () => {
    expect(getEntityTitle({title: null, name: null, showName: '1', className: '2'})).toBe('1 2');
  });

  it('should return null', () => {
    expect(getEntityTitle()).toBeNull();
  });
});
