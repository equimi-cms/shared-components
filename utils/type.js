import type {IContacts} from "../components/Contacts/utils/types";

export interface IBreedingData {
  studbook?: String;
  sire?: String;
  sireStudbook?: String;
  dame?: String;
  dameStudbook?: String;
  sireOfDame?: String;
  sireOfDameStudbook?: String;
}

export interface IPreference {
  category: Array<string>;
  member: number;
  horse: number;
}

export interface ICurrency {
  code: string;
  decimal_digits: number;
  name: string;
  name_plural: string;
  rounding: number;
  symbol: string;
  symbol_native: string;
}

export interface IVideo {
  videoId: String;
  date: String;
}

export interface ImageOptions {
  x: Number;
  y: Number;
  width: Number;
  height: Number;
  rotate: Number;
  scaleX: Number;
  scaleY: Number;
  zoom: Number;
  canvas: {
    height: Number,
    left: Number,
    naturalHeight: Number,
    naturalWidth: Number,
    top: Number,
    width: Number,
  };
  editor: {
    width: Number,
    height: Number,
  };
  index?: Number;
  title?: String;
}

export interface Image {
  image: String;
  options: ImageOptions;
  original: String;
}

export interface IImage extends Image {}

export interface IMember {
  id: String;
  isPublished: Boolean;
  isDraftViewed: Boolean;
  name: String;
  description: String;
  disciplines: [];
  coverImage: {};
  videos: [];
  horses: [];
}

export interface INews {
  id: Number;
  title: String;
  body: String;
  created: String;
  isPublished: Boolean;
  videos: Array<String>;
  coverImage: {};
  createdAt: Date;
}

export interface IHorseBirthday {
  year: String;
  month?: String;
  day?: String;
}

export interface IHorse {
  name: String;
  gender: Number;
  level: String;
  height: String;
  color: String;
  description: String;
  birthday: IHorseBirthday;
  isPublished: Boolean;
  status?: Number;
  owner?: String;
  breeding: IBreedingData;
  coverImage: IImage;
}

export interface IAddResultData {
  memberName: String;
  horseName: String;
  className: String;
  level: String;
  place: String;
  date: String;
  showName: String;
  country: String;
  description: String;
}

export interface IResult extends IAddResultData {
  coverPhoto: IImage;
}

export interface IService {}

export interface IMedia {
  id: String;
  accountId: String;
  userId: String;
  entityId: String;
  entityType: String;
  title: String;
  desc: String;
  results: Array<IResult>;
  horses: Array<IHorse>;
  members: Array<IMember>;
  content: IImage | IVideo;
  created: String;
}

export interface IProfile {
  id: String;
  name: String;
  email: String;
  phoneNumber: String;
  facebookId: String;
}

export interface IPlan {
  id: String;
  name: String;
  isActive: Boolean;
  created: String;
  price: Number;
}

export interface ISelectData {
  id: any;
  title: String;
}

export interface IFont {
  title: string;
  fontName: string;
  [number]: {
    styles: {
      desktop: any,
      mobile: any,
    },
  };
}

export interface ILogo {
  text: string;
  fontName: string;
}

export interface IAccount {
  facebookId?: number;
  name: string;
  title: string;
  about: string;
  email: string;
  preferences: IPreference;
  currency: ICurrency;
  avatarImage?: IImage;
  coverImage?: IImage;
  logoImage?: IImage & {isLoading?: boolean};
  faviconImage?: IImage & {isLoading?: boolean};
  urlAlias: string;
  domain: string;
  plan: {name: string, status: string};
  domainStatus: {
    domainStatus: boolean,
  };
  settings: {
    notifyDrafts: boolean,
    serverIp: string,
  };
  contacts: IContacts;
  profile: IProfile;
  logoMeta: ILogo & {isLoading?: boolean};
}

export interface ITemplate {
  name: string;
  title: string;
  type: string;
  allowAccentColor: boolean;
}
