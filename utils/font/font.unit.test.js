import React from 'react';
import {getFontPropsBy, getGoogleFontSrc, getLogoFont} from './font';
import {FONTS} from '../data';

const montserratData = FONTS[0];

describe('getFontPropsBy', () => {
  it('should return font by title', () => {
    expect(getFontPropsBy('Montserrat')).toEqual(FONTS[0]);
  });

  it('should return font by fontName', () => {
    expect(getFontPropsBy('Montserrat', 'fontName')).toEqual(montserratData);
  });

  it('should return undefined when no args passed', () => {
    expect(getFontPropsBy()).toBeUndefined();
  });
});

describe('getGoogleFontSrc', () => {
  it('should return proper font src for Montserrat font', () => {
    expect(
      getGoogleFontSrc([
        {
          id: 'Montserrat',
          title: 'Montserrat',
          weight: 400,
        },
      ])
    ).toEqual('https://fonts.googleapis.com/css2?family=Montserrat:wght@400&display=swap');
  });

  it('should return empty string when no data passed', () => {
    expect(getGoogleFontSrc([])).toEqual('');
  });

  it('should return empty string when args passed', () => {
    expect(getGoogleFontSrc()).toEqual('');
  });
});

describe('getLogoFont', () => {
  it('should return logo font by title when its mobile', () => {
    expect(getLogoFont('Montserrat', 'title', 400, true)).toEqual({
      fontFamily: montserratData.fontName,
      styles: montserratData[400].styles.mobile,
    });
  });

  it('should return logo font by title when its desktop', () => {
    expect(getLogoFont('Montserrat', 'title', 400, false)).toEqual({
      fontFamily: montserratData.fontName,
      styles: montserratData[400].styles.desktop,
    });
  });

  it('should return object with undefined styles when no font found', () => {
    expect(getLogoFont('Montserrat', 'title', 710, false)).toEqual({
      fontFamily: 'Montserrat',
      styles: undefined,
    });
  });
});
