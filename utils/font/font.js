import type {IFont, ISelectData} from '../type';
import {FONTS} from '../data';

/**
 *
 * @param value
 * @param by
 * @param fontsData
 * @returns {IFont}
 */
export const getFontPropsBy = (
  value: any,
  by: string = 'title',
  fontsData: [IFont] = FONTS
): IFont => {
  return fontsData.filter(el => el[by] === value)[0];
};

/**
 *
 * @param fontsData
 * @returns {string}
 */
export const getGoogleFontSrc = (fontsData: [ISelectData & {weight: number}] = []) => {
  const fontsSrc = fontsData
    .map(({title, weight}) => {
      const {fontName} = getFontPropsBy(title) ?? {};
      return fontName ? `family=${fontName.replace(' ', '+')}:wght@${weight}` : '';
    })
    .join('&');

  return fontsSrc ? `https://fonts.googleapis.com/css2?${fontsSrc}&display=swap` : '';
};

/**
 *
 * @param value
 * @param by
 * @param weight
 * @param isMobile
 * @returns {{fontFamily: string, styles: *}}
 */
export const getLogoFont = (
  value,
  by: string,
  weight: number,
  isMobile: boolean
): {fontFamily: string, styles: any} => {
  const fontProps = getFontPropsBy(value, by);
  const styles = fontProps ? fontProps[weight]?.styles : {};
  const {desktop, mobile} = styles ?? {};
  return {
    fontFamily: fontProps?.fontName,
    styles: isMobile ? mobile : desktop,
  };
};

/**
 *
 * @param value
 * @returns {{weight: (*|string), title: (*|string)}}
 */
export const parseFontProps = (value): {title: string, weight: string | number} => {
  const [title, weight] = value ? value.split('@') : [];
  return {
    title,
    weight,
  };
};

/**
 *
 * @param title
 * @param weight
 * @returns {string}
 */
export const combineFontProps = (title: string, weight: string | number): string => {
  return `${title}@${weight}`;
};
