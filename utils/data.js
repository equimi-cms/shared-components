import type {IFont, ISelectData} from "./type";

export const GENDER: [ISelectData] = [
  {
    title: "Stallion",
    id: 0,
  },
  {
    title: "Mare",
    id: 1,
  },
  {
    title: "Gelding",
    id: 2,
  },
  {
    title: "Filly",
    id: 3,
  },
  {
    title: "Colt",
    id: 4,
  },
];

export const HORSE_GENDER = {
  STALLION: 0,
  MARE: 1,
  GELDING: 2,
  FILLY: 3,
  COLT: 4,
};

export const STATUS = [
  {
    title: "For_Breeding",
    value: 0,
  },
  {
    title: "For_Sale",
    value: 1,
  },
  {
    title: "Sold",
    value: 2,
  },
  {
    title: "Retired",
    value: 3,
  },
];

export const HORSE_STATUS = {
  FOR_BREEDING: 0,
  FOR_SALE: 1,
  SOLD: 2,
  RETIRED: 3,
};

export const MONTSERRAT: IFont = {
  title: "Montserrat",
  fontName: "Montserrat",
  400: {
    styles: {
      desktop: {
        letterSpacing: "-1px",
      },
      mobile: {
        letterSpacing: "-0.6px",
      },
    },
  },
};
export const EXO: IFont = {
  title: "Exo",
  fontName: "Exo",
  500: {
    styles: {
      desktop: {
        letterSpacing: 0,
      },
      mobile: {
        letterSpacing: 0,
      },
    },
  },
};
export const ROBOTO_SLAB: IFont = {
  title: "Roboto Slab",
  fontName: "Roboto Slab",
  400: {
    styles: {
      desktop: {
        letterSpacing: "-0.2px",
      },
      mobile: {
        letterSpacing: "-0.1px",
      },
    },
  },
};
export const FREDOKA_ONE: IFont = {
  title: "Fredoka One",
  fontName: "Fredoka One",
  400: {
    styles: {
      desktop: {
        letterSpacing: 0,
      },
      mobile: {
        letterSpacing: 0,
      },
    },
  },
};
export const NUNITO_SANS: IFont = {
  title: "Nunito Sans",
  fontName: "Nunito Sans",
  400: {
    styles: {
      desktop: {
        letterSpacing: "2px",
      },
      mobile: {
        letterSpacing: "1.2px",
      },
    },
  },
};
export const ROBOTO: IFont = {
  title: "Roboto",
  fontName: "Roboto",
  400: {
    styles: {
      desktop: {
        letterSpacing: "1.5px",
      },
      mobile: {
        letterSpacing: "0.9px",
      },
    },
  },
};
export const NUNITO: IFont = {
  title: "Nunito",
  fontName: "Nunito",
  400: {
    styles: {
      desktop: {
        letterSpacing: "4px",
      },
      mobile: {
        letterSpacing: "2.4px",
      },
    },
  },
};
export const NOVA_SQUARE: IFont = {
  title: "Nova Square",
  fontName: "Nova Square",
  400: {
    styles: {
      desktop: {
        letterSpacing: "4px",
      },
      mobile: {
        letterSpacing: "2.4px",
      },
    },
  },
};

export const FONTS: [IFont] = [
  MONTSERRAT,
  EXO,
  ROBOTO_SLAB,
  FREDOKA_ONE,
  NUNITO_SANS,
  ROBOTO,
  NUNITO,
  NOVA_SQUARE,
];

export const LOGO_FONTS = [
  {
    title: "Montserrat",
    weight: 400,
    id: "Montserrat@400",
  },

  {
    title: "Exo",
    weight: 500,
    id: "Exo@500",
  },

  {
    title: "Roboto Slab",
    weight: 400,
    id: "Roboto Slab@400",
  },

  {
    title: "Fredoka One",
    weight: 400,
    id: "Fredoka One@400",
  },

  {
    title: "Nunito Sans",
    weight: 400,
    id: "Nunito Sans@400",
  },

  {
    title: "Roboto",
    weight: 400,
    id: "Robotos@400",
  },

  {
    title: "Nunito",
    weight: 400,
    id: "Nunito@400",
  },

  {
    title: "Nova Square",
    weight: 400,
    id: "Nova Square@400",
  },
];
