import config from "../config";
import upperFirst from "lodash/upperFirst";
import {Entities} from "./types";
import Moment from "moment/moment";
import isObject from "lodash/isObject";
import type {Entity} from "./types";
import {STATUS} from "./data";

/**
 *
 * @returns {string}
 */
export const getUid = () => {
  const S4 = () => (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  return S4() + S4() + "-" + S4() + S4();
};

/**
 *
 * @param imageName
 * @param withHash
 * @param imageSrc
 * @returns {string}
 */
export const getImageSrc = (
  imageName: string,
  withHash: false,
  imageSrc: String = config.imageSrc
) => {
  const hash = withHash ? `?${getUid()}` : "";
  return imageName ? `${imageSrc}/${imageName}${hash}` : "";
};

/**
 *
 * @param data
 * @param size
 * @param withHash
 * @param imageSrc
 */
export const getImageSrcWithSize = (
  data: {image: string, original: string, sizes: {}} = {
    image: null,
    original: null,
    sizes: {},
  },
  size: string = "",
  withHash: boolean = false,
  imageSrc: String = config.imageSrc
) => {
  if (!Object.keys(data).length) {
    return "";
  }

  const isSize = data.hasOwnProperty("sizes") && data.sizes.hasOwnProperty(size);
  return getImageSrc(isSize ? data.sizes[size] : data.image, withHash, imageSrc);
};

/**
 *
 * @param entity
 * @returns {*}
 */
export const getCapitalEntityTitle = (entity: Entity) => {
  return upperFirst(entity);
};
/**
 *
 * @param entity
 * @param isUpperFirst
 * @returns {*}
 */
export const getSingleEntityTitle = (entity: Entity, isUpperFirst: boolean = false) => {
  if (!entity) {
    return console.warn("No entity");
  }

  const _upperFirst = (value) => (isUpperFirst ? upperFirst(value) : value);

  if (entity === Entities.NEWS || entity === Entities.ALL) {
    return _upperFirst(entity);
  }

  let _entityTitle = Array.from(entity);
  _entityTitle.pop();
  _entityTitle = _entityTitle.join("");

  return _upperFirst(_entityTitle);
};

/**
 *
 * @param entity
 * @returns {*}
 */
export const getPluralEntityTitle = (entity: String) => {
  if (!entity) return console.warn("No entity");
  const isAlreadyPlural =
    entity[entity.length - 1] === "s" || entity === Entities.NEWS || entity === Entities.ALL;
  if (isAlreadyPlural) return entity;
  return `${entity}s`;
};

export const getScrollbarWidth = () => {
  const isServerEnv = typeof document === "undefined";
  if (isServerEnv) return;

  // Creating invisible container
  const outer = document.createElement("div");
  outer.style.visibility = "hidden";
  outer.style.overflow = "scroll";
  outer.style.msOverflowStyle = "scrollbar";
  document.body.appendChild(outer);

  const inner = document.createElement("div");
  outer.appendChild(inner);
  const scrollbarWidth = outer.offsetWidth - inner.offsetWidth;
  outer.parentNode.removeChild(outer);
  return scrollbarWidth;
};

export const scrollbarWidth = getScrollbarWidth();

export const getRequestErrorMsg = (e) => {
  console.warn(e);

  try {
    let msg = e.response;

    if (!msg) return "Network error. Check your internet connection";

    msg = msg.hasOwnProperty("data") ? msg.data : null;
    msg =
      !!msg && msg.hasOwnProperty("error") && msg.error.hasOwnProperty("message")
        ? msg.error.message
        : "Network Error";
    return msg;
  } catch (e) {
    console.warn("Catch:", e);
    return null;
  }
};

export const getTitle = (title: string) => {
  return `Equimi | ${title}`;
};

export const hideMainScroll = (hide: boolean = true) => {
  document.body.classList.toggle("no-scroll", hide);

  // Removing scroll from html will automatically scroll to top of the page
  document.querySelector("html").classList.toggle("no-scroll", hide);

  //prevent dom rerender
  // document.body.style.paddingRight = `${hide ? scrollbarWidth : 0}px`;

  // scroll to top of the page
  hide && window.scroll(0, 0);
};

export const floatToFixed = (value: number = 0, fixed: number = 2) => {
  return parseFloat(value.toFixed(fixed));
};

/**
 * Convert from array of types accept type for input
 * ['png','svg'] => .png,.svg
 * @param supportedFileTypes
 * @returns {string}
 */
export const getSupportedFileTypes = (
  supportedFileTypes: Array<string> = config.supportedFileTypes
) => {
  return supportedFileTypes.map((el) => `.${el},`).join("");
};

export const getSupportedFileTypesFromString = (supportedFileTypes: string): Array<string> => {
  if (!supportedFileTypes) {
    console.warn("No supportedFileTypes");
  }
  return supportedFileTypes
    ? supportedFileTypes
        .replace(/\./g, "")
        .split(",")
        .filter((el) => el)
    : [];
};

/**
 *
 * @param type
 * @param classPrefix
 * @returns {string}
 */
export const renderTypesAsClassNames = (type: string | Array<string>, classPrefix: string = "") => {
  if (!type) {
    return;
  }
  return Array.isArray(type)
    ? type
        .filter((el) => el)
        .map((el) => classPrefix + el)
        .join(" ")
    : classPrefix + type;
};

/**
 * Shorten number to thousands, millions, billions, etc.
 * http://en.wikipedia.org/wiki/Metric_prefix
 *
 * @param {number} num Number to shorten.
 * @param {number} [digits=0] The number of digits to appear after the decimal point.
 * @returns {string|number}
 *
 * @example
 * // returns '12.5k'
 * shortenLargeNumber(12543, 1)
 *
 * @example
 * // returns '-13k'
 * shortenLargeNumber(-12567)
 *
 * @example
 * // returns '51M'
 * shortenLargeNumber(51000000)
 *
 * @example
 * // returns 651
 * shortenLargeNumber(651)
 *
 * @example
 * // returns 0.12345
 * shortenLargeNumber(0.12345)
 */
export const shortenLargeNumber = (num, digits) => {
  let units = ["k", "M", "G", "T", "P", "E", "Z", "Y"],
    decimal;

  for (let i = units.length - 1; i >= 0; i--) {
    decimal = Math.pow(1000, i + 1);

    if (num <= -decimal || num >= decimal) {
      return +(num / decimal).toFixed(digits) + units[i];
    }
  }

  return num;
};

/**
 * Moment
 * Localized formats `L` 09/04/1986 converts to 09.04.1986
 * @param date
 * @returns {*}
 */
export const getLocalisedLDate = (date) => Moment(date).format("DD.MM.YYYY");
//use later
// Moment(date)
//   .format('L')
//   .replace(/\//g, '.');
/**
 * @param data
 * @param time
 * @returns {Promise<any>}
 */
export const delay = (data: any, time: number = 0): Promise<any> =>
  new Promise((resolve) => setTimeout(() => resolve(data), time));
/**
 *
 * @param data
 * @param id
 * @returns {*}
 */
export const getIndexById = (data: T, id: any): number => data.findIndex((item) => item.id === id);

/**
 * Deep compare of object keys
 * @params obj1
 * @params obj2
 * @returns {boolean}
 */
export const isEqualObjectKeys = (obj1: {}, obj2: {}, verboseLogs = false, expect: () => void) => {
  if (!obj1 || !obj2) {
    return true;
  }
  const keys1 = Object.keys(obj1).sort();
  const keys2 = Object.keys(obj2).sort();

  expect && expect(keys1).toEqual(keys2);
  if (keys1.length !== keys2.length) {
    return false;
  }

  const isKeysEqual = keys1.every((k, i) => {
    expect && expect(k).toEqual(keys2[i]);

    if (k !== keys2[i]) {
      console.warn("Different Key", k);
    }
    return k === keys2[i];
  });

  if (!isKeysEqual) {
    return false;
  }

  return keys1.every((kk) => {
    const v1 = obj1[kk];
    const v2 = obj2[kk];
    if (v1 !== null && isObject(v1) && Object.keys(v1).length) {
      verboseLogs && console.warn("Check field: ", kk);
      return isEqualObjectKeys(v1, v2, verboseLogs, expect);
    } else {
      return true;
    }
  });
};
export const getStatusByValue = (value: number) => {
  const index = STATUS.findIndex((el) => el.value === value);
  return index !== -1 ? STATUS[index] : {value: "", title: ""};
};

/**
 *
 * @returns {undefined}
 */
export const blankFunction = () => undefined;

export const isUnitTestMode = () => typeof window !== "undefined" && window.unitTestMode;
