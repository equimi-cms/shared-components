/**
 *
 * @param elRef
 * @param stickyToRef
 * @param options
 */
export const setStickyPosition = (
  elRef,
  stickyToRef,
  options?: {offsetTop: 0, offsetLeft: 0, stickyToClientRect: {}} = {}
) => {
  if (!elRef?.current) return console.warn("No elRef passed");
  if (!stickyToRef?.current) return console.warn("No stickyToRef passed");

  const el = elRef.current;

  const {offsetTop = 0, offsetLeft = 0, stickyToClientRect} = options;
  const {left = 0, top = 0} = stickyToClientRect
    ? stickyToClientRect
    : stickyToRef?.current.getBoundingClientRect();

  const _left = `${left + offsetLeft}px`;
  const _top = `${top + offsetTop}px`;

  el.style.top = _top;
  el.style.left = _left;
  el.style.setProperty("--left", _left);
  el.style.setProperty("--top", _top);
};


export const isClient = () => {
  return typeof window !== "undefined";
};
