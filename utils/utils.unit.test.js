import {isEqualObjectKeys} from 'utils/utils';

describe('isEqualObjectKeys', () => {
  it('should return true when objects {a: 1} {a: 2}', () => {
    expect(isEqualObjectKeys({a: 1}, {a: 2})).toBeTruthy();
  });

  it('should return true when objects {a: {b: 1}} {a: {b: 2}}', () => {
    expect(isEqualObjectKeys({a: {b: 1}}, {a: {b: 2}})).toBeTruthy();
  });

  it('should return false when objects {a: {c: 1}} {a: {b: 2}}', () => {
    expect(isEqualObjectKeys({a: {c: 1}}, {a: {b: 2}})).toBeFalsy();
  });

  it('should return false when objects {a: {c: {}} {a: {b: 2}}', () => {
    expect(isEqualObjectKeys({a: {c: {}}}, {a: {b: 2}})).toBeFalsy();
  });
});
