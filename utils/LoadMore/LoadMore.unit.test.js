import {LoadMore} from './LoadMore.js';
import config from '../../config';
describe('LoadMore.js', () => {
  it('should disallow load more data by default', () => {
    const lm = new LoadMore(20);
    expect(lm.canLoadMore()).toBeFalsy();
  });

  it('should allow load more data when allItemsCount is more then loadedItemsCount', () => {
    const lm = new LoadMore(20);
    const allItemsCount = 40;
    const loadedItemsCount = 20;

    lm.setAllItemsCount(allItemsCount).setLoadedItemsCount(loadedItemsCount);

    expect(lm.canLoadMore()).toBeTruthy();
  });

  it('should increase offset when call getNextOffset', () => {
    const lm = new LoadMore(20);
    expect(lm.getNextOffset()).toBe(20);
  });

  it('should disallow load more data when loadedItemsCount is equal allItemsCount', () => {
    const lm = new LoadMore(20);
    const allItemsCount = 40;
    const loadedItemsCount = 40;
    lm.setLoadedItemsCount(loadedItemsCount).setAllItemsCount(allItemsCount);
    expect(lm.canLoadMore()).toBeFalsy();
  });

  it('should reset to default when call reset()', () => {
    const lm = new LoadMore(20);
    const allItemsCount = 40;
    const loadedItemsCount = 40;
    lm.setLoadedItemsCount(loadedItemsCount).setAllItemsCount(allItemsCount);
    lm.reset();

    expect(lm._count).toBe(config.loadData.count);
    expect(lm._offset).toBe(0);
    expect(lm._allItemsCount).toBe(0);
    expect(lm._loadedItemsCount).toBe(0);
  });
});
