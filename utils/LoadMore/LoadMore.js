import config from '../../config';

export class LoadMore {
  _count = config.loadData.count;
  _offset: number = 0;
  _loadedItemsCount: number = 0;
  _allItemsCount: number = 0;

  /**
   *
   * @param count
   */
  constructor(count: number = config.loadData.count) {
    this._count = count;
  }

  /**
   *
   * @returns {boolean}
   */
  canLoadMore() {
    return this._loadedItemsCount < this._allItemsCount && this._offset <= this._loadedItemsCount;
  }

  /**
   *
   * @returns {number}
   */
  getNextOffset() {
    this._offset += this._count;
    return this._offset;
  }

  /**
   *
   * @param count
   * @returns {LoadMore}
   */
  setAllItemsCount(count: number) {
    this._allItemsCount = count;
    return this;
  }

  /**
   *
   * @param count
   * @returns {LoadMore}
   */
  setLoadedItemsCount(count: number) {
    this._loadedItemsCount = count;
    return this;
  }

  reset(count) {
    this._count = count !== undefined ? count : config.loadData.count;
    this._offset = 0;
    this._loadedItemsCount = 0;
    this._allItemsCount = 0;
  }
}
