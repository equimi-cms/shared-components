import { formatPriceWithSpaces } from "./price";

describe("formatPriceWithSpaces", () => {
  it("should proper format price with cents 100.01", () => {
    expect(formatPriceWithSpaces(100.01)).toBe("100.01");
  });

  it("should proper format price with cents 1000.01", () => {
    expect(formatPriceWithSpaces(1000.01)).toBe("1 000.01");
  });

  it("should proper format number 100", () => {
    expect(formatPriceWithSpaces(100)).toBe("100");
  });

  it("should proper format string 100", () => {
    expect(formatPriceWithSpaces("100")).toBe("100");
  });

  it("should proper format 1000", () => {
    expect(formatPriceWithSpaces(1000)).toBe("1 000");
  });

  it("should proper format 100000", () => {
    expect(formatPriceWithSpaces(100000)).toBe("100 000");
  });

  it("should proper format 100000000", () => {
    expect(formatPriceWithSpaces(100000000)).toBe("100 000 000");
  });
});
