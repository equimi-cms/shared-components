import Color from "color";
import CONFIG from "../../config";

/**
 *
 * @param color
 * @returns {*[]}
 */
export const getHSLAArrayFromCSSString = (color) => {
  return Color(color).hsl().round().array();
};

export const getAccentColors = (
  userAccentColorPrimary: string = CONFIG.accentColor.hex,
  isDarkThemeActive: boolean = false,
  templateData: {allowAccentColor: boolean} = {allowAccentColor: true}
) => {
  const neutralColor = "#424242";
  const {allowAccentColor} = templateData;

  const isNeutralColor = userAccentColorPrimary === neutralColor;
  /**
   * For templates that don't support accent colors use the default color from CONFIG
   */
  const userAccentColor = allowAccentColor ? userAccentColorPrimary : CONFIG.accentColor.hex;

  const defaultConfig = {
    colorBgPrimary: "#ffffff",
    accentColorPrimary: userAccentColor,
    promoColor: userAccentColor,
  };

  const darkConfig = {
    ...defaultConfig,
    colorBgPrimary: "#181818",
    accentColorPrimary: isNeutralColor ? "#ffffff" : userAccentColorPrimary,
  };

  const config = isDarkThemeActive ? darkConfig : defaultConfig;

  const [h, s, l, a = 1] = config?.accentColorPrimary
    ? getHSLAArrayFromCSSString(config.accentColorPrimary)
    : CONFIG.accentColor.hsl;

  const accentColorPrimary = userAccentColor
    ? `
       --color-bg-primary:${config.colorBgPrimary};
       --promo-color:${config.promoColor};
       --accent-color-primary:${config.accentColorPrimary};
       --accent-color-primary-h: ${h};
       --accent-color-primary-s: ${s}%;
       --accent-color-primary-l: ${l}%;
       `
    : "";

  const secondaryColorLightness = l - 5;

  const accentColorSecondary = userAccentColorPrimary
    ? `--accent-color-secondary: hsla(${h}, ${s}%, ${secondaryColorLightness}% , ${a});`
    : "";

  return {
    accentColorPrimary,
    accentColorSecondary,
    config,
    isNeutralColor,
    h,
    s,
    l,
  };
};
