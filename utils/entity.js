import moment from 'moment';
import {Entities} from './types';
import type {IHorse, IMember, INews, IService} from './type';
import {GENDER, STATUS} from './data';
import type {Entity} from '../utils/types';

interface IEntityUrlParts {
  created: string;
  urlAlias: string;
}
export const getNewsItemUrl = ({prefix = '', created, urlAlias}: IEntityUrlParts) => {
  const _created = moment(created).format('DD-MM-YYYY');
  return `${prefix}/news/${_created}/${urlAlias}`;
};

export const getServiceItemUrl = ({prefix = '', urlAlias}: IEntityUrlParts) => {
  return `${prefix}/services/${urlAlias}`;
};

const IUrlAlias: String = '';

/**
 * Get relative member url
 * @param data IUrlAlias | IMember
 * @return url String
 */
export const getMemberUrl = (data: IUrlAlias | IMember = '') => {
  return `/${Entities.MEMBER}/${getAlias(data)}`;
};

/**
 *
 * @param prefix
 * @param data
 * @return {string}
 */
export const getAbsoluteMemberUrl = (prefix: any, data: IUrlAlias | IHorse) => {
  return `${prefix}${getMemberUrl(data)}`;
};

/**
 * Get relative member url
 * @param data IUrlAlias | IMember
 * @return url String
 */
export const getSponsorUrl = (data: IUrlAlias | ISponsor = '') => {
  return `/${Entities.SPONSOR}/${getAlias(data)}`;
};

/**
 *
 * @param prefix
 * @param data
 * @return {string}
 */
export const getAbsoluteSponsorUrl = (prefix: any, data: IUrlAlias | ISponsor) => {
  return `${prefix}${getSponsorUrl(data)}`;
};

/**
 * Get relative horse url
 * @param data IUrlAlias | IHorse
 * @return url String
 */
export const getHorseUrl = (data: IUrlAlias | IHorse = '') => {
  return `/${Entities.HORSE}/${getAlias(data)}`;
};

/**
 *
 * @param prefix
 * @param data
 * @return {string}
 */
export const getAbsoluteHorseUrl = (prefix: any, data: IUrlAlias | IHorse) => {
  return `${prefix}${getHorseUrl(data)}`;
};

/**
 *
 * @param data
 * @param verbose
 */
export const getAlias = (data: String | {urlAlias: String}, verbose: boolean = false) => {
  const urlAlias =
    typeof data === 'string' ? data : data && data.hasOwnProperty('urlAlias') ? data.urlAlias : '';
  !urlAlias && verbose && console.warn('No url alias: ', urlAlias);
  return urlAlias;
};

/**
 * Get relative result url
 * @param data IUrlAlias | IHorse
 * @return url String
 */
export const getResultsUrl = (data: IUrlAlias | IResult = '') => {
  return `/${Entities.RESULT}/${getAlias(data)}`;
};

/**
 *
 * @param prefix
 * @param data
 * @return {string}
 */
export const getAbsoluteResultUrl = (prefix: any, data: IUrlAlias | IResult) => {
  return `${prefix}${getResultsUrl(data)}`;
};

/**
 * @param genderNumber
 * @returns {(ISelectData|String)|null}
 */
export const getGenderTitle = (genderNumber: Number = null) => {
  if (genderNumber == null) {
    return null;
  }
  const gender = GENDER.find(item => item.id === genderNumber);
  return gender && gender.title;
};

/**
 * @param statusNumber
 * @returns {(ISelectData|String)|null}
 */
export const getStatus = (statusNumber: Number = null) => {
  if (statusNumber == null) {
    return null;
  }
  const status = STATUS.find(item => item.value === statusNumber);
  return status && status.title;
};

export const getEntityUrl = (
  entity: Entity,
  data: IUrlAlias | IHorse | IMember | INews | IResult | IService
) => {
  switch (entity) {
    case Entities.HORSE:
      return getHorseUrl(data);
    case Entities.MEMBER:
      return getMemberUrl(data);
    case Entities.RESULT:
      return getResultsUrl(data);
    case Entities.NEWS:
      return getNewsItemUrl(data);
    case Entities.SERVICE:
      return getServiceItemUrl(data);
    default:
      console.warn('No entity!');
  }
};

/**
 * @param data
 */
export const getEntityTitle = (data: IUrlAlias | IHorse | IMember | INews | IResult) => {
  if (!data) return null;

  const {title, name, showName, className} = data;

  if (title != null) {
    return title;
  } else if (name != null) {
    return name;
  } else {
    return showName || className ? `${showName} ${className}` : null;
  }
};
