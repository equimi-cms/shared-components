import loadSegment from "./lib";

export class SegmentService {
  appId: String = "";
  isLoading = false;
  isLoaded = false;
  analytics = window.analytics ?? {identify: () => null, track: () => null, page: () => null};

  constructor(appId) {
    this.appId = appId;
  }

  load(onLoad: Function = () => null) {
    if (!this.isLoading) {
      this.isLoading = true;
      loadSegment(this.appId, () => {
        this.analytics = window.analytics;
        onLoad();
        this.isLoaded = true;
      });
    }
  }

  launch(data: {id: String, name: String, email: String}) {
    const {id, name, email, ...others} = data;
    this.analytics.identify(id, {
      name,
      email,
      ...others,
    });
  }

  guestLaunch(accountId: string) {
    this.analytics.identify({
      accountId,
    });
  }

  page(name: string, properties: {} = {}) {
    name ? this.analytics.page(name, properties) : this.analytics.page();
  }

  trackEvent(eventName: String, metadata: {} = null) {
    this.analytics.track(eventName, metadata);
  }
}
