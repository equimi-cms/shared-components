//@flow
import debounce from 'lodash/debounce';

type onResize = (e: any, isMobile: Boolean, isTablet: Boolean) => null;

export const defaultPhoneWidth = 479;

/**
 * Use to handle window resize event
 *
 * @example
 * const onResize = (e,isMobile, isTablet)=>{...handle resize}
 * const wr = new WindowResponse(onResize,150);
 * wr.destroy(); // unsubscribe from event
 */
export class WindowResponse {
  _config: {phoneWidth: number, tabletWidth: number};
  _onResizeCb: onResize = (e, isMobile, isTablet) => null;
  _debounceResize = () => {};

  /**
   * @param onResizeCb
   * @param debounceTime
   * @param config
   */
  constructor(
    onResizeCb: onResize = this._onResizeCb,
    debounceTime: number = 100,
    config: {phoneWidth: number, tabletWidth: number} = {phoneWidth: defaultPhoneWidth, tabletWidth: 1000}
  ) {
    this._config = config;
    this.init(onResizeCb, debounceTime);
  }

  /**
   * @param onResizeCb
   * @param debounceTime
   */
  init(onResizeCb: onResize, debounceTime: number) {
    this._onResizeCb = onResizeCb;
    this._debounceResize = debounce(this._onResize, debounceTime);

    this._onResizeCb(null, this.isMobile(), this.isTablet());
    window.addEventListener('resize', this._debounceResize);
  }

  _onResize = (e: Event) => {
    this._onResizeCb(e, this.isMobile(), this.isTablet());
  };

  isMobile(): Boolean {
    return window.innerWidth <= this._config.phoneWidth;
  }

  static isMobile(): Boolean {
    return window.innerWidth <= defaultPhoneWidth;
  }

  isTablet(): Boolean {
    return window.innerWidth <= this._config.tabletWidth;
  }

  destroy() {
    window.removeEventListener('resize', this._debounceResize);
  }
}
