import random from 'lodash/random';

export const videoItems = () => {
  const data = {
    kind: 'youtube#searchListResponse',
    etag: '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/KZAcnbV-btPWqSNsDTHknhZtRm0"',
    nextPageToken: 'CBQQAA',
    regionCode: 'UA',
    pageInfo: {totalResults: 9155, resultsPerPage: 20},
    items: [
      {
        kind: 'youtube#searchResult',
        etag: '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/IX-aYVGcxVFTzbNFZAEX3krq6zM"',
        id: {kind: 'youtube#video', videoId: 'Y8LRcaCk0sI'},
        snippet: {
          publishedAt: '2009-05-16T10:21:45.000Z',
          channelId: 'UCyAgSgP7NSsig-s8yGqBsrA',
          title: 'Serfing',
          description: '',
          thumbnails: {
            default: {
              url: 'https://picsum.photos/200/300/?random',
              width: 120,
              height: 90,
            },
            medium: {
              url: 'https://picsum.photos/200/300/?random',
              width: 320,
              height: 180,
            },
            high: {
              url: 'https://picsum.photos/200/300/?random',
              width: 480,
              height: 360,
            },
          },
          channelTitle: 'Anastasios',
          liveBroadcastContent: 'none',
        },
      },
      {
        kind: 'youtube#searchResult',
        etag: '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/5q9ByNNDZ5R8Gjsy7Zz1udzTWa0"',
        id: {kind: 'youtube#video', videoId: 'YnQV-OHCkBA'},
        snippet: {
          publishedAt: '2016-11-04T14:12:28.000Z',
          channelId: 'UCMSOL72igoXnKkaDXzNzU5A',
          title: 'Serfing Macao',
          description: '',
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/YnQV-OHCkBA/default.jpg',
              width: 120,
              height: 90,
            },
            medium: {
              url: 'https://i.ytimg.com/vi/YnQV-OHCkBA/mqdefault.jpg',
              width: 320,
              height: 180,
            },
            high: {
              url: 'https://i.ytimg.com/vi/YnQV-OHCkBA/hqdefault.jpg',
              width: 480,
              height: 360,
            },
          },
          channelTitle: 'Alex Pashin',
          liveBroadcastContent: 'none',
        },
      },
      {
        kind: 'youtube#searchResult',
        etag: '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/UjdEzvPQyh-K8GMqpEtBaQP57DI"',
        id: {kind: 'youtube#video', videoId: 'a3_xCCrS4iU'},
        snippet: {
          publishedAt: '2017-10-18T08:28:54.000Z',
          channelId: 'UCsSEjQKJY-EesmaECz7xKyQ',
          title: 'Серфинг -Serfing - лучшие моменты',
          description:
            'Серфинг – этот вид водного спорта представляет собой катание вдоль волны на специальной доске.Большинство...',
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/a3_xCCrS4iU/default.jpg',
              width: 120,
              height: 90,
            },
            medium: {
              url: 'https://i.ytimg.com/vi/a3_xCCrS4iU/mqdefault.jpg',
              width: 320,
              height: 180,
            },
            high: {
              url: 'https://i.ytimg.com/vi/a3_xCCrS4iU/hqdefault.jpg',
              width: 480,
              height: 360,
            },
          },
          channelTitle: 'GoProSport',
          liveBroadcastContent: 'none',
        },
      },
      {
        kind: 'youtube#searchResult',
        etag: '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/Nq2oJ_E1N6ePjv6jDbT6ZbwfRGY"',
        id: {kind: 'youtube#video', videoId: 'XfteKieUoTM'},
        snippet: {
          publishedAt: '2015-03-30T00:55:38.000Z',
          channelId: 'UCxgZADeOnzYo0SKhgSv90JA',
          title: 'First serfing!! Hawaii',
          description: '',
          thumbnails: {
            default: {
              url: 'https://i.ytimg.com/vi/XfteKieUoTM/default.jpg',
              width: 120,
              height: 90,
            },
            medium: {
              url: 'https://i.ytimg.com/vi/XfteKieUoTM/mqdefault.jpg',
              width: 320,
              height: 180,
            },
            high: {
              url: 'https://i.ytimg.com/vi/XfteKieUoTM/hqdefault.jpg',
              width: 480,
              height: 360,
            },
          },
          channelTitle: 'DIMM GRAND',
          liveBroadcastContent: 'none',
        },
      },
    ],
  };

  data.items.forEach(el => {
    el.id.videoId = el.id.videoId + random(1, 100000);
  });
  return data;
};
export const videoItem = {
  kind: 'youtube#videoListResponse',
  etag: '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/2vchFISsI2pl625jBhCRQGZW0GQ"',
  pageInfo: {
    totalResults: 1,
    resultsPerPage: 1,
  },
  items: [
    {
      kind: 'youtube#video',
      etag: '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/Ps7IgF8KWqFHC9kEgc-HPgpklpw"',
      id: 'Ks-_Mh1QhMc',
      snippet: {
        publishedAt: '2012-10-01T15:27:35.000Z',
        channelId: 'UCAuUUnT6oDeKwE6v1NGQxug',
        title: 'Your body language may shape who you are | Amy Cuddy',
        description:
          "Body language affects how others see us, but it may also change how we see ourselves. Social psychologist Amy Cuddy argues that \"power posing\" -- standing in a posture of confidence, even when we don't feel confident -- can boost feelings of confidence, and might have an impact on our chances for success. (Note: Some of the findings presented in this talk have been referenced in an ongoing debate among social scientists about robustness and reproducibility. Read Amy Cuddy's response here: http://ideas.ted.com/inside-the-debate-about-power-posing-a-q-a-with-amy-cuddy/)\n\nGet TED Talks recommended just for you! Learn more at https://www.ted.com/signup.\n\nThe TED Talks channel features the best talks and performances from the TED Conference, where the world's leading thinkers and doers give the talk of their lives in 18 minutes (or less). Look for talks on Technology, Entertainment and Design -- plus science, business, global issues, the arts and more.\n\nFollow TED on Twitter: http://www.twitter.com/TEDTalks\nLike TED on Facebook: https://www.facebook.com/TED\n\nSubscribe to our channel: https://www.youtube.com/TED",
        thumbnails: {
          default: {
            url: 'https://picsum.photos/200/300/?random',
            width: 120,
            height: 90,
          },
          medium: {
            url: 'https://picsum.photos/200/300/?random',
            width: 320,
            height: 180,
          },
          high: {
            url: 'https://picsum.photos/200/300/?random',
            width: 480,
            height: 360,
          },
          standard: {
            url: 'https://picsum.photos/200/300/?random',
            width: 640,
            height: 480,
          },
          maxres: {
            url: 'https://picsum.photos/200/300/?random',
            width: 1280,
            height: 720,
          },
        },
        channelTitle: 'TED',
        tags: [
          'Amy Cuddy',
          'TED',
          'TEDTalk',
          'TEDTalks',
          'TED Talk',
          'TED Talks',
          'TEDGlobal',
          'brain',
          'business',
          'psychology',
          'self',
          'success',
        ],
        categoryId: '22',
        liveBroadcastContent: 'none',
        defaultLanguage: 'en',
        localized: {
          title: 'Your body language may shape who you are | Amy Cuddy',
          description:
            "Body language affects how others see us, but it may also change how we see ourselves. Social psychologist Amy Cuddy argues that \"power posing\" -- standing in a posture of confidence, even when we don't feel confident -- can boost feelings of confidence, and might have an impact on our chances for success. (Note: Some of the findings presented in this talk have been referenced in an ongoing debate among social scientists about robustness and reproducibility. Read Amy Cuddy's response here: http://ideas.ted.com/inside-the-debate-about-power-posing-a-q-a-with-amy-cuddy/)\n\nGet TED Talks recommended just for you! Learn more at https://www.ted.com/signup.\n\nThe TED Talks channel features the best talks and performances from the TED Conference, where the world's leading thinkers and doers give the talk of their lives in 18 minutes (or less). Look for talks on Technology, Entertainment and Design -- plus science, business, global issues, the arts and more.\n\nFollow TED on Twitter: http://www.twitter.com/TEDTalks\nLike TED on Facebook: https://www.facebook.com/TED\n\nSubscribe to our channel: https://www.youtube.com/TED",
        },
        defaultAudioLanguage: 'en',
      },
      contentDetails: {
        duration: 'PT21M3S',
        dimension: '2d',
        definition: 'hd',
        caption: 'true',
        licensedContent: true,
        projection: 'rectangular',
      },
      statistics: {
        viewCount: '15978863',
        likeCount: '215898',
        dislikeCount: '4319',
        favoriteCount: '0',
        commentCount: '7648',
      },
    },
  ],
};
