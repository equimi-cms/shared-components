import {videoItem, videoItems} from "./mockData";
import produce from "immer";
import _uniqBy from "lodash/uniqBy";

/**
 * Youtube service for handling Youtube API data
 */
export class Youtube {
  /**
   *
   * @param key - Youtube API key
   * @param useStaticData - Set 'true' to prevent large number of requests to Youtube API while development
   */
  constructor(key: string, useStaticData: boolean = false, http) {
    this.key = key;
    this.url = "https://www.googleapis.com/youtube/v3";
    this.useStaticData = useStaticData;
    this.http = http;
  }

  /**
   *
   * @param query - Search query
   * @param maxResults - Number of results from request
   * @param nextPageToken - Use to request next page data
   * @returns {Promise<any>}
   */
  search(query: string, maxResults: number = 20, nextPageToken: string = "") {
    const key = `&key=${this.key}`;
    const q = `&q=${query}`;
    const pageToken = `&pageToken=${nextPageToken}`;
    const part = `part=snippet&type=video`;
    const url = `${this.url}/search?${part}${pageToken}&maxResults=${maxResults}${q}${key}`;
    return this._request(
      () => this.http.request({url, method: "GET"}, () => null, false),
      videoItems()
    );
  }

  /**
   * @param id - Video Id
   * @param cancel - Function to cancel request
   */
  getVideoById(id: string, cancel: () => {}) {
    //for dev
    // return new Promise(resolve => resolve({data:videoItem}));
    const url = `${this.url}/videos?part=contentDetails,snippet,statistics&id=${id}&key=${this.key}`;
    return this._request(() => this.http.request({url, method: "GET"}, cancel, false), videoItem);
  }

  /**
   * @param url - Youtube video url
   */
  getVideoIdFromUrl = (url: string) => {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    const match = url.match(regExp);
    return match && match[7].length === 11 ? match[7] : false;
  };

  /**
   * Youtube returns duplicates in search response when load more videos
   * @param data
   * @returns {Array}
   */
  getVideosUniqById = data => {
    const _data = produce(data, nState => {});
    return _uniqBy(_data, item => this.getVideoId(item));
  };

  /**
   *
   * @param data
   * @returns {*}
   */
  getVideoId = (data: {id: String | Object} = {id: ""}) => {
    const {id} = data;
    return typeof id === "string" ? id : id.videoId;
  };

  /**
   *
   * @param request - Common request to get data
   * @param data - Static data which returns when 'useStaticData' is true
   * @returns {Promise<any>}
   * @private
   */
  _request(request: () => {}, data: any = null) {
    return new Promise(res => {
      this.useStaticData ? setTimeout(() => res({data}), 500) : request().then(res);
    });
  }
}
