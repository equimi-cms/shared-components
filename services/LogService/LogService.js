// @flow
interface ILogMessage {
  app: String;
  accountId: String;
  message: {
    text?: String,
  };
  type: String;
}

export default class LogService {
  static App = {
    api: 'api',
    dash: 'dash',
    public: 'public',
  };

  static LogType = {
    log: 'log',
    info: 'info',
    error: 'error',
    warn: 'warn',
  };

  app: String;
  accountId: String;
  logHandler: ILogMessage => void;

  constructor(
    app: String,
    accountId: String = '',
    logHandler: ILogMessage => void = () => void 0
  ) {
    this.app = app;
    this.accountId = accountId;
    this.logHandler = logHandler;
  }

  _fireLog(
    messageData: any,
    type: String = LogService.LogType.log,
    accountId: String = this.accountId
  ) {
    const messageText = typeof messageData === 'string' ? messageData : null;
    const message = messageText ? {text: messageText} : messageData;

    this.logHandler({
      app: this.app,
      accountId,
      message,
      type,
    });
  }

  /**
   * @param message
   */
  log(message: any): void {
    this._fireLog(message, LogService.LogType.log);
  }

  /**
   * @param message
   */
  info(message: any): void {
    this._fireLog(message, LogService.LogType.info);
  }

  /**
   * @param message
   */
  error(message: any): void {
    this._fireLog(message, LogService.LogType.error);
  }

  /**
   * @param message
   */
  warn(message: any): void {
    this._fireLog(message, LogService.LogType.warn);
  }
}
