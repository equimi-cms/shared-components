import LogService from './LogService';

describe('LogService', () => {
  let logService;
  const mock = {
    app: LogService.App.dash,
    id: 'test-id',
    testMsg: {
      text: 'test',
    },
  };
  const handleLog = jest.fn();

  beforeAll(() => {
    logService = new LogService(mock.app, mock.id, data => handleLog(data));
  });

  it('fire log', () => {
    logService.log(mock.testMsg);
    expect(handleLog).toHaveBeenCalledWith({
      accountId: mock.id,
      app: mock.app,
      message: mock.testMsg,
      type: LogService.LogType.log,
    });
  });

  it('fire warn', () => {
    logService.warn(mock.testMsg);
    expect(handleLog).toHaveBeenCalledWith({
      accountId: mock.id,
      app: mock.app,
      message: mock.testMsg,
      type: LogService.LogType.warn,
    });
  });

  it('fire info', () => {
    logService.info(mock.testMsg);
    expect(handleLog).toHaveBeenCalledWith({
      accountId: mock.id,
      app: mock.app,
      message: mock.testMsg,
      type: LogService.LogType.info,
    });
  });

  it('fire error with custom app field', () => {
    logService.app = 1;
    logService.error(mock.testMsg);
    expect(handleLog).toHaveBeenCalledWith({
      accountId: mock.id,
      app: 1,
      message: mock.testMsg,
      type: LogService.LogType.error,
    });
  });

  it('fire log with testMsg as string', () => {
    logService.log('test');
    expect(handleLog).toHaveBeenCalledWith({
      accountId: mock.id,
      app: mock.app,
      message: {
        text: 'test',
      },
      type: LogService.LogType.log,
    });
  });
});
